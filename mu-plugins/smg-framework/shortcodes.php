<?php
/**
 * SMG Theme Shortcodes
 *
 * @package WordPress
 * @subpackage SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

 /**
  * Returns SVG Shortcode by FILE
  *
  * @param string $image
  * @param string $size
  * @param string $custom
  */
function get_svg_code( $atts ) {
	$svg = file_get_contents($atts['src']);
	return $svg;
}
add_shortcode('svg', 'get_svg_code');


/**
 * Returns SVG Shortcode with SPAN WRAPPER
 *
 * @param string $image
 * @param string $size
 * @param string $custom
 */
function get_svg_advanced_code( $atts ) {
  $atts = shortcode_atts(
		array(
			'src' => '',
			'width'  => '',
			'height' => '',
		), $atts
	);
	$svg = file_get_contents($atts['src']);
	$width_css = '';
	$height_css = '';
	if ( ! $atts['width'] == '' ) {
		$width_css = 'width:' . $atts['width'] . 'px; ';
	}
	if ( ! $atts['height'] == '' ) {
		$height_css = 'height:' . $atts['height'] . 'px;';
	}
	$svg = '<span class="svg" style="' . $width_css . $height_css . '">' . $svg . '</span>';
	return $svg;
}
add_shortcode('svg_advanced', 'get_svg_advanced_code');
