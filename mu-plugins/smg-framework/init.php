<?php
/**
 * SMG Theme Init
 *
 * @package WordPress
 * @subpackage SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */


/**
 * Enqueue cleanup styles and scripts
 */
if( ! function_exists( 'smg_cleanup_scripts' ) ) {
	function smg_cleanup_scripts() {
		// Remove Dashicons Enqueue
		if (! current_user_can('administrator') && ! is_user_logged_in() ) {
			wp_deregister_style( 'dashicons' );
		}
		// Add the comment-reply library on pages where it is necessary
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
}


/**
 * Enable SVG's to be uploaded
 */
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


/**
 * Count Number of Words In Post
 */
if( ! function_exists('word_count') ){
	function word_count() {
		$content = get_post_field( 'post_content', get_the_id() );
		$word_count = str_word_count( strip_tags( $content ) );
		return $word_count;
	}
}
