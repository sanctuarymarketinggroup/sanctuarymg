<?php
/**
 * SMG Helper Functions
 *
 * @package WordPress
 * @subpackage SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

/**
 * Returns ACF img
 *
 * @param string $image
 * @param string $size
 * @param string $custom
 */
 function acf_image( $image, $size = '', $custom = '' ) {
 	if($size != ''){
 		$image_size = $image['sizes'][ $size ];
 		$width = $image['sizes'][ $size . '-width' ];
 		$height = $image['sizes'][ $size . '-height' ];
 	} else {
 		$image_size = $image['url'];
 		$width = $image['width'];
 		$height = $image['height'];
 	}
 	$image_var = '<img ' . $custom . ' src="' . $image_size . '" alt="' . $image['alt'] . '" title="' . $image['alt'] . '" width="' . $width . '" height="' . $height . '" />';
 	return $image_var;
 }

/**
 * @param string $file
 * @param mixed  $args
 * @param string $default_folder
 * */
function show_template( $file, $args = null, $default_folder = 'inc' ) {
    $file = $default_folder . '/' . $file . '.php';
    if ( $args ) {
        extract( $args );
    }
    if ( locate_template( $file ) ) {
        include(locate_template( $file )); // Theme Check free. Child themes support.
    }
}

/* Get thumbnail image */
/**
 * @var int $id
 * @var string $size = 'full'
 * @return string
 */
function get_attached_img_url( $id, $size = 'full' ) {
    $img = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), $size );
    return $img[0];
}

/* Add click to call on phone numbers */
/**
 * @param string $phone
 * @return string
 */
function filter_phone( $phone ) {
	$tel = $phone;
	$tel = str_replace('-', '', $tel);
	$tel = str_replace('(', '', $tel);
	$tel = str_replace(')', '', $tel);
	$tel = str_replace('.', '', $tel);
	$tel = str_replace(' ', '', $tel);
	$tel = "+1" . $tel;
	return $tel;
}


/**
 * Add mailto on email address
 * 
 * @param string $email
 * @return string
 */
function link_email( $email ) {
	return '<a href="mailto:' . $email . '">' . $email . '</a>';
}


/**
 * @param string $name
 * @return string
 */
function get_svg( $source, $width = '', $height = '' ) {

	if( false !== strpos( $source, 'http' ) ) {
		return file_get_contents($source);
	}

	$svg = file_get_contents(get_stylesheet_directory_uri() . '/assets/images/svg/' . $source . '.svg');

	if( empty($width) & empty($height) ){
		return $svg;
	}

	if ( ! empty($width) ) {
		$width_css = 'width:' . $width . 'px; ';
	}
	if ( ! $height == '' ) {
		$height_css = 'height:' . $height . 'px;';
	}
	$svg = '<span class="svg" style="' . $width_css . $height_css . '">' . $svg . '</span>';
	return $svg;
}


/* Retrieves Primary Category Name */
/**
 * @param string $post_id
 * @param string $taxonomy
 * @return string
 */
function get_tax_primary_name($tax_type = 'category', $link = true, $id = 0) {
  if($id === 0){
    $id = get_the_ID();
  }
 	$primary_cat_id = get_post_meta($id, '_yoast_wpseo_primary_'.$tax_type, true);
 	if($primary_cat_id){
 		$primary_cat = get_term($primary_cat_id, 'project_cat');
 		if(isset($primary_cat->name)){
 			$taxName = $primary_cat->name;
 		}
 		if(isset($primary_cat->slug) && $link){
 			$taxName = '<a href="'.get_term_link($primary_cat->term_id, $tax_type).'">'.$taxName.'</a>';
 		}
 	}
  return $taxName;
}
