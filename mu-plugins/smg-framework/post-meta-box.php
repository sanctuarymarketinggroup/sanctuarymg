<?php

function featured_post_meta_box() {
  $args = array(
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'show_ui' => true
    // '_builtin' => false
  );
  $posts = get_post_types( $args, 'names', 'and' );
  unset($posts['attachment']);
  add_meta_box(
    'featured_post',
    __( 'Featured Post', 'smg' ),
    'featured_post_meta_box_callback',
    $posts,
    'side',
    'high'
  );
}
add_action( 'add_meta_boxes', 'featured_post_meta_box' );

function featured_post_meta_box_callback( $post ) {
  $field = '_featured_post';
  $value = esc_attr( get_post_meta( $post->ID, $field, true ) );
  $text = esc_html__( 'Set post as featured', 'smg' );
  $state = checked( $value, 'yes', false);
  $value = $state ? 'yes' : 'no';
  printf(
    '<input type="hidden" id="%1$s" name="%1$s" value="%2$s" /><input type="checkbox" id="%1$s_checkbox" name="%1$s_checkbox" value="1" %3$s onchange="updateFeaturedPost(this)"><label for="%1$s_checkbox">%4$s</label>',
    $field, $value, $state, $text
  );
  echo '<script>
    function updateFeaturedPost(el) {
      document.getElementById("'. $field .'").value = el.checked ? "yes" : "no";
    }
  </script>';
}

function save_featured_post_meta_box($post_id) {
  if (false === isset( $_POST['_featured_post'] )) {
    return;
  }
  $value = $_POST['_featured_post'] === 'yes' ? 'yes' : 'no';
  update_post_meta( $post_id, '_featured_post', $value );
}
add_action('save_post', 'save_featured_post_meta_box');

// ADD NEW COLUMN
function featured_post_columns_head($defaults) {
	$defaults['_featured_post'] = 'Featured';
	return $defaults;
}

// SHOW THE FEATURED IMAGE
function featured_post_columns_content($column_name, $post_ID) {
	if ($column_name == '_featured_post') {
    if (get_post_meta($post_ID, '_featured_post', true) == 'yes') {
      echo '<img src="' . plugins_url( '/assets/svg/star.svg', __FILE__ ) . '" width="32" />';
    }
	}
}

add_filter('manage_posts_columns', 'featured_post_columns_head');
add_action('manage_posts_custom_column', 'featured_post_columns_content', 10, 2);

add_filter('manage_studies_columns', 'featured_post_columns_head');
add_action('manage_studies_custom_column', 'featured_post_columns_content', 10, 2);

add_filter('manage_portfolios_columns', 'featured_post_columns_head');
add_action('manage_portfolios_custom_column', 'featured_post_columns_content', 10, 2);

add_filter('manage_reviews_columns', 'featured_post_columns_head');
add_action('manage_reviews_custom_column', 'featured_post_columns_content', 10, 2);
