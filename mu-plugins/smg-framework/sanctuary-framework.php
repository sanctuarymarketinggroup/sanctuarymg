<?php

/** Initialize */
require_once( 'smg-framework/init.php' );

/** Cleanup Functions */
require_once( 'smg-framework/cleanup.php' );

/** Healper Functions */
require_once( 'smg-framework/helper.php' );

/** Add Post Meta Box */
require_once( 'smg-framework/post-meta-box.php' );

/** Relative Theme Assets for Calling Scripts */
require_once( 'smg-framework/protocol-relative-theme-assets.php' );

/** Shortcodes */
require_once( 'smg-framework/shortcodes.php' );