<?php
/**
 * The template for displaying search results pages.
 *
 * @package WordPress
 * @subpackage SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

get_header(); ?>

<main id="search" class="body subpage" role="main">
	<div class="main-content">
		<header>
      <?php yoast_breadcrumb(); ?>
			<h1 class="entry-title">
				<?php _e( 'Search Results for', 'smg' ); ?> "<?php echo get_search_query(); ?>"
			</h1>
		</header>
		<div class="archive-content">
			<?php
				if( have_posts() ) {
					while ( have_posts() ) { the_post();
						get_template_part( 'content', get_post_format() );
					}
				} else {
					get_template_part( 'content', 'none' );
				}
			?>
			<?php smg_pagination(); ?>
		</div>
	</div>
</main>

<?php get_footer(); ?>
