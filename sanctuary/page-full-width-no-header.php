<?php
/* Template Name: Full Width No Header Template */

get_header(); ?>

<main id="page" class="container subpage " role="main">
<?php while ( have_posts() ) : the_post(); ?>
    <article class="main-content" id="post-<?php the_ID(); ?>">
        <header class="no-header-height">
        </header>
        <div class="entry-content row-radial-gradient-2">
            <?php the_content(); ?>
            <?php // edit_post_link( __( 'Edit', 'foundationpress' ), '<span class="edit-link">', '</span>' ); ?>
        </div>
        <footer>
            <?php // wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
        </footer>
    </article>
<?php endwhile;?>
</main>

<?php get_footer();
