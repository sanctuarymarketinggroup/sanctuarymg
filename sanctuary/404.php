<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

function digisavvy_beaver_builder( $post_ids ) {
	$post_ids[] = 10627;
	return $post_ids; 
}
add_filter( 'fl_builder_global_posts', 'digisavvy_beaver_builder' );

get_header(); ?>

<main id="not-found-page" class="container subpage" role="main">
	<article class="main-content" id="post-<?php the_ID(); ?>">
		<header>
			<?php // _e( 'The page you are looking for this is not.', 'smg' ); ?>
			<?php // _e( 'Well, this tactic didn\'t go as planned', 'smg' ); ?>
			<?php // _e( 'Missing: Landing Page. If found please call 330.266.1188', 'smg' ); ?>
			<?php // _e( 'Missing: Page. If found please call 330.266.1188', 'smg' ); ?>
			<h1 class="entry-title"><?php _e( 'This is not the page you are looking for.', 'smg' ); ?></h1>
		</header>
		<div class="entry-content row-radial-gradient-2">
		<?php echo do_shortcode('[fl_builder_insert_layout slug="404-error-page"]'); ?>
		</div>
  </article>
</main>

<?php get_footer();
