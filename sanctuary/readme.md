# This is an <h1> tag
## This is an <h2> tag
###### This is an <h6> tag

*This text will be italic*
_This will also be italic_

**This text will be bold**
__This will also be bold__

_You **can** combine them_

* Item 1
* Item 2
  * Item 2a
  * Item 2b

1. Item 1
1. Item 2
1. Item 3
   1. Item 3a
   1. Item 3b

# Custom Post Types

## Testimonials
  * Category ( featured/homepage )
## Case Studies
  * Logo
  * Category ( featured/homepage )
  * Featured Image
  * Gallery?
## Portfolio

# Custom Taxonomy

## Category

# Global

## Header

## Footer
* Grow with Us
  * tp-footer-1.php
  * tp-footer-1.scss
* Connect with us
  * st-footer-2.php
  * st-footer-2.scss
* Copyright
  * st-footer-3.php
  * st-footer-3.scss


# Pages

## 1. Homepage
* Hero - ACF & Featured Image & Featured Video
  * ACF Hero Content
  * Featured Image
  * Featured Video
    * Post Meta Field -OR- ACF
  * front.php
  * Header (Inline CSS)
* Section 2 - Beaver Builder
  * entry-content
  * front.scss
* Section 3 - Beaver Builder
  * entry-content
  * front.scss
* Section 4 - Beaver Builder
  * entry-content
  * front.scss
* Section 5 - Real Results
  * Shortcode - testimonials -OR- Beaver Builder Custom Module
    * CPT - Pull in testimonials.
  * Shortcode - projects -OR- Beaver Builder Custom Module
    * CPT - Pull in featured projects.
* Section 6 - Full funnel
  * Shortcode - funnel functionality -OR- Beaver Builder Custom Module
  * shortcodes.php
  * shortcodes/full-funnel.scss
* Section 7 - Portfolio
  * Beaver Builder - Content
  * Shortcode -OR- Beaver Builder Custom Module
    * CPT - Pull in Portfolio?
* Section 8 - Get More
  * Beaver Builder
* Section 9 - Grow with us
  * 


## 2. Marketing Leadership
* Hero - ACF & Featured Image
  * ACF Hero Content
  * Featured Image
  * page.php
  * Header (Inline CSS)

## 3. Text and Graphics