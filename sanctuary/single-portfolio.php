<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

get_header(); 
//TO DO add in stuff from Theme options
$portfolio = get_field('portfolio_single_header_image');
$featured_image = $portfolio ? $portfolio['url'] : get_the_post_thumbnail_url( get_the_id(), 'large' );
?>

<main id="post" class="contianer subpage portfolio-single row-radial-gradient-2" role="main">
<?php while ( have_posts() ) { the_post(); ?>
	<article class="main-content" id="post-<?php the_ID(); ?>">
		<div id="portfolio-header" style="background-image:url('<?php echo $featured_image; ?>');">
		<h1 class="entry-title nonscripty">
				<?php the_title(); ?>
			</h1>
		<a class="slight-next" href="#porfolio-content">
			<?php echo get_svg('down-arrow-1'); ?> 
		</a>
	</div>
		<div id="porfolio-content" class="entry-content">
			<?php smg_breadcrumbs(); ?>
			<?php the_content(); ?>
		</div>
		<footer>
			<?php comments_template(); ?>
			<meta itemprop="copyrightHolder" content="<?php echo get_setting('site-name');?>"/>
			<?php $header_logo = get_setting('header-logo');  ?> 
			<?php $url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID)); ?>
			<span itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
				<meta itemprop="url"  content="<?php echo $url['0'];?>"/>
				<meta itemprop="width"  content="<?php echo $url['1'];?>"/>
				<meta itemprop="height"  content="<?php echo $url['2'];?>"/>
			</span>
			<span itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
				<meta itemprop="url"  content="<?php echo HOME_URL;?>"/>
				<span itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
					<meta itemprop="url"  content="<?php echo $header_logo['url'];?>"/>
					<meta itemprop="width"  content="<?php echo $header_logo['width'];?>"/>
					<meta itemprop="height"  content="<?php echo $header_logo['height'];?>"/>
				</span>
				<meta itemprop="name" content="<?php echo get_setting('site-name');?>">
			</span>
		</footer>
	</article>
<?php } ?>
</main>
<?php get_footer(); ?>
