<?php
/**
 * The template part for displaying a message that posts cannot be found
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

?>

<article class="main-content" id="post-<?php the_ID(); ?>">
	<header>
		<h2 class="entry-title">
			<?php _e( 'Nothing Found', 'smg' ); ?>
		</h2>
	</header>
	<div class="entry-content">
		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) { ?>
			<p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'smg' ), admin_url( 'post-new.php' ) ); ?></p>
		<?php } elseif ( is_search() ) { ?>
			<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'smg' ); ?></p>
			<?php get_search_form(); ?>
		<?php } else { ?>
			<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'smg' ); ?></p>
			<?php get_search_form(); ?>
		<?php } ?>
	</div>
</article>