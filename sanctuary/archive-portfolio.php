<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

global $wp_query;

get_header();
?>

<div id="archive" class="container subpage row-radial-gradient-2" role="main">
	<div class="main-content">
		<header>
      <?php yoast_breadcrumb();?>
			<h1 class="entry-title scripty">
				<?php echo get_the_archive_title(); ?>
			</h1>
			<div class="archive-content-filter">
				<a href="#" portfolio_cat="-1">View All</a>
				<?php
				$cats = get_terms(array(
						'taxonomy' => 'portfolio_cat',
						'hide_empty' => true,
				));
				foreach ($cats as $cat) {
						echo '<a href="#" portfolio_cat="' . $cat->term_id . '">' . $cat->name . '</a>';
				}
				?>
			</div>
		</header>
		<div class="archive-content archive-preview-blocks">
			<div id="nop" style="display:none;" count="<?php echo wp_count_posts('portfolio')->publish; ?>"></div>
			<?php
if (have_posts()) {
    while (have_posts()) {the_post();
        show_template(
            'content-block',
            array(
                'terms' => array(
                    'portfolio_cat' => get_the_terms(get_the_ID(), 'portfolio_cat'),
                ),
                'image' => get_field('portfolio_archive_square_image')['url'], // get_the_post_thumbnail_url( get_the_id() ),
                'link' => get_permalink(),
                'before' => apply_filters('before_block_content', ''),
                'after' => apply_filters('after_block_content', ''),
            ),
            './'
        );
    }
} else {
    get_template_part('content', 'none');
}
if (0 !== $wp_query->post_count % 12) {
    echo '<div id="num-singles" value="' . $wp_query->post_count . '"></div>';
    show_template('content-block-none', array(), './');
    show_template('content-block-none', array(), './');
}
?>
			<?php // smg_pagination(); ?>
		</div>
	</div>
</div>

<?php get_footer();?>
