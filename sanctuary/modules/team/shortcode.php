<?php
function smg_team_shortcode_blocks_code( $atts , $content = null ) {

	// Attributes
	$atts = shortcode_atts(
		array(
      'limit' => '-1',
      'ids' => ''
		),
		$atts,
		'team_blocks'
	);
  
  // Query
  $args = array ( 
    'post_type' => 'team',
    'posts_per_page' => $atts['limit'] 
  ); 
  if($atts['ids'] != '') {
    $ids = explode(',',$atts['ids']);
    $args['post__in'] = $ids;
    $args['orderby'] = 'post__in'; 
  }
	$the_query = new WP_Query( $args );
	
  // Posts
  ob_start();
  echo '<div id="team-short" class="archive-preview-blocks archive-team-blocks">';
	while ( $the_query->have_posts() ) {
    $the_query->the_post();
	  show_template(
      'content-block',
      array(
        'image' => get_field('team_archive_square_image',get_the_id())['url'], // get_the_post_thumbnail_url( get_the_id() ),
        'link' => get_permalink(get_the_id()),
        'before' => apply_filters('before_block_content', ''),
        'after' => apply_filters('after_block_content', '<h2>' . get_the_title() . '</h2><p>' . get_field('position') . '</p>')
      ),
      './'
    );
  }
	echo '</div>';
	// Reset post data
	wp_reset_postdata();
	
	// Return code
	return ob_get_clean();


}

add_shortcode('team_blocks', 'smg_team_shortcode_blocks_code');