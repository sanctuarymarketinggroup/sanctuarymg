<?php

/**
 * Config
 * 
 * Make sure to do Search and Replace with Case Sensetive
 * @Search & Replace - team = 
 * @Search & Replace - Team = 
 */

/**
 * Module Settings
 */
SMG_MODVAR::set('team_slider_limit', '5');
SMG_MODVAR::set('team_slider_now', '50');
SMG_MODVAR::set('team_slider_title', 'Team Slider Title');

add_action( 'init', 'smg_register_team_cpt' );
add_filter( 'acf/settings/load_json', 'smg_register_team_acf' );
add_action( 'pre_get_posts', 'smg_reorder_team_posts');
//add_action( 'init', 'smg_register_team_tax' );
//add_action( 'template_redirect', 'redirect_team_single' );

include_once 'shortcode.php';


/**
 * Create Custom Post Type
 */

function smg_register_team_cpt() {
  $rewrite = array(
    'slug'                  => '/why-sanctuary/team',
    'with_front'            => true,
    'pages'                 => true,
    'feeds'                 => true,
  );
  register_post_type('team', array(
    'label'         => __( 'The Team', 'text_domain' ),
    'description'   => __( 'The Team Description', 'text_domain' ),
    'labels'        => get_cpt_labels('Team', 'The Team'),
    'supports'      => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
    'public'        => true,
    'menu_position' => 5,
    'has_archive'   => 'why-sanctuary/team',
    'rewrite'       => $rewrite
  ));
}

/**
 * Register Taxonomies
 */
function smg_register_team_tax() {
  register_taxonomy('team_tax', array('team'), array(
    'labels'            => get_tax_labels('Teams Category', 'Teams Categories'),
    'hierarchical'      => true,
    'public'            => true,
    'show_admin_column' => true,
    'show_tagcloud'     => false
  ));
}

/**
 * Register ACF Fields
 */
function smg_register_team_acf($paths){
  $paths[] = SMG_MOD_DIR . '/teams';
  return $paths;
};

/**
 * Redirect All Single Pages to Archive Page
 */
function redirect_team_single() {
  $queried_post_type = get_query_var('post_type');
  if ( is_single() && 'team' ==  $queried_post_type ) {
    $url = get_post_type_archive_link( 'team' );
    wp_redirect( $url, 301 );
    exit;
  }
}

/**
 * Shortcode Slider
 */
function smg_team_shortcode_slider($atts){

  // Attributes
	$atts = shortcode_atts(
		array(
      'cat' => '',
      'limit' => SMG_MODVAR::get('team_slider_limit'),
      'now' => SMG_MODVAR::get('team_slider_now'),
      'title' => SMG_MODVAR::get('team_slider_title')
		),
		$atts,
		'team_slider'
  );
  
  $args = array(
    'post_type' => 'team',
    'posts_per_page' => $atts['limit'],
    'orderby' => 'rand'
  );
  if( isset($atts['cat']) ) {
    $args['tax_query'] = array(
      array (
        'taxonomy' => 'team-category',
        'field' => 'id',
        'terms' => $atts['cat'],
      )
    );
  }

  $custom_query = new WP_Query($args);
  $output = '';
  if($custom_query->have_posts()){
    $output .= '<div class="teams-container">';
      $output .= '<h2 class="title">' . $atts['title'] . '</h2>';
      $output .= '<div class="slider">';
      while($custom_query->have_posts()) { $custom_query->the_post();
        $output .= '<div class="slide">';
          $output .= '<div class="slide-title">';
            $output .= '<div>' . get_the_title() . '</div>';
          $output .= '</div>';
          $output .= '<div class="content">';
            $output .= '<p>'; 
              $output .= wp_trim_words(get_the_content(), $atts['now']);
              $output .= ' <a href="' . get_post_type_archive_link( $args['post_type'] ) . '#team-' . get_the_id() . '">Read&nbsp;More</a>';
            $output .= '</p>';
          $output .= '</div>';
        $output .= '</div>';
      }
      $output .= '</div>';
    $output .= '</div>';
    wp_reset_postdata();
  }
  return $output;
}

function smg_reorder_team_posts($query) {
  if ( $query->is_post_type_archive('team') && $query->is_main_query() ) {
    $query->set( 'order', 'ASC' );
    $query->set( 'orderby', 'menu_order' );
    $query->set( 'posts_per_page', -1 );
  }
}