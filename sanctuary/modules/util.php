<?php

final class SMG_MODVAR {
  private static $vars;
  
  public static function get( $var ) {
    return self::$vars[$var];
  }
  
  public static function set( $var, $value ) {
    self::$vars[$var] = $value;
  }
}

function get_cpt_labels( $singular, $plural, $args = array() ) {
  $default = array(
    'name'                  => _x( $plural, $singular . ' ', 'text_domain' ),
    'singular_name'         => _x( $singular, $singular . ' ', 'text_domain' ),
    'menu_name'             => __( $plural, 'text_domain' ),
    'name_admin_bar'        => __( $singular, 'text_domain' ),
    'archives'              => __( $singular . ' Archives', 'text_domain' ),
    'attributes'            => __( $singular . ' Attributes', 'text_domain' ),
    'parent_item_colon'     => __( 'Parent ' . $singular . ':', 'text_domain' ),
    'all_items'             => __( 'All '.$plural, 'text_domain' ),
    'add_new_item'          => __( 'Add New ' . $singular, 'text_domain' ),
    'add_new'               => __( 'Add New', 'text_domain' ),
    'new_item'              => __( 'New ' . $singular, 'text_domain' ),
    'edit_item'             => __( 'Edit ' . $singular, 'text_domain' ),
    'update_item'           => __( 'Update ' . $singular, 'text_domain' ),
    'view_item'             => __( 'View ' . $singular, 'text_domain' ),
    'view_items'            => __( 'View ' . $plural, 'text_domain' ),
    'search_items'          => __( 'Search ' . $singular, 'text_domain' ),
    'not_found'             => __( 'Not found', 'text_domain' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
    'featured_image'        => __( 'Featured Image', 'text_domain' ),
    'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
    'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
    'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
    'insert_into_item'      => __( 'Insert into ' . $singular, 'text_domain' ),
    'uploaded_to_this_item' => __( 'Uploaded to this ' . $singular, 'text_domain' ),
    'items_list'            => __( $plural . ' list', 'text_domain' ),
    'items_list_navigation' => __( $plural . ' list navigation', 'text_domain' ),
    'filter_items_list'     => __( 'Filter '. $plural .' list', 'text_domain' )
  );
  return array_merge($default, $args);
}

function get_tax_labels( $singular, $plural, $args = array() ) {
  $default = array(
    'name'                       => _x( $plural, $singular  .' General Name', 'text_domain' ),
    'singular_name'              => _x( $singular, $singular . ' Singular Name', 'text_domain' ),
    'menu_name'                  => __( $singular, 'text_domain' ),
    'all_items'                  => __( 'All ' . $plural, 'text_domain' ),
    'parent_item'                => __( 'Parent ' . $singular, 'text_domain' ),
    'parent_item_colon'          => __( 'Parent ' . $singular.':', 'text_domain' ),
    'new_item_name'              => __( 'New ' . $singular.' Name', 'text_domain' ),
    'add_new_item'               => __( 'Add New ' . $singular, 'text_domain' ),
    'edit_item'                  => __( 'Edit ' . $singular, 'text_domain' ),
    'update_item'                => __( 'Update ' . $singular, 'text_domain' ),
    'view_item'                  => __( 'View ' . $singular, 'text_domain' ),
    'separate_items_with_commas' => __( 'Separate ' . $plural.' with commas', 'text_domain' ),
    'add_or_remove_items'        => __( 'Add or remove ' . $plural, 'text_domain' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
    'popular_items'              => __( 'Popular ' .$plural, 'text_domain' ),
    'search_items'               => __( 'Search ' . $plural, 'text_domain' ),
    'not_found'                  => __( 'Not Found', 'text_domain' ),
    'no_terms'                   => __( 'No ' . $plural, 'text_domain' ),
    'items_list'                 => __( $plural . ' list', 'text_domain' ),
    'items_list_navigation'      => __( $plural . ' list navigation', 'text_domain' ),
  );
  return array_merge($default, $args);
}