<?php

/**
 * Config
 * 
 * Make sure to do Search and Replace with Case Sensetive
 * @Search & Replace - team = team-gallery
 * @Search & Replace - Team = 
 */

/**
 * Module Settings
 */
SMG_MODVAR::set('team_gallery_limit', '5');
SMG_MODVAR::set('team_gallery_now', '50');
SMG_MODVAR::set('team_gallery_title', 'Team Gallery Slider Title');

add_action( 'init', 'smg_register_team_gallery_cpt' );
//add_filter( 'acf/settings/load_json', 'smg_register_team_gallery_acf' );
//add_action( 'init', 'smg_register_team_gallery_tax' );
//add_action( 'template_redirect', 'redirect_team_single' );
// /add_shortcode('team_gallery', 'smg_team_gallery_shortcode');



/**
 * Create Custom Post Type
 */

function smg_register_team_gallery_cpt() {
  register_post_type('team-gallery', array(
    'label'         => __( 'Team Galleries', 'text_domain' ),
    'description'   => __( 'Team Galleries Description', 'text_domain' ),
    'labels'        => get_cpt_labels('Team Gallery', 'Team Galleries'),
    'public'        => true,
    'menu_position' => 5,
    'has_archive'   => true,
  ));
}

/**
 * Register Taxonomies
 */
function smg_register_team_gallery_tax() {
  register_taxonomy('team_gallery_tax', array('team'), array(
    'labels'            => get_tax_labels('Team Gallery Category', 'Team Gallery Categories'),
    'hierarchical'      => true,
    'public'            => true,
    'show_admin_column' => true,
    'show_tagcloud'     => false
  ));
}

/**
 * Register ACF Fields
 */
function smg_register_team_gallery_acf($paths){
  $paths[] = SMG_MOD_DIR . '/team-gallery';
  return $paths;
};

/**
 * Redirect All Single Pages to Archive Page
 */
function redirect_team_gallery_single() {
  $queried_post_type = get_query_var('post_type');
  if ( is_single() && 'team-gallery' ==  $queried_post_type ) {
    $url = get_post_type_archive_link( 'team-gallery' );
    wp_redirect( $url, 301 );
    exit;
  }
}

/**
 * Shortcode Slider
 */
function smg_team_gallery_shortcode($atts){

  // Attributes
	$atts = shortcode_atts(
		array(
      'cat' => '',
      'limit' => SMG_MODVAR::get('team_gallery_limit'),
      'now' => SMG_MODVAR::get('team_gallery_now'),
      'title' => SMG_MODVAR::get('team_gallery_title')
		),
		$atts,
		'team_gallery'
  );
  
  $args = array(
    'post_type' => 'team-gallery',
    'posts_per_page' => $atts['limit'],
    'orderby' => 'rand'
  );
  if( isset($atts['cat']) ) {
    $args['tax_query'] = array(
      array (
        'taxonomy' => 'team-gallery-category',
        'field' => 'id',
        'terms' => $atts['cat'],
      )
    );
  }

  $custom_query = new WP_Query($args);
  $output = '';
  if($custom_query->have_posts()){
    $output .= '<div class="teams-container">';
      $output .= '<h2 class="title">' . $atts['title'] . '</h2>';
      $output .= '<div class="slider">';
      while($custom_query->have_posts()) { $custom_query->the_post();
        $output .= '<div class="slide">';
          $output .= '<div class="slide-title">';
            $output .= '<div>' . get_the_title() . '</div>';
          $output .= '</div>';
          $output .= '<div class="content">';
            $output .= '<p>'; 
              $output .= wp_trim_words(get_the_content(), $atts['now']);
              $output .= ' <a href="' . get_post_type_archive_link( $args['post_type'] ) . '#team-' . get_the_id() . '">Read&nbsp;More</a>';
            $output .= '</p>';
          $output .= '</div>';
        $output .= '</div>';
      }
      $output .= '</div>';
    $output .= '</div>';
    wp_reset_postdata();
  }
  return $output;
}
