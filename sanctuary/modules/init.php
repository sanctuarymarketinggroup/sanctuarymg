<?php

/**
 * Defines for Modules
 */
define('SMG_MOD_DIR', get_template_directory() . '/modules');
define('SMG_MOD_ROOT', get_stylesheet_directory_uri() . '/modules');

/**
 * Require Utility Function
 */
require_once('util.php');


/**
 * Require Module: Team
 */
require_once('team/init.php');

/**
 * Require Module: Team Gallery
 */
require_once('team-gallery/init.php');
