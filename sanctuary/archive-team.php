<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

global $wp_query;

get_header();
?>

<div id="archive" class="container subpage row-radial-gradient-2" role="main">
	<div class="main-content">
		<header>
      <?php yoast_breadcrumb(); ?>
			<h1 class="entry-title scripty">
				<?php echo get_the_archive_title(); ?>
			</h1>
		</header>
		<div class="archive-content archive-preview-blocks archive-team-blocks">
			<div class="case-studies-row">
				<div class="case-studies-intro">
					<h2>Leaders. Partners. Specialists.</h2>
					<p>Our people are the heart of Sanctuary and the value we provide. We are digital marketing specialists with deep expertise in a range of disciplines. Here’s the team who will work with you to direct and execute every detail of your digital market strategy.</p>
				</div>
			</div>
			<?php
				if( have_posts() ) {
					while ( have_posts() ) { the_post();
            show_template(
              'content-block',
              array(
                'image' => get_field('team_archive_square_image')['url'], // get_the_post_thumbnail_url( get_the_id() ),
                'link' => get_permalink(),
                'before' => apply_filters('before_block_content', ''),
                'after' => apply_filters('after_block_content', '<h2>' . get_the_title() . '</h2><p>' . get_field('position') . '</p>')
              ),
              './'
            );
					}
				} else {
					get_template_part( 'content', 'none' );
				}
			?>
			<?php smg_pagination(); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
