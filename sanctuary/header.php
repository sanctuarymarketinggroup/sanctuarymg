<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package WordPress
 * @subpackage SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-T63V68');</script>
		<!-- End Google Tag Manager --> 
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo THEME_ROOT; ?>/assets/images/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo THEME_ROOT; ?>/assets/images/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo THEME_ROOT; ?>/assets/images/favicon-16x16.png">
		<link rel="manifest" href="<?php echo THEME_ROOT; ?>/assets/images/site.webmanifest"> 
		<link rel="mask-icon" href="<?php echo THEME_ROOT; ?>/assets/images/safari-pinned-tab.svg" color="#fa8b00">
		<meta name="msapplication-TileColor" content="#fa8b00">
		<meta name="theme-color" content="#ffffff">
		<!-- <script type="text/javascript"> 
			var _ss = _ss || [];
			_ss.push(['_setDomain', 'https://koi-3QNMYQLLP0.marketingautomation.services/net']);
			_ss.push(['_setAccount', 'KOI-4AB2887ZNS']);
			_ss.push(['_trackPageView']);
			(function() {
				var ss = document.createElement('script');
				ss.type = 'text/javascript'; ss.async = true;
				ss.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'koi-3QNMYQLLP0.marketingautomation.services/client/ss.js?ver=2.4.0';
				var scr = document.getElementsByTagName('script')[0];
				scr.parentNode.insertBefore(ss, scr);
			})();
		</script> -->
		<?php wp_head(); ?>
	
		<link rel="preload" href="<?php echo THEME_ROOT; ?>/assets/fonts/selima_-webfont.eot" as="font">
		<script async src="https://js.alpixtrack.com/alphpixel.js?cust=4139087-510-WKYC"></script>
	</head>
	<body <?php body_class(); ?>>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T63V68"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<?php show_template('st-header-1'); ?>
