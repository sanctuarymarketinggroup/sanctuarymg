(function($) {
  var $grid = $("#team-gallery").isotope({
    // main isotope options
    itemSelector: ".team-gallery-bg",
    // set layoutMode
    layoutMode: "packery"
  });
})(jQuery);
