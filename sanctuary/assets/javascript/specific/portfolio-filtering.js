(function($) {
  var $archive = $(".archive-preview-blocks");
  var archive = document.querySelector(".archive-preview-blocks");
  var $block = $archive.find("a");
  var $filter = $(".archive-content-filter");
  var _projects_filter = function(event) {
    event.preventDefault();
    $(this)
      .siblings("a")
      .removeClass("active");
    $(this).addClass("active");
    $selected = $(this).attr("portfolio_cat");
    if ($selected == -1) {
      $block.show();
      return;
    }
    $block.each(function(index) {
      if (
        $(this)
          .data("cat")
          .toString() !== $selected.toString()
      ) {
        $(this).hide();
      } else {
        $(this).show();
      }
    });
  };
  var _maybeFilter = function() {
    console.log("Maybe Filter");
    $selected = $filter.find(".active").attr("portfolio_cat");
    $block = $archive.find("a");
    console.log("Select :" + $selected);
    if ($selected == -1) {
      $block.show();
      return;
    }
    $block.each(function(index) {
      console.log(
        "cat :" +
          $(this)
            .data("cat")
            .toString()
      );
      if (
        $(this)
          .data("cat")
          .toString() !== $selected.toString()
      ) {
        $(this).hide();
      } else {
        $(this).show();
      }
    });
  };
  var count = 12;
  var _loadMoreProjects = function() {
    var nop = $archive.find("#nop").attr("count");

    console.log("NOP: " + nop);
    console.log("COUNT: " + count);
    if (parseInt(count) < parseInt(nop)) {
      console.log("Skip Ajax");
      var rect = archive.getBoundingClientRect();
      var wHeight = $(window).height();

      if (rect.bottom - 1200 <= wHeight) {
        var data = {
          action: "getProjects",
          offset: count
        };
        $.ajax({
          type: "POST",
          dataType: "html",
          data: data,
          url: portAjax.ajax_url,
          success: function(response) {
            console.log("got new data");
            $archive.append(response);
            _maybeFilter();
          }
        });
        count += 3;
      }
    }
  };

  $filter.find("a").on("click", _projects_filter);

  $(window).on("scroll", _loadMoreProjects);
})(jQuery);
