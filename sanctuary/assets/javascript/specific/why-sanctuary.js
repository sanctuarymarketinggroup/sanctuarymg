(function($) {
  /** Define Variables */
  var $app = $(".why-sanctuary-animation");
  var $inner = $app.find(".fl-text");

  /** Initialization Function */
  var _init = function() {
    var ar = $inner
      .children()
      .first()
      .html()
      .split(" ");
    var str = "";
    ar.forEach(function(elem) {
      str += "<p><span>" + elem + "</span></p>";
    });
    $inner
      .children()
      .first()
      .replaceWith(str);
  };

  /** Lazy Load Sanctuary Why Animation */
  var _load = function() {
    lazyLoad.lazy(".why-sanctuary-animation .fl-text p", 1.3);
  };

  /** Triggers */
  $(document).on("ready", _init);
  $(window).on("scroll", _load);
})(jQuery);
