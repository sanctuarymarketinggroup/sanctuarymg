(function($) {
  // var _init = function() {
  //   setTimeout(function() {
  //     $("body").append(
  //       "<!--https://www.intercom.com/help/configure-intercom/customize-the-intercom-messenger/customize-the-intercom-messenger-technical -->"
  //     );

  //     // Enable when testing and pushing to production
  //     //

  //   }, 1500);
  // };
  // document.addEventListener("DOMContentLoaded", _init);

  // var w = window;
  // var ic = w.Intercom;
  // if (typeof ic === 'function') {
  //   ic('reattach_activator');
  //   ic('update', intercomSettings);
  //   console.log('Mikey');
  // } else {
  //   console.log('Mikey BOI!!!');
  //   var d = document;
  //   var i = function() {
  //     i.c(arguments);
  //   };
  //   i.q = [];
  //   i.c = function(args) {
  //     i.q.push(args);
  //   };
  //   w.Intercom = i;
  //   function l() {
  //     var s = d.createElement('script');
  //     s.type = 'text/javascript';
  //     s.async = true;
  //     s.src = 'https://widget.intercom.io/widget/rn6uzde6';
  //     var x = d.getElementsByTagName('script')[0];
  //     x.parentNode.insertBefore(s, x);
  //   }
  //   if (w.attachEvent) {
  //     w.attachEvent('onload', l);
  //     console.log('Kevin BOI!!!');
  //   } else {
  //     console.log('Mikey BOI 33333!!!');
  //     w.addEventListener('load', l, false);
  //   }
  // }
  $(".smg-intercom").on("click", function(event) {
    if ($(this).prop("tagName") == "A") {
      event.preventDefault();
    }
    if ($("#interloaded").length >= 1) return;
    console.log("load intercom 2");
    $(
      '<div id="interloaded"></div><script type="text/javascript" async src="https://widget.intercom.io/widget/rn6uzde6"></script><script>window.intercomSettings = { app_id: "rn6uzde6", custom_launcher_selector: ".smg-chat-now" }; </script><script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic("reattach_activator");ic("update",w.intercomSettings);}else{var d=document;var i=function(){i.c(arguments);};i.q=[];i.c=function(args){i.q.push(args);};w.Intercom=i;var l=function(){var s=d.createElement("script");s.type="text/javascript";s.async=true;s.src="";var x=d.getElementsByTagName("script")[0];x.parentNode.insertBefore(s,x);};if(w.attachEvent){w.attachEvent("onload",l);}else{w.addEventListener("load",l,false);}}})();</script>'
    ).insertAfter("#smg-intercom");

    window.Intercom("show");
  });
})(jQuery);
