(function($) {
  var lazyImageLoad = function() {
    //add_lazy_load_data_to_bb();
    var lazypartners = document.querySelectorAll(".g-partnersbadge");
    var inAdvance = 300;
    lazypartners.forEach(function(image) {
      if (image.offsetParent == null) {
        return;
      }
      if (image.classList.contains("loaded")) {
        return;
      }
      var rect = image.getBoundingClientRect(),
        scrollTop = window.pageYOffset || document.documentElement.scrollTop;
      if (rect.top < window.innerHeight + window.pageYOffset + inAdvance) {
        $(".g-partnersbadge").addClass("loaded");
        $(
          '<div id="partnerloaded"></div><script type="text/javascript" async src="//apis.google.com/js/platform.js"></script>'
        ).insertBefore(".g-partnersbadge");
      }
    });
  };
  $(document).on("ready", lazyImageLoad);
  window.addEventListener("scroll", lazyImageLoad);
})(jQuery);
