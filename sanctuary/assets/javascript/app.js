// var lazyImageLoad = function() {
//   var lazyImages = document.querySelectorAll(".lazy");
//   var inAdvance = 300;
//   lazyImages.forEach(function(image) {
//     if (image.offsetParent == null) {
//       return;
//     }
//     if (image.classList.contains("loaded")) {
//       return;
//     }
//     if (image.offsetTop < window.innerHeight + window.pageYOffset + inAdvance) {
//       if (image.dataset.src != undefined) {
//         image.src = image.dataset.src;
//       }
//       if (image.dataset.bg != undefined) {
//         image.style.backgroundImage = "url(" + image.dataset.bg + ")";
//       }
//       image.onload = function() {
//         image.classList.add("loaded");
//       };
//     }
//   });
// };
// (function($) {
//   $(document).on("ready", lazyImageLoad);
//   window.addEventListener("scroll", lazyImageLoad);
//   window.addEventListener("resize", lazyImageLoad);
// })(jQuery);
;(function ($) {
  /*
    Options:
  
  #############################
    Required
  #############################
  
    -- Slide in Side = default left
      -- Possible options "top", "left", "right", "bottom"
  
    -- Slide in Speed = default 400
      -- Options any miliseconds
  
    -- Optional Open/Close Button = default Create on for them
      -- Any object with a div or class
  
    -- Width of Menu = default 100%
      -- Any number
  
  #############################
    Non Essentails
  #############################
  
    -- Fixed or Static = Default Fixed
      -- Non essential for now
  
    -- Animation
      -- Non essential for now
  
    -- Optional Button Styling
      -- Non essential for now
  
  
  
  */
  $.fn.mobile_menu = function (options) {
    // Define Default settings:
    var defaults = {
      side: 'left',
      speed: '0.5s',
      toggler: '.menu-toggle',
      width: '100%',
      height: '100%',
      buttonStyles: true
    };
    var initals = {
      toggleButton: '<div class="menu-toggle"></div>',
      menuWrap: '<div class="offcanvas-menu-wrap"></div>'
    };
    var $toggler;
    var offcanvas;
    var $this = $(this);
    var settings = $.extend({}, initals, defaults, options);

    if (settings.toggler == defaults.toggler) {
      $this.before(settings.toggleButton);
    }
    /* Determine if you want toggler Styles */


    $toggler = $(settings.toggler);

    if (settings.buttonStyles) {
      $toggler.addClass('offcanvas-toggle');
    }

    switch (settings.side) {
      case "left":
        offcanvas = 'translateX(-100%)';
        break;

      case "top":
        offcanvas = 'translateY(-100%)';
        break;

      case "right":
        offcanvas = 'translateX(100%)';
        break;

      case "bottom":
        offcanvas = 'translateY(100%)';
        break;
    }

    var styles = {
      'transform': offcanvas,
      'transition': settings.speed,
      height: settings.height,
      width: settings.width
    };
    return this.each(function () {
      $this.addClass('offcanvas-menu').wrap(settings.menuWrap);
      $this.parent().css(styles);
      console.log($toggler);
      $toggler.on('click', function () {
        console.log('open/close');
        $(this).toggleClass('offcanvas-active');
        console.log($this);
        $this.closest('.offcanvas-menu-wrap').toggleClass('offcanvas-open');
      });
    });
  };

  $('#mobile_menu').mobile_menu({
    side: 'bottom'
  });
})(jQuery);
;(function ($) {
  if ($('#num-singles').length <= 0) return;
  var num = $('#num-singles').attr('value');
  var $noBlocks = $('.preview-nocontent-block');
  var size = window.innerWidth;
  var del;

  var _setSize = function () {
    size = window.innerWidth;
  };

  var _setDel = function () {
    del = 2;

    if (1800 <= size) {
      del = 3;
    }

    if (767 >= size) {
      del = num;
    }
  };

  var _setBlocks = function () {
    $noBlocks.hide();
    if (0 === num % del) return;

    for (var i = 0; i < del - num % del; i++) {
      $noBlocks.eq(i).show();
    }
  };

  var _change = function () {
    _setSize();

    _setDel();

    _setBlocks();
  };

  $(window).on('load', _change);
  $(window).on('resize', _change);
})(jQuery);
;(function ($) {
  var $app = $('.gform_wrapper');
  var $select = $app.find('select');
  var $captcha = $app.find('.ginput_recaptcha');
  var $readOnly = $app.find('.gf_readonly input');
  $(window).on('load', function () {
    // Actions
    $app.on('focus blur', 'input, textarea, select', _activateField);
    $(document).on('gform_post_render', _reinit); // Callbacks

    _init();
  });

  var _init = function () {
    _addClassRecaptcha();

    _addReadOnly(); // _hide_select_labels();


    _loopFields(_activateField);
  };

  var _reinit = function () {
    _loopFields(_activateField);
  };

  var _addClassRecaptcha = function () {
    $captcha.siblings('label').addClass('form-recaptcha-label');
  };

  var _hide_select_labels = function () {
    $select.closest('.gfield').find('label').hide();
  };

  var _addReadOnly = function () {
    $readOnly.attr('readonly', 'readonly');
  };

  var _loopFields = function (callback) {
    $app.find('input, select, textarea').each(function (event) {
      callback(event, this);
    });
  };

  var _activateField = function (event, target) {
    var $this;

    if (undefined !== target) {
      $this = $(target);
    } else {
      $this = $(this);
    }

    if ($this.parent().is('span')) {
      $this.closest('span').removeClass('used');
    } else {
      $this.closest('.gfield').removeClass('used');
    }

    if ('' !== $this.val() || event.type === 'focusin') {
      if ($this.parent().is('span')) {
        var $elem = $this.closest('span');
        $elem.addClass('used');
      } else {
        var $elem = $this.closest('.gfield');
        $elem.addClass('used');
      }
    }
  };
})(jQuery);
;(function ($) {
  /** Scroll to Div Via the Hash */
  var _initHashScroll = function () {
    var div = $(window.location.hash);

    if (div.length > 0) {
      _scrollToDiv(div);
    }
  };
  /** Helper Function: Scroll to Div */


  var _scrollToDiv = function (div) {
    $("html, body").animate({
      scrollTop: div.offset().top
    }, 1000);
  };
  /** Scroll To The Next Section */


  var _scrollToHrefDiv = function (event) {
    event.preventDefault();
    var div = $(this).attr("href");
    div = $(div);

    if (div.length > 0) {
      _scrollToDiv(div);
    }
  };
  /** Scroll to Next Beaver Builder Row */


  var _scrollToNextRow = function () {
    $("html, body").animate({
      scrollTop: $(this).closest(".fl-row").next(".fl-row").offset().top
    }, 1000);
  };
  /** Triggers */


  $(window).on("load", _initHashScroll);
  $(".slight-next").on("click", _scrollToHrefDiv);
  $('a[href="#next"]').on("click", _scrollToNextRow);
  $('a[href="#next-no-animation"]').on("click", _scrollToNextRow);
})(jQuery);
;(function ($) {
  /**
   * Code to Hide/Show Header
   */
  var $body = $("body");
  var $header = $("#header");
  var $menu = $("#menu"); // var $menu = $header.find('#header-menu');

  var lastScrollTop = 0;
  var mobileStopScroll = false;

  var testScroll = function () {
    if (mobileStopScroll) {
      return;
    }

    var st = $(window).scrollTop();

    if (st < lastScrollTop) {
      if ($header.hasClass("scroll")) {
        removeScroll();
      }
    } else if (st > 74) {
      if (!$header.hasClass("scroll")) {
        addScroll();
      }
    } else {
      removeScroll();
    }

    lastScrollTop = st;
  };

  var addScroll = function () {
    $header.addClass("scroll");
  };

  var removeScroll = function () {
    $header.removeClass("scroll");
  };

  var open_mobile_menu = function () {
    $(this).closest("#menu").toggleClass("open-menu");
    $(this).find(".bars").toggleClass("change");
    $("body, html").toggleClass("no-scroll");
    mobileStopScroll = mobileStopScroll ? false : true;
  };

  var activate_sub_menu = function () {
    var test = $(this).hasClass("dropdown-open");
    $(this).closest("#menu-main-menu").find(".dropdown-open").removeClass("dropdown-open");

    if (!test) {
      $(this).addClass("dropdown-open");
      $(this).siblings(".dropdown").toggleClass("dropdown-open");
    }
  };

  $(window).on("scroll", testScroll); // if ($(window).width() < 1200) {
  //   $menu.find("#mobile-menu").on("click", open_mobile_menu);
  //   $menu.find(".dropdown-button").on("click", activate_sub_menu);
  // }

  var init_menu = function () {
    if ($(window).width() < 1200) {
      $menu.find("#mobile-menu").on("click", open_mobile_menu);
      $menu.find(".dropdown-button").on("click", activate_sub_menu);
    }
  };

  $(document).ready(init_menu);
  $(window).on('resize', init_menu);
  /**
   * code to schedule meeting popup
   */
  // $lightbox = $("#smg-lb");
  // var open_lightbox = function(event) {
  //   event.preventDefault();
  //   $lightbox.addClass("active");
  //   $lightbox
  //     .find("#smg-lb-cont")
  //     .html(
  //       '<script src="https://cdn.scheduleonce.com/mergedjs/so.js"></script><!--ScheduleOnce embed START--><div id="SOIDIV_TomMurphy" data-so-page="TomMurphy" data-height="550" data-style="border: 1px solid #d8d8d8; min-width: 290px; max-width: 900px;" data-psz="00"></div><!--ScheduleOnce embed END-->'
  //     );
  // };
  // var close_lightbox = function(event) {
  //   $lightbox.removeClass("active");
  //   $lightbox.find("#smg-lb-cont").html("");
  // };
  // $lightbox.on("click", close_lightbox);
  // $lightbox.find("#smg-close").on("click", close_lightbox);
  // $body.find(".sch-meet").on("click", open_lightbox);
})(jQuery);
;var lazyLoad = function ($) {
  var _lazyLoad = function (elem, num) {
    var $elem = $(".smg-lazy-load");
    var divider = 2; // console.log(elem);

    if (typeof num === "number") {
      divider = num;
    }

    if (typeof elem === "string") {
      $elem = $(elem);
    }

    var height = window.innerHeight;
    var width = window.innerWidth;
    $elem.each(function () {
      if ($(this).hasClass("active")) {
        return true;
      }

      var rect = this.getBoundingClientRect();

      if (rect.top < height / divider) {
        // console.log(rect);
        $(this).addClass("active");
      }
    });
  };

  $(window).on("scroll", _lazyLoad);
  return {
    lazy: _lazyLoad
  };
}(jQuery);
;(function ($) {
  if ($(".lightbox-smg").length < 1) {
    return;
  }
  /** Define Variables */


  var $app = $(".lightbox-smg");
  /** Load Video Function */

  var _loadVideo = function (event) {
    event.preventDefault();
    $app.toggleClass("active");
    $lightVid = $(this).data("vid");
    $lightPhoto = $(this).data("lightbox");

    if ($lightVid) {
      $app.find(".lightbox-content").html('<div class="responsive-iframe"><iframe width="560" height="315" frameborder="0" allowfullscreen src="https://www.youtube.com/embed/' + $lightVid + '?rel=0&autoplay=1"></iframe></div>');
    } else if ($lightPhoto) {
      $app.find(".lightbox-content").html("<img src='" + $lightPhoto + "' />");
    }
  };
  /** Stop Video Function */


  var _stopPlayer = function () {
    $app.toggleClass("active");
    $app.find("iframe").attr("src", "");
  };
  /** Triggers */


  $(".light-popup").on("click", _loadVideo);
  $app.on("click", ".overlay, .lightbox-close", _stopPlayer);
})(jQuery);
;// (function($) {
//   var $schMeet =  $('.sch-meet');
//   var load_on_click = function(){
//      < !--ScheduleOnce embed START-- >
//       <div id="SOIDIV_TomMurphy" data-so-page="TomMurphy" data-height="550" data-style="border: 1px solid #d8d8d8; min-width: 290px; max-width: 900px;" data-psz="00"></div>
//       <!--ScheduleOnce embed END-- >
//   }
//   $schMeet.on('click', load_on_click);
//   // var _init = function() {
//   //   setTimeout(function() {
//   //     $("body").append(
//   //       "<script src='https://cdn.scheduleonce.com/mergedjs/so.js'></script>"
//   //     );
//   //     // Enable when testing and pushing to production
//   //     //
//   //     // $.getScript({
//   //     //   url: "https://cdn.scheduleonce.com/mergedjs/so.js",
//   //     //   cache: true
//   //     // });
//   //   }, 1500);
//   // };
//   // document.addEventListener("DOMContentLoaded", _init);
// })(jQuery);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImpxdWVyeS5sYXp5LWltYWdlLmpzIiwibW9iaWxlLW1lbnUuanMiLCJhcmNoaXZlLmpzIiwiZm9ybXMuanMiLCJnZW5lcmFsLmpzIiwiaGVhZGVyLmpzIiwibGF6eS1sb2FkZXIuanMiLCJsaWdodGJveC5qcyIsInNjaGVkdWxlT25jZS5qcyJdLCJuYW1lcyI6WyIkIiwiZm4iLCJtb2JpbGVfbWVudSIsIm9wdGlvbnMiLCJkZWZhdWx0cyIsInNpZGUiLCJzcGVlZCIsInRvZ2dsZXIiLCJ3aWR0aCIsImhlaWdodCIsImJ1dHRvblN0eWxlcyIsImluaXRhbHMiLCJ0b2dnbGVCdXR0b24iLCJtZW51V3JhcCIsIiR0b2dnbGVyIiwib2ZmY2FudmFzIiwiJHRoaXMiLCJzZXR0aW5ncyIsImV4dGVuZCIsImJlZm9yZSIsImFkZENsYXNzIiwic3R5bGVzIiwiZWFjaCIsIndyYXAiLCJwYXJlbnQiLCJjc3MiLCJjb25zb2xlIiwibG9nIiwib24iLCJ0b2dnbGVDbGFzcyIsImNsb3Nlc3QiLCJqUXVlcnkiLCJsZW5ndGgiLCJudW0iLCJhdHRyIiwiJG5vQmxvY2tzIiwic2l6ZSIsIndpbmRvdyIsImlubmVyV2lkdGgiLCJkZWwiLCJfc2V0U2l6ZSIsIl9zZXREZWwiLCJfc2V0QmxvY2tzIiwiaGlkZSIsImkiLCJlcSIsInNob3ciLCJfY2hhbmdlIiwiJGFwcCIsIiRzZWxlY3QiLCJmaW5kIiwiJGNhcHRjaGEiLCIkcmVhZE9ubHkiLCJfYWN0aXZhdGVGaWVsZCIsImRvY3VtZW50IiwiX3JlaW5pdCIsIl9pbml0IiwiX2FkZENsYXNzUmVjYXB0Y2hhIiwiX2FkZFJlYWRPbmx5IiwiX2xvb3BGaWVsZHMiLCJzaWJsaW5ncyIsIl9oaWRlX3NlbGVjdF9sYWJlbHMiLCJjYWxsYmFjayIsImV2ZW50IiwidGFyZ2V0IiwidW5kZWZpbmVkIiwiaXMiLCJyZW1vdmVDbGFzcyIsInZhbCIsInR5cGUiLCIkZWxlbSIsIl9pbml0SGFzaFNjcm9sbCIsImRpdiIsImxvY2F0aW9uIiwiaGFzaCIsIl9zY3JvbGxUb0RpdiIsImFuaW1hdGUiLCJzY3JvbGxUb3AiLCJvZmZzZXQiLCJ0b3AiLCJfc2Nyb2xsVG9IcmVmRGl2IiwicHJldmVudERlZmF1bHQiLCJfc2Nyb2xsVG9OZXh0Um93IiwibmV4dCIsIiRib2R5IiwiJGhlYWRlciIsIiRtZW51IiwibGFzdFNjcm9sbFRvcCIsIm1vYmlsZVN0b3BTY3JvbGwiLCJ0ZXN0U2Nyb2xsIiwic3QiLCJoYXNDbGFzcyIsInJlbW92ZVNjcm9sbCIsImFkZFNjcm9sbCIsIm9wZW5fbW9iaWxlX21lbnUiLCJhY3RpdmF0ZV9zdWJfbWVudSIsInRlc3QiLCJpbml0X21lbnUiLCJyZWFkeSIsImxhenlMb2FkIiwiX2xhenlMb2FkIiwiZWxlbSIsImRpdmlkZXIiLCJpbm5lckhlaWdodCIsInJlY3QiLCJnZXRCb3VuZGluZ0NsaWVudFJlY3QiLCJsYXp5IiwiX2xvYWRWaWRlbyIsIiRsaWdodFZpZCIsImRhdGEiLCIkbGlnaHRQaG90byIsImh0bWwiLCJfc3RvcFBsYXllciJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Q0M1QkEsQ0FBQyxVQUFTQSxDQUFULEVBQVc7QUFFWjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFvQ0VBLEVBQUFBLENBQUMsQ0FBQ0MsRUFBRixDQUFLQyxXQUFMLEdBQW1CLFVBQVdDLE9BQVgsRUFBcUI7QUFFdEM7QUFDQSxRQUFJQyxRQUFRLEdBQUc7QUFDYkMsTUFBQUEsSUFBSSxFQUFFLE1BRE87QUFFYkMsTUFBQUEsS0FBSyxFQUFFLE1BRk07QUFHYkMsTUFBQUEsT0FBTyxFQUFFLGNBSEk7QUFJYkMsTUFBQUEsS0FBSyxFQUFFLE1BSk07QUFLYkMsTUFBQUEsTUFBTSxFQUFFLE1BTEs7QUFNYkMsTUFBQUEsWUFBWSxFQUFFO0FBTkQsS0FBZjtBQVNBLFFBQUlDLE9BQU8sR0FBRztBQUNaQyxNQUFBQSxZQUFZLEVBQUUsaUNBREY7QUFFWkMsTUFBQUEsUUFBUSxFQUFFO0FBRkUsS0FBZDtBQUtBLFFBQUlDLFFBQUo7QUFDQSxRQUFJQyxTQUFKO0FBRUEsUUFBSUMsS0FBSyxHQUFHaEIsQ0FBQyxDQUFDLElBQUQsQ0FBYjtBQUVBLFFBQUlpQixRQUFRLEdBQUdqQixDQUFDLENBQUNrQixNQUFGLENBQVUsRUFBVixFQUFjUCxPQUFkLEVBQXVCUCxRQUF2QixFQUFpQ0QsT0FBakMsQ0FBZjs7QUFFQSxRQUFHYyxRQUFRLENBQUNWLE9BQVQsSUFBb0JILFFBQVEsQ0FBQ0csT0FBaEMsRUFBd0M7QUFDdENTLE1BQUFBLEtBQUssQ0FBQ0csTUFBTixDQUFhRixRQUFRLENBQUNMLFlBQXRCO0FBQ0Q7QUFFRDs7O0FBQ0FFLElBQUFBLFFBQVEsR0FBR2QsQ0FBQyxDQUFDaUIsUUFBUSxDQUFDVixPQUFWLENBQVo7O0FBQ0EsUUFBR1UsUUFBUSxDQUFDUCxZQUFaLEVBQXlCO0FBQ3ZCSSxNQUFBQSxRQUFRLENBQUNNLFFBQVQsQ0FBa0Isa0JBQWxCO0FBQ0Q7O0FBRUQsWUFBU0gsUUFBUSxDQUFDWixJQUFsQjtBQUNFLFdBQUssTUFBTDtBQUNFVSxRQUFBQSxTQUFTLEdBQUcsbUJBQVo7QUFDQTs7QUFDRixXQUFLLEtBQUw7QUFDRUEsUUFBQUEsU0FBUyxHQUFHLG1CQUFaO0FBQ0E7O0FBQ0YsV0FBSyxPQUFMO0FBQ0VBLFFBQUFBLFNBQVMsR0FBRyxrQkFBWjtBQUNBOztBQUNGLFdBQUssUUFBTDtBQUNFQSxRQUFBQSxTQUFTLEdBQUcsa0JBQVo7QUFDQTtBQVpKOztBQWVBLFFBQUlNLE1BQU0sR0FBRztBQUNYLG1CQUFjTixTQURIO0FBRVgsb0JBQWVFLFFBQVEsQ0FBQ1gsS0FGYjtBQUdYRyxNQUFBQSxNQUFNLEVBQUdRLFFBQVEsQ0FBQ1IsTUFIUDtBQUlYRCxNQUFBQSxLQUFLLEVBQUdTLFFBQVEsQ0FBQ1Q7QUFKTixLQUFiO0FBT0EsV0FBTyxLQUFLYyxJQUFMLENBQVUsWUFBVztBQUV4Qk4sTUFBQUEsS0FBSyxDQUFDSSxRQUFOLENBQWUsZ0JBQWYsRUFBaUNHLElBQWpDLENBQXNDTixRQUFRLENBQUNKLFFBQS9DO0FBQ0FHLE1BQUFBLEtBQUssQ0FBQ1EsTUFBTixHQUFlQyxHQUFmLENBQW9CSixNQUFwQjtBQUNBSyxNQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWWIsUUFBWjtBQUNBQSxNQUFBQSxRQUFRLENBQUNjLEVBQVQsQ0FBWSxPQUFaLEVBQXFCLFlBQVU7QUFDN0JGLFFBQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLFlBQVo7QUFFQTNCLFFBQUFBLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUTZCLFdBQVIsQ0FBb0Isa0JBQXBCO0FBQ0FILFFBQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZWCxLQUFaO0FBQ0FBLFFBQUFBLEtBQUssQ0FBQ2MsT0FBTixDQUFjLHNCQUFkLEVBQXNDRCxXQUF0QyxDQUFrRCxnQkFBbEQ7QUFFRCxPQVBEO0FBVUgsS0FmTSxDQUFQO0FBZ0JELEdBeEVEOztBQTJFQTdCLEVBQUFBLENBQUMsQ0FBQyxjQUFELENBQUQsQ0FBa0JFLFdBQWxCLENBQThCO0FBQzVCRyxJQUFBQSxJQUFJLEVBQUU7QUFEc0IsR0FBOUI7QUFLRCxDQXRIRCxFQXNIRzBCLE1BdEhIO0NDQUEsQ0FBQyxVQUFTL0IsQ0FBVCxFQUFZO0FBQ1gsTUFBSUEsQ0FBQyxDQUFDLGNBQUQsQ0FBRCxDQUFrQmdDLE1BQWxCLElBQTRCLENBQWhDLEVBQW1DO0FBQ25DLE1BQUlDLEdBQUcsR0FBR2pDLENBQUMsQ0FBQyxjQUFELENBQUQsQ0FBa0JrQyxJQUFsQixDQUF1QixPQUF2QixDQUFWO0FBQ0EsTUFBSUMsU0FBUyxHQUFHbkMsQ0FBQyxDQUFDLDBCQUFELENBQWpCO0FBQ0EsTUFBSW9DLElBQUksR0FBR0MsTUFBTSxDQUFDQyxVQUFsQjtBQUNBLE1BQUlDLEdBQUo7O0FBRUEsTUFBSUMsUUFBUSxHQUFHLFlBQVc7QUFDeEJKLElBQUFBLElBQUksR0FBR0MsTUFBTSxDQUFDQyxVQUFkO0FBQ0QsR0FGRDs7QUFJQSxNQUFJRyxPQUFPLEdBQUcsWUFBVztBQUN2QkYsSUFBQUEsR0FBRyxHQUFHLENBQU47O0FBQ0EsUUFBSSxRQUFRSCxJQUFaLEVBQWtCO0FBQ2hCRyxNQUFBQSxHQUFHLEdBQUcsQ0FBTjtBQUNEOztBQUNELFFBQUksT0FBT0gsSUFBWCxFQUFpQjtBQUNmRyxNQUFBQSxHQUFHLEdBQUdOLEdBQU47QUFDRDtBQUNGLEdBUkQ7O0FBVUEsTUFBSVMsVUFBVSxHQUFHLFlBQVc7QUFDMUJQLElBQUFBLFNBQVMsQ0FBQ1EsSUFBVjtBQUNBLFFBQUksTUFBTVYsR0FBRyxHQUFHTSxHQUFoQixFQUFxQjs7QUFDckIsU0FBSyxJQUFJSyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHTCxHQUFHLEdBQUlOLEdBQUcsR0FBR00sR0FBakMsRUFBdUNLLENBQUMsRUFBeEMsRUFBNEM7QUFDMUNULE1BQUFBLFNBQVMsQ0FBQ1UsRUFBVixDQUFhRCxDQUFiLEVBQWdCRSxJQUFoQjtBQUNEO0FBQ0YsR0FORDs7QUFRQSxNQUFJQyxPQUFPLEdBQUcsWUFBVztBQUN2QlAsSUFBQUEsUUFBUTs7QUFDUkMsSUFBQUEsT0FBTzs7QUFDUEMsSUFBQUEsVUFBVTtBQUNYLEdBSkQ7O0FBTUExQyxFQUFBQSxDQUFDLENBQUNxQyxNQUFELENBQUQsQ0FBVVQsRUFBVixDQUFhLE1BQWIsRUFBcUJtQixPQUFyQjtBQUNBL0MsRUFBQUEsQ0FBQyxDQUFDcUMsTUFBRCxDQUFELENBQVVULEVBQVYsQ0FBYSxRQUFiLEVBQXVCbUIsT0FBdkI7QUFDRCxDQXJDRCxFQXFDR2hCLE1BckNIO0NDQUEsQ0FBQyxVQUFVL0IsQ0FBVixFQUFhO0FBQ1osTUFBSWdELElBQUksR0FBR2hELENBQUMsQ0FBQyxnQkFBRCxDQUFaO0FBQ0EsTUFBSWlELE9BQU8sR0FBR0QsSUFBSSxDQUFDRSxJQUFMLENBQVUsUUFBVixDQUFkO0FBQ0EsTUFBSUMsUUFBUSxHQUFHSCxJQUFJLENBQUNFLElBQUwsQ0FBVSxtQkFBVixDQUFmO0FBQ0EsTUFBSUUsU0FBUyxHQUFHSixJQUFJLENBQUNFLElBQUwsQ0FBVSxvQkFBVixDQUFoQjtBQUVBbEQsRUFBQUEsQ0FBQyxDQUFDcUMsTUFBRCxDQUFELENBQVVULEVBQVYsQ0FBYSxNQUFiLEVBQXFCLFlBQVk7QUFDL0I7QUFDQW9CLElBQUFBLElBQUksQ0FBQ3BCLEVBQUwsQ0FBUSxZQUFSLEVBQXNCLHlCQUF0QixFQUFpRHlCLGNBQWpEO0FBQ0FyRCxJQUFBQSxDQUFDLENBQUNzRCxRQUFELENBQUQsQ0FBWTFCLEVBQVosQ0FBZSxtQkFBZixFQUFvQzJCLE9BQXBDLEVBSCtCLENBSy9COztBQUNBQyxJQUFBQSxLQUFLO0FBQ04sR0FQRDs7QUFTQSxNQUFJQSxLQUFLLEdBQUcsWUFBWTtBQUN0QkMsSUFBQUEsa0JBQWtCOztBQUNsQkMsSUFBQUEsWUFBWSxHQUZVLENBR3RCOzs7QUFDQUMsSUFBQUEsV0FBVyxDQUFDTixjQUFELENBQVg7QUFDRCxHQUxEOztBQU9BLE1BQUlFLE9BQU8sR0FBRyxZQUFZO0FBQ3hCSSxJQUFBQSxXQUFXLENBQUNOLGNBQUQsQ0FBWDtBQUNELEdBRkQ7O0FBSUEsTUFBSUksa0JBQWtCLEdBQUcsWUFBWTtBQUNuQ04sSUFBQUEsUUFBUSxDQUFDUyxRQUFULENBQWtCLE9BQWxCLEVBQTJCeEMsUUFBM0IsQ0FBb0Msc0JBQXBDO0FBQ0QsR0FGRDs7QUFJQSxNQUFJeUMsbUJBQW1CLEdBQUcsWUFBWTtBQUNwQ1osSUFBQUEsT0FBTyxDQUVKbkIsT0FGSCxDQUVXLFNBRlgsRUFJR29CLElBSkgsQ0FJUSxPQUpSLEVBTUdQLElBTkg7QUFPRCxHQVJEOztBQVVBLE1BQUllLFlBQVksR0FBRyxZQUFZO0FBQzdCTixJQUFBQSxTQUFTLENBQUNsQixJQUFWLENBQWUsVUFBZixFQUEyQixVQUEzQjtBQUNELEdBRkQ7O0FBSUEsTUFBSXlCLFdBQVcsR0FBRyxVQUFVRyxRQUFWLEVBQW9CO0FBQ3BDZCxJQUFBQSxJQUFJLENBQUNFLElBQUwsQ0FBVSx5QkFBVixFQUFxQzVCLElBQXJDLENBQTBDLFVBQVV5QyxLQUFWLEVBQWlCO0FBQ3pERCxNQUFBQSxRQUFRLENBQUNDLEtBQUQsRUFBUSxJQUFSLENBQVI7QUFDRCxLQUZEO0FBR0QsR0FKRDs7QUFNQSxNQUFJVixjQUFjLEdBQUcsVUFBVVUsS0FBVixFQUFpQkMsTUFBakIsRUFBeUI7QUFDNUMsUUFBSWhELEtBQUo7O0FBRUEsUUFBSWlELFNBQVMsS0FBS0QsTUFBbEIsRUFBMEI7QUFDeEJoRCxNQUFBQSxLQUFLLEdBQUdoQixDQUFDLENBQUNnRSxNQUFELENBQVQ7QUFDRCxLQUZELE1BRU87QUFDTGhELE1BQUFBLEtBQUssR0FBR2hCLENBQUMsQ0FBQyxJQUFELENBQVQ7QUFDRDs7QUFFRCxRQUFJZ0IsS0FBSyxDQUFDUSxNQUFOLEdBQWUwQyxFQUFmLENBQWtCLE1BQWxCLENBQUosRUFBK0I7QUFDN0JsRCxNQUFBQSxLQUFLLENBQUNjLE9BQU4sQ0FBYyxNQUFkLEVBQXNCcUMsV0FBdEIsQ0FBa0MsTUFBbEM7QUFDRCxLQUZELE1BRU87QUFDTG5ELE1BQUFBLEtBQUssQ0FBQ2MsT0FBTixDQUFjLFNBQWQsRUFBeUJxQyxXQUF6QixDQUFxQyxNQUFyQztBQUNEOztBQUVELFFBQUksT0FBT25ELEtBQUssQ0FBQ29ELEdBQU4sRUFBUCxJQUFzQkwsS0FBSyxDQUFDTSxJQUFOLEtBQWUsU0FBekMsRUFBb0Q7QUFDbEQsVUFBSXJELEtBQUssQ0FBQ1EsTUFBTixHQUFlMEMsRUFBZixDQUFrQixNQUFsQixDQUFKLEVBQStCO0FBQzdCLFlBQUlJLEtBQUssR0FBR3RELEtBQUssQ0FBQ2MsT0FBTixDQUFjLE1BQWQsQ0FBWjtBQUNBd0MsUUFBQUEsS0FBSyxDQUFDbEQsUUFBTixDQUFlLE1BQWY7QUFDRCxPQUhELE1BR087QUFDTCxZQUFJa0QsS0FBSyxHQUFHdEQsS0FBSyxDQUFDYyxPQUFOLENBQWMsU0FBZCxDQUFaO0FBQ0F3QyxRQUFBQSxLQUFLLENBQUNsRCxRQUFOLENBQWUsTUFBZjtBQUNEO0FBQ0Y7QUFDRixHQXhCRDtBQXlCRCxDQTNFRCxFQTJFR1csTUEzRUg7Q0NBQSxDQUFDLFVBQVMvQixDQUFULEVBQVk7QUFDWDtBQUNBLE1BQUl1RSxlQUFlLEdBQUcsWUFBVztBQUMvQixRQUFJQyxHQUFHLEdBQUd4RSxDQUFDLENBQUNxQyxNQUFNLENBQUNvQyxRQUFQLENBQWdCQyxJQUFqQixDQUFYOztBQUNBLFFBQUlGLEdBQUcsQ0FBQ3hDLE1BQUosR0FBYSxDQUFqQixFQUFvQjtBQUNsQjJDLE1BQUFBLFlBQVksQ0FBQ0gsR0FBRCxDQUFaO0FBQ0Q7QUFDRixHQUxEO0FBT0E7OztBQUNBLE1BQUlHLFlBQVksR0FBRyxVQUFTSCxHQUFULEVBQWM7QUFDL0J4RSxJQUFBQSxDQUFDLENBQUMsWUFBRCxDQUFELENBQWdCNEUsT0FBaEIsQ0FDRTtBQUNFQyxNQUFBQSxTQUFTLEVBQUVMLEdBQUcsQ0FBQ00sTUFBSixHQUFhQztBQUQxQixLQURGLEVBSUUsSUFKRjtBQU1ELEdBUEQ7QUFTQTs7O0FBQ0EsTUFBSUMsZ0JBQWdCLEdBQUcsVUFBU2pCLEtBQVQsRUFBZ0I7QUFDckNBLElBQUFBLEtBQUssQ0FBQ2tCLGNBQU47QUFDQSxRQUFJVCxHQUFHLEdBQUd4RSxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFrQyxJQUFSLENBQWEsTUFBYixDQUFWO0FBQ0FzQyxJQUFBQSxHQUFHLEdBQUd4RSxDQUFDLENBQUN3RSxHQUFELENBQVA7O0FBQ0EsUUFBSUEsR0FBRyxDQUFDeEMsTUFBSixHQUFhLENBQWpCLEVBQW9CO0FBQ2xCMkMsTUFBQUEsWUFBWSxDQUFDSCxHQUFELENBQVo7QUFDRDtBQUNGLEdBUEQ7QUFTQTs7O0FBQ0EsTUFBSVUsZ0JBQWdCLEdBQUcsWUFBVztBQUNoQ2xGLElBQUFBLENBQUMsQ0FBQyxZQUFELENBQUQsQ0FBZ0I0RSxPQUFoQixDQUNFO0FBQ0VDLE1BQUFBLFNBQVMsRUFBRTdFLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FDUjhCLE9BRFEsQ0FDQSxTQURBLEVBRVJxRCxJQUZRLENBRUgsU0FGRyxFQUdSTCxNQUhRLEdBR0NDO0FBSmQsS0FERixFQU9FLElBUEY7QUFTRCxHQVZEO0FBWUE7OztBQUNBL0UsRUFBQUEsQ0FBQyxDQUFDcUMsTUFBRCxDQUFELENBQVVULEVBQVYsQ0FBYSxNQUFiLEVBQXFCMkMsZUFBckI7QUFDQXZFLEVBQUFBLENBQUMsQ0FBQyxjQUFELENBQUQsQ0FBa0I0QixFQUFsQixDQUFxQixPQUFyQixFQUE4Qm9ELGdCQUE5QjtBQUNBaEYsRUFBQUEsQ0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUI0QixFQUFyQixDQUF3QixPQUF4QixFQUFpQ3NELGdCQUFqQztBQUNBbEYsRUFBQUEsQ0FBQyxDQUFDLDhCQUFELENBQUQsQ0FBa0M0QixFQUFsQyxDQUFxQyxPQUFyQyxFQUE4Q3NELGdCQUE5QztBQUNELENBL0NELEVBK0NHbkQsTUEvQ0g7Q0NBQSxDQUFDLFVBQVUvQixDQUFWLEVBQWE7QUFDWjs7O0FBR0EsTUFBSW9GLEtBQUssR0FBR3BGLENBQUMsQ0FBQyxNQUFELENBQWI7QUFDQSxNQUFJcUYsT0FBTyxHQUFHckYsQ0FBQyxDQUFDLFNBQUQsQ0FBZjtBQUNBLE1BQUlzRixLQUFLLEdBQUd0RixDQUFDLENBQUMsT0FBRCxDQUFiLENBTlksQ0FPWjs7QUFDQSxNQUFJdUYsYUFBYSxHQUFHLENBQXBCO0FBQ0EsTUFBSUMsZ0JBQWdCLEdBQUcsS0FBdkI7O0FBRUEsTUFBSUMsVUFBVSxHQUFHLFlBQVk7QUFDM0IsUUFBSUQsZ0JBQUosRUFBc0I7QUFDcEI7QUFDRDs7QUFDRCxRQUFJRSxFQUFFLEdBQUcxRixDQUFDLENBQUNxQyxNQUFELENBQUQsQ0FBVXdDLFNBQVYsRUFBVDs7QUFDQSxRQUFJYSxFQUFFLEdBQUdILGFBQVQsRUFBd0I7QUFDdEIsVUFBSUYsT0FBTyxDQUFDTSxRQUFSLENBQWlCLFFBQWpCLENBQUosRUFBZ0M7QUFDOUJDLFFBQUFBLFlBQVk7QUFDYjtBQUNGLEtBSkQsTUFJTyxJQUFJRixFQUFFLEdBQUcsRUFBVCxFQUFhO0FBQ2xCLFVBQUksQ0FBQ0wsT0FBTyxDQUFDTSxRQUFSLENBQWlCLFFBQWpCLENBQUwsRUFBaUM7QUFDL0JFLFFBQUFBLFNBQVM7QUFDVjtBQUNGLEtBSk0sTUFJQTtBQUNMRCxNQUFBQSxZQUFZO0FBQ2I7O0FBQ0RMLElBQUFBLGFBQWEsR0FBR0csRUFBaEI7QUFDRCxHQWpCRDs7QUFtQkEsTUFBSUcsU0FBUyxHQUFHLFlBQVk7QUFDMUJSLElBQUFBLE9BQU8sQ0FBQ2pFLFFBQVIsQ0FBaUIsUUFBakI7QUFDRCxHQUZEOztBQUlBLE1BQUl3RSxZQUFZLEdBQUcsWUFBWTtBQUM3QlAsSUFBQUEsT0FBTyxDQUFDbEIsV0FBUixDQUFvQixRQUFwQjtBQUNELEdBRkQ7O0FBSUEsTUFBSTJCLGdCQUFnQixHQUFHLFlBQVk7QUFDakM5RixJQUFBQSxDQUFDLENBQUMsSUFBRCxDQUFELENBQ0c4QixPQURILENBQ1csT0FEWCxFQUVHRCxXQUZILENBRWUsV0FGZjtBQUdBN0IsSUFBQUEsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUNHa0QsSUFESCxDQUNRLE9BRFIsRUFFR3JCLFdBRkgsQ0FFZSxRQUZmO0FBR0E3QixJQUFBQSxDQUFDLENBQUMsWUFBRCxDQUFELENBQWdCNkIsV0FBaEIsQ0FBNEIsV0FBNUI7QUFDQTJELElBQUFBLGdCQUFnQixHQUFHQSxnQkFBZ0IsR0FBRyxLQUFILEdBQVcsSUFBOUM7QUFDRCxHQVREOztBQVdBLE1BQUlPLGlCQUFpQixHQUFHLFlBQVk7QUFDbEMsUUFBSUMsSUFBSSxHQUFHaEcsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRMkYsUUFBUixDQUFpQixlQUFqQixDQUFYO0FBQ0EzRixJQUFBQSxDQUFDLENBQUMsSUFBRCxDQUFELENBQ0c4QixPQURILENBQ1csaUJBRFgsRUFFR29CLElBRkgsQ0FFUSxnQkFGUixFQUdHaUIsV0FISCxDQUdlLGVBSGY7O0FBSUEsUUFBSSxDQUFDNkIsSUFBTCxFQUFXO0FBQ1RoRyxNQUFBQSxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFvQixRQUFSLENBQWlCLGVBQWpCO0FBQ0FwQixNQUFBQSxDQUFDLENBQUMsSUFBRCxDQUFELENBQ0c0RCxRQURILENBQ1ksV0FEWixFQUVHL0IsV0FGSCxDQUVlLGVBRmY7QUFHRDtBQUNGLEdBWkQ7O0FBY0E3QixFQUFBQSxDQUFDLENBQUNxQyxNQUFELENBQUQsQ0FBVVQsRUFBVixDQUFhLFFBQWIsRUFBdUI2RCxVQUF2QixFQS9EWSxDQWdFWjtBQUNBO0FBQ0E7QUFDQTs7QUFLQSxNQUFJUSxTQUFTLEdBQUcsWUFBVztBQUN6QixRQUFJakcsQ0FBQyxDQUFDcUMsTUFBRCxDQUFELENBQVU3QixLQUFWLEtBQW9CLElBQXhCLEVBQThCO0FBQzVCOEUsTUFBQUEsS0FBSyxDQUFDcEMsSUFBTixDQUFXLGNBQVgsRUFBMkJ0QixFQUEzQixDQUE4QixPQUE5QixFQUF1Q2tFLGdCQUF2QztBQUNBUixNQUFBQSxLQUFLLENBQUNwQyxJQUFOLENBQVcsa0JBQVgsRUFBK0J0QixFQUEvQixDQUFrQyxPQUFsQyxFQUEyQ21FLGlCQUEzQztBQUNEO0FBQ0YsR0FMRDs7QUFPQS9GLEVBQUFBLENBQUMsQ0FBQ3NELFFBQUQsQ0FBRCxDQUFZNEMsS0FBWixDQUFrQkQsU0FBbEI7QUFDQWpHLEVBQUFBLENBQUMsQ0FBQ3FDLE1BQUQsQ0FBRCxDQUFVVCxFQUFWLENBQWEsUUFBYixFQUF1QnFFLFNBQXZCO0FBQ0E7OztBQUdBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDRCxDQXhHRCxFQXdHR2xFLE1BeEdIO0NDQUEsSUFBSW9FLFFBQVEsR0FBSSxVQUFTbkcsQ0FBVCxFQUFZO0FBQzFCLE1BQUlvRyxTQUFTLEdBQUcsVUFBU0MsSUFBVCxFQUFlcEUsR0FBZixFQUFvQjtBQUNsQyxRQUFJcUMsS0FBSyxHQUFHdEUsQ0FBQyxDQUFDLGdCQUFELENBQWI7QUFDQSxRQUFJc0csT0FBTyxHQUFHLENBQWQsQ0FGa0MsQ0FJbEM7O0FBRUEsUUFBSSxPQUFPckUsR0FBUCxLQUFlLFFBQW5CLEVBQTZCO0FBQzNCcUUsTUFBQUEsT0FBTyxHQUFHckUsR0FBVjtBQUNEOztBQUVELFFBQUksT0FBT29FLElBQVAsS0FBZ0IsUUFBcEIsRUFBOEI7QUFDNUIvQixNQUFBQSxLQUFLLEdBQUd0RSxDQUFDLENBQUNxRyxJQUFELENBQVQ7QUFDRDs7QUFFRCxRQUFJNUYsTUFBTSxHQUFHNEIsTUFBTSxDQUFDa0UsV0FBcEI7QUFDQSxRQUFJL0YsS0FBSyxHQUFHNkIsTUFBTSxDQUFDQyxVQUFuQjtBQUVBZ0MsSUFBQUEsS0FBSyxDQUFDaEQsSUFBTixDQUFXLFlBQVc7QUFDcEIsVUFBSXRCLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUTJGLFFBQVIsQ0FBaUIsUUFBakIsQ0FBSixFQUFnQztBQUM5QixlQUFPLElBQVA7QUFDRDs7QUFFRCxVQUFJYSxJQUFJLEdBQUcsS0FBS0MscUJBQUwsRUFBWDs7QUFDQSxVQUFJRCxJQUFJLENBQUN6QixHQUFMLEdBQVd0RSxNQUFNLEdBQUc2RixPQUF4QixFQUFpQztBQUMvQjtBQUNBdEcsUUFBQUEsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRb0IsUUFBUixDQUFpQixRQUFqQjtBQUNEO0FBQ0YsS0FWRDtBQVdELEdBNUJEOztBQThCQXBCLEVBQUFBLENBQUMsQ0FBQ3FDLE1BQUQsQ0FBRCxDQUFVVCxFQUFWLENBQWEsUUFBYixFQUF1QndFLFNBQXZCO0FBRUEsU0FBTztBQUNMTSxJQUFBQSxJQUFJLEVBQUVOO0FBREQsR0FBUDtBQUdELENBcENjLENBb0NackUsTUFwQ1ksQ0FBZjtDQ0FBLENBQUMsVUFBUy9CLENBQVQsRUFBWTtBQUNYLE1BQUlBLENBQUMsQ0FBQyxlQUFELENBQUQsQ0FBbUJnQyxNQUFuQixHQUE0QixDQUFoQyxFQUFtQztBQUNqQztBQUNEO0FBRUQ7OztBQUNBLE1BQUlnQixJQUFJLEdBQUdoRCxDQUFDLENBQUMsZUFBRCxDQUFaO0FBRUE7O0FBQ0EsTUFBSTJHLFVBQVUsR0FBRyxVQUFTNUMsS0FBVCxFQUFnQjtBQUMvQkEsSUFBQUEsS0FBSyxDQUFDa0IsY0FBTjtBQUNBakMsSUFBQUEsSUFBSSxDQUFDbkIsV0FBTCxDQUFpQixRQUFqQjtBQUNBK0UsSUFBQUEsU0FBUyxHQUFHNUcsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRNkcsSUFBUixDQUFhLEtBQWIsQ0FBWjtBQUNBQyxJQUFBQSxXQUFXLEdBQUc5RyxDQUFDLENBQUMsSUFBRCxDQUFELENBQVE2RyxJQUFSLENBQWEsVUFBYixDQUFkOztBQUNBLFFBQUlELFNBQUosRUFBZTtBQUNiNUQsTUFBQUEsSUFBSSxDQUNERSxJQURILENBQ1EsbUJBRFIsRUFFRzZELElBRkgsQ0FHSSx3SUFDRUgsU0FERixHQUVFLG9DQUxOO0FBT0QsS0FSRCxNQVFPLElBQUlFLFdBQUosRUFBaUI7QUFDdEI5RCxNQUFBQSxJQUFJLENBQUNFLElBQUwsQ0FBVSxtQkFBVixFQUErQjZELElBQS9CLENBQW9DLGVBQWVELFdBQWYsR0FBNkIsTUFBakU7QUFDRDtBQUNGLEdBaEJEO0FBa0JBOzs7QUFDQSxNQUFJRSxXQUFXLEdBQUcsWUFBVztBQUMzQmhFLElBQUFBLElBQUksQ0FBQ25CLFdBQUwsQ0FBaUIsUUFBakI7QUFDQW1CLElBQUFBLElBQUksQ0FBQ0UsSUFBTCxDQUFVLFFBQVYsRUFBb0JoQixJQUFwQixDQUF5QixLQUF6QixFQUFnQyxFQUFoQztBQUNELEdBSEQ7QUFLQTs7O0FBQ0FsQyxFQUFBQSxDQUFDLENBQUMsY0FBRCxDQUFELENBQWtCNEIsRUFBbEIsQ0FBcUIsT0FBckIsRUFBOEIrRSxVQUE5QjtBQUNBM0QsRUFBQUEsSUFBSSxDQUFDcEIsRUFBTCxDQUFRLE9BQVIsRUFBaUIsMkJBQWpCLEVBQThDb0YsV0FBOUM7QUFDRCxDQXBDRCxFQW9DR2pGLE1BcENIO0NDQUE7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJhcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyB2YXIgbGF6eUltYWdlTG9hZCA9IGZ1bmN0aW9uKCkge1xuLy8gICB2YXIgbGF6eUltYWdlcyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCIubGF6eVwiKTtcbi8vICAgdmFyIGluQWR2YW5jZSA9IDMwMDtcbi8vICAgbGF6eUltYWdlcy5mb3JFYWNoKGZ1bmN0aW9uKGltYWdlKSB7XG4vLyAgICAgaWYgKGltYWdlLm9mZnNldFBhcmVudCA9PSBudWxsKSB7XG4vLyAgICAgICByZXR1cm47XG4vLyAgICAgfVxuLy8gICAgIGlmIChpbWFnZS5jbGFzc0xpc3QuY29udGFpbnMoXCJsb2FkZWRcIikpIHtcbi8vICAgICAgIHJldHVybjtcbi8vICAgICB9XG4vLyAgICAgaWYgKGltYWdlLm9mZnNldFRvcCA8IHdpbmRvdy5pbm5lckhlaWdodCArIHdpbmRvdy5wYWdlWU9mZnNldCArIGluQWR2YW5jZSkge1xuLy8gICAgICAgaWYgKGltYWdlLmRhdGFzZXQuc3JjICE9IHVuZGVmaW5lZCkge1xuLy8gICAgICAgICBpbWFnZS5zcmMgPSBpbWFnZS5kYXRhc2V0LnNyYztcbi8vICAgICAgIH1cbi8vICAgICAgIGlmIChpbWFnZS5kYXRhc2V0LmJnICE9IHVuZGVmaW5lZCkge1xuLy8gICAgICAgICBpbWFnZS5zdHlsZS5iYWNrZ3JvdW5kSW1hZ2UgPSBcInVybChcIiArIGltYWdlLmRhdGFzZXQuYmcgKyBcIilcIjtcbi8vICAgICAgIH1cbi8vICAgICAgIGltYWdlLm9ubG9hZCA9IGZ1bmN0aW9uKCkge1xuLy8gICAgICAgICBpbWFnZS5jbGFzc0xpc3QuYWRkKFwibG9hZGVkXCIpO1xuLy8gICAgICAgfTtcbi8vICAgICB9XG4vLyAgIH0pO1xuLy8gfTtcblxuLy8gKGZ1bmN0aW9uKCQpIHtcbi8vICAgJChkb2N1bWVudCkub24oXCJyZWFkeVwiLCBsYXp5SW1hZ2VMb2FkKTtcbi8vICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoXCJzY3JvbGxcIiwgbGF6eUltYWdlTG9hZCk7XG4vLyAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwicmVzaXplXCIsIGxhenlJbWFnZUxvYWQpO1xuLy8gfSkoalF1ZXJ5KTtcbiIsIihmdW5jdGlvbigkKXtcblxuLypcbiAgT3B0aW9uczpcblxuIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyNcbiAgUmVxdWlyZWRcbiMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjXG5cbiAgLS0gU2xpZGUgaW4gU2lkZSA9IGRlZmF1bHQgbGVmdFxuICAgIC0tIFBvc3NpYmxlIG9wdGlvbnMgXCJ0b3BcIiwgXCJsZWZ0XCIsIFwicmlnaHRcIiwgXCJib3R0b21cIlxuXG4gIC0tIFNsaWRlIGluIFNwZWVkID0gZGVmYXVsdCA0MDBcbiAgICAtLSBPcHRpb25zIGFueSBtaWxpc2Vjb25kc1xuXG4gIC0tIE9wdGlvbmFsIE9wZW4vQ2xvc2UgQnV0dG9uID0gZGVmYXVsdCBDcmVhdGUgb24gZm9yIHRoZW1cbiAgICAtLSBBbnkgb2JqZWN0IHdpdGggYSBkaXYgb3IgY2xhc3NcblxuICAtLSBXaWR0aCBvZiBNZW51ID0gZGVmYXVsdCAxMDAlXG4gICAgLS0gQW55IG51bWJlclxuXG4jIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjI1xuICBOb24gRXNzZW50YWlsc1xuIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyNcblxuICAtLSBGaXhlZCBvciBTdGF0aWMgPSBEZWZhdWx0IEZpeGVkXG4gICAgLS0gTm9uIGVzc2VudGlhbCBmb3Igbm93XG5cbiAgLS0gQW5pbWF0aW9uXG4gICAgLS0gTm9uIGVzc2VudGlhbCBmb3Igbm93XG5cbiAgLS0gT3B0aW9uYWwgQnV0dG9uIFN0eWxpbmdcbiAgICAtLSBOb24gZXNzZW50aWFsIGZvciBub3dcblxuXG5cbiovXG5cbiAgJC5mbi5tb2JpbGVfbWVudSA9IGZ1bmN0aW9uICggb3B0aW9ucyApIHtcblxuICAgIC8vIERlZmluZSBEZWZhdWx0IHNldHRpbmdzOlxuICAgIHZhciBkZWZhdWx0cyA9IHtcbiAgICAgIHNpZGU6ICdsZWZ0JyxcbiAgICAgIHNwZWVkOiAnMC41cycsXG4gICAgICB0b2dnbGVyOiAnLm1lbnUtdG9nZ2xlJyxcbiAgICAgIHdpZHRoOiAnMTAwJScsXG4gICAgICBoZWlnaHQ6ICcxMDAlJyxcbiAgICAgIGJ1dHRvblN0eWxlczogdHJ1ZSxcbiAgICB9O1xuXG4gICAgdmFyIGluaXRhbHMgPSB7XG4gICAgICB0b2dnbGVCdXR0b246ICc8ZGl2IGNsYXNzPVwibWVudS10b2dnbGVcIj48L2Rpdj4nLFxuICAgICAgbWVudVdyYXA6ICc8ZGl2IGNsYXNzPVwib2ZmY2FudmFzLW1lbnUtd3JhcFwiPjwvZGl2PicsXG4gICAgfTtcblxuICAgIHZhciAkdG9nZ2xlcjtcbiAgICB2YXIgb2ZmY2FudmFzO1xuXG4gICAgdmFyICR0aGlzID0gJCh0aGlzKTtcblxuICAgIHZhciBzZXR0aW5ncyA9ICQuZXh0ZW5kKCB7fSwgaW5pdGFscywgZGVmYXVsdHMsIG9wdGlvbnMgKTtcblxuICAgIGlmKHNldHRpbmdzLnRvZ2dsZXIgPT0gZGVmYXVsdHMudG9nZ2xlcil7XG4gICAgICAkdGhpcy5iZWZvcmUoc2V0dGluZ3MudG9nZ2xlQnV0dG9uKTtcbiAgICB9XG5cbiAgICAvKiBEZXRlcm1pbmUgaWYgeW91IHdhbnQgdG9nZ2xlciBTdHlsZXMgKi9cbiAgICAkdG9nZ2xlciA9ICQoc2V0dGluZ3MudG9nZ2xlcik7XG4gICAgaWYoc2V0dGluZ3MuYnV0dG9uU3R5bGVzKXtcbiAgICAgICR0b2dnbGVyLmFkZENsYXNzKCdvZmZjYW52YXMtdG9nZ2xlJyk7XG4gICAgfVxuXG4gICAgc3dpdGNoICggc2V0dGluZ3Muc2lkZSApIHtcbiAgICAgIGNhc2UgXCJsZWZ0XCI6XG4gICAgICAgIG9mZmNhbnZhcyA9ICd0cmFuc2xhdGVYKC0xMDAlKSc7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSBcInRvcFwiOlxuICAgICAgICBvZmZjYW52YXMgPSAndHJhbnNsYXRlWSgtMTAwJSknO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgXCJyaWdodFwiOlxuICAgICAgICBvZmZjYW52YXMgPSAndHJhbnNsYXRlWCgxMDAlKSc7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSBcImJvdHRvbVwiOlxuICAgICAgICBvZmZjYW52YXMgPSAndHJhbnNsYXRlWSgxMDAlKSc7XG4gICAgICAgIGJyZWFrO1xuICAgIH1cblxuICAgIHZhciBzdHlsZXMgPSB7XG4gICAgICAndHJhbnNmb3JtJyA6IG9mZmNhbnZhcyxcbiAgICAgICd0cmFuc2l0aW9uJyA6IHNldHRpbmdzLnNwZWVkLFxuICAgICAgaGVpZ2h0IDogc2V0dGluZ3MuaGVpZ2h0LFxuICAgICAgd2lkdGggOiBzZXR0aW5ncy53aWR0aCxcbiAgICB9O1xuXG4gICAgcmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbigpIHtcblxuICAgICAgICAkdGhpcy5hZGRDbGFzcygnb2ZmY2FudmFzLW1lbnUnKS53cmFwKHNldHRpbmdzLm1lbnVXcmFwKTtcbiAgICAgICAgJHRoaXMucGFyZW50KCkuY3NzKCBzdHlsZXMgKTtcbiAgICAgICAgY29uc29sZS5sb2coJHRvZ2dsZXIpO1xuICAgICAgICAkdG9nZ2xlci5vbignY2xpY2snLCBmdW5jdGlvbigpe1xuICAgICAgICAgIGNvbnNvbGUubG9nKCdvcGVuL2Nsb3NlJyk7XG5cbiAgICAgICAgICAkKHRoaXMpLnRvZ2dsZUNsYXNzKCdvZmZjYW52YXMtYWN0aXZlJyk7XG4gICAgICAgICAgY29uc29sZS5sb2coJHRoaXMpO1xuICAgICAgICAgICR0aGlzLmNsb3Nlc3QoJy5vZmZjYW52YXMtbWVudS13cmFwJykudG9nZ2xlQ2xhc3MoJ29mZmNhbnZhcy1vcGVuJyk7XG5cbiAgICAgICAgfSk7XG5cblxuICAgIH0pO1xuICB9XG5cblxuICAkKCcjbW9iaWxlX21lbnUnKS5tb2JpbGVfbWVudSh7XG4gICAgc2lkZTogJ2JvdHRvbScsXG4gIH0pO1xuXG5cbn0pKGpRdWVyeSk7XG4iLCIoZnVuY3Rpb24oJCkge1xuICBpZiAoJCgnI251bS1zaW5nbGVzJykubGVuZ3RoIDw9IDApIHJldHVybjtcbiAgdmFyIG51bSA9ICQoJyNudW0tc2luZ2xlcycpLmF0dHIoJ3ZhbHVlJyk7XG4gIHZhciAkbm9CbG9ja3MgPSAkKCcucHJldmlldy1ub2NvbnRlbnQtYmxvY2snKTtcbiAgdmFyIHNpemUgPSB3aW5kb3cuaW5uZXJXaWR0aDtcbiAgdmFyIGRlbDtcblxuICB2YXIgX3NldFNpemUgPSBmdW5jdGlvbigpIHtcbiAgICBzaXplID0gd2luZG93LmlubmVyV2lkdGg7XG4gIH07XG5cbiAgdmFyIF9zZXREZWwgPSBmdW5jdGlvbigpIHtcbiAgICBkZWwgPSAyO1xuICAgIGlmICgxODAwIDw9IHNpemUpIHtcbiAgICAgIGRlbCA9IDM7XG4gICAgfVxuICAgIGlmICg3NjcgPj0gc2l6ZSkge1xuICAgICAgZGVsID0gbnVtO1xuICAgIH1cbiAgfTtcblxuICB2YXIgX3NldEJsb2NrcyA9IGZ1bmN0aW9uKCkge1xuICAgICRub0Jsb2Nrcy5oaWRlKCk7XG4gICAgaWYgKDAgPT09IG51bSAlIGRlbCkgcmV0dXJuO1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgZGVsIC0gKG51bSAlIGRlbCk7IGkrKykge1xuICAgICAgJG5vQmxvY2tzLmVxKGkpLnNob3coKTtcbiAgICB9XG4gIH07XG5cbiAgdmFyIF9jaGFuZ2UgPSBmdW5jdGlvbigpIHtcbiAgICBfc2V0U2l6ZSgpO1xuICAgIF9zZXREZWwoKTtcbiAgICBfc2V0QmxvY2tzKCk7XG4gIH07XG5cbiAgJCh3aW5kb3cpLm9uKCdsb2FkJywgX2NoYW5nZSk7XG4gICQod2luZG93KS5vbigncmVzaXplJywgX2NoYW5nZSk7XG59KShqUXVlcnkpO1xuIiwiKGZ1bmN0aW9uICgkKSB7XG4gIHZhciAkYXBwID0gJCgnLmdmb3JtX3dyYXBwZXInKTtcbiAgdmFyICRzZWxlY3QgPSAkYXBwLmZpbmQoJ3NlbGVjdCcpO1xuICB2YXIgJGNhcHRjaGEgPSAkYXBwLmZpbmQoJy5naW5wdXRfcmVjYXB0Y2hhJyk7XG4gIHZhciAkcmVhZE9ubHkgPSAkYXBwLmZpbmQoJy5nZl9yZWFkb25seSBpbnB1dCcpO1xuXG4gICQod2luZG93KS5vbignbG9hZCcsIGZ1bmN0aW9uICgpIHtcbiAgICAvLyBBY3Rpb25zXG4gICAgJGFwcC5vbignZm9jdXMgYmx1cicsICdpbnB1dCwgdGV4dGFyZWEsIHNlbGVjdCcsIF9hY3RpdmF0ZUZpZWxkKTtcbiAgICAkKGRvY3VtZW50KS5vbignZ2Zvcm1fcG9zdF9yZW5kZXInLCBfcmVpbml0KTtcbiAgXG4gICAgLy8gQ2FsbGJhY2tzXG4gICAgX2luaXQoKTtcbiAgfSk7XG5cbiAgdmFyIF9pbml0ID0gZnVuY3Rpb24gKCkge1xuICAgIF9hZGRDbGFzc1JlY2FwdGNoYSgpO1xuICAgIF9hZGRSZWFkT25seSgpO1xuICAgIC8vIF9oaWRlX3NlbGVjdF9sYWJlbHMoKTtcbiAgICBfbG9vcEZpZWxkcyhfYWN0aXZhdGVGaWVsZCk7XG4gIH07XG5cbiAgdmFyIF9yZWluaXQgPSBmdW5jdGlvbiAoKSB7XG4gICAgX2xvb3BGaWVsZHMoX2FjdGl2YXRlRmllbGQpO1xuICB9O1xuXG4gIHZhciBfYWRkQ2xhc3NSZWNhcHRjaGEgPSBmdW5jdGlvbiAoKSB7XG4gICAgJGNhcHRjaGEuc2libGluZ3MoJ2xhYmVsJykuYWRkQ2xhc3MoJ2Zvcm0tcmVjYXB0Y2hhLWxhYmVsJyk7XG4gIH07XG5cbiAgdmFyIF9oaWRlX3NlbGVjdF9sYWJlbHMgPSBmdW5jdGlvbiAoKSB7XG4gICAgJHNlbGVjdFxuXG4gICAgICAuY2xvc2VzdCgnLmdmaWVsZCcpXG5cbiAgICAgIC5maW5kKCdsYWJlbCcpXG5cbiAgICAgIC5oaWRlKCk7XG4gIH07XG5cbiAgdmFyIF9hZGRSZWFkT25seSA9IGZ1bmN0aW9uICgpIHtcbiAgICAkcmVhZE9ubHkuYXR0cigncmVhZG9ubHknLCAncmVhZG9ubHknKTtcbiAgfTtcblxuICB2YXIgX2xvb3BGaWVsZHMgPSBmdW5jdGlvbiAoY2FsbGJhY2spIHtcbiAgICAkYXBwLmZpbmQoJ2lucHV0LCBzZWxlY3QsIHRleHRhcmVhJykuZWFjaChmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgIGNhbGxiYWNrKGV2ZW50LCB0aGlzKTtcbiAgICB9KTtcbiAgfTtcblxuICB2YXIgX2FjdGl2YXRlRmllbGQgPSBmdW5jdGlvbiAoZXZlbnQsIHRhcmdldCkge1xuICAgIHZhciAkdGhpcztcblxuICAgIGlmICh1bmRlZmluZWQgIT09IHRhcmdldCkge1xuICAgICAgJHRoaXMgPSAkKHRhcmdldCk7XG4gICAgfSBlbHNlIHtcbiAgICAgICR0aGlzID0gJCh0aGlzKTtcbiAgICB9XG5cbiAgICBpZiAoJHRoaXMucGFyZW50KCkuaXMoJ3NwYW4nKSkge1xuICAgICAgJHRoaXMuY2xvc2VzdCgnc3BhbicpLnJlbW92ZUNsYXNzKCd1c2VkJyk7XG4gICAgfSBlbHNlIHtcbiAgICAgICR0aGlzLmNsb3Nlc3QoJy5nZmllbGQnKS5yZW1vdmVDbGFzcygndXNlZCcpO1xuICAgIH1cblxuICAgIGlmICgnJyAhPT0gJHRoaXMudmFsKCkgfHwgZXZlbnQudHlwZSA9PT0gJ2ZvY3VzaW4nKSB7XG4gICAgICBpZiAoJHRoaXMucGFyZW50KCkuaXMoJ3NwYW4nKSkge1xuICAgICAgICB2YXIgJGVsZW0gPSAkdGhpcy5jbG9zZXN0KCdzcGFuJyk7XG4gICAgICAgICRlbGVtLmFkZENsYXNzKCd1c2VkJyk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB2YXIgJGVsZW0gPSAkdGhpcy5jbG9zZXN0KCcuZ2ZpZWxkJyk7XG4gICAgICAgICRlbGVtLmFkZENsYXNzKCd1c2VkJyk7XG4gICAgICB9XG4gICAgfVxuICB9O1xufSkoalF1ZXJ5KTtcbiIsIihmdW5jdGlvbigkKSB7XG4gIC8qKiBTY3JvbGwgdG8gRGl2IFZpYSB0aGUgSGFzaCAqL1xuICB2YXIgX2luaXRIYXNoU2Nyb2xsID0gZnVuY3Rpb24oKSB7XG4gICAgdmFyIGRpdiA9ICQod2luZG93LmxvY2F0aW9uLmhhc2gpO1xuICAgIGlmIChkaXYubGVuZ3RoID4gMCkge1xuICAgICAgX3Njcm9sbFRvRGl2KGRpdik7XG4gICAgfVxuICB9O1xuXG4gIC8qKiBIZWxwZXIgRnVuY3Rpb246IFNjcm9sbCB0byBEaXYgKi9cbiAgdmFyIF9zY3JvbGxUb0RpdiA9IGZ1bmN0aW9uKGRpdikge1xuICAgICQoXCJodG1sLCBib2R5XCIpLmFuaW1hdGUoXG4gICAgICB7XG4gICAgICAgIHNjcm9sbFRvcDogZGl2Lm9mZnNldCgpLnRvcFxuICAgICAgfSxcbiAgICAgIDEwMDBcbiAgICApO1xuICB9O1xuXG4gIC8qKiBTY3JvbGwgVG8gVGhlIE5leHQgU2VjdGlvbiAqL1xuICB2YXIgX3Njcm9sbFRvSHJlZkRpdiA9IGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICB2YXIgZGl2ID0gJCh0aGlzKS5hdHRyKFwiaHJlZlwiKTtcbiAgICBkaXYgPSAkKGRpdik7XG4gICAgaWYgKGRpdi5sZW5ndGggPiAwKSB7XG4gICAgICBfc2Nyb2xsVG9EaXYoZGl2KTtcbiAgICB9XG4gIH07XG5cbiAgLyoqIFNjcm9sbCB0byBOZXh0IEJlYXZlciBCdWlsZGVyIFJvdyAqL1xuICB2YXIgX3Njcm9sbFRvTmV4dFJvdyA9IGZ1bmN0aW9uKCkge1xuICAgICQoXCJodG1sLCBib2R5XCIpLmFuaW1hdGUoXG4gICAgICB7XG4gICAgICAgIHNjcm9sbFRvcDogJCh0aGlzKVxuICAgICAgICAgIC5jbG9zZXN0KFwiLmZsLXJvd1wiKVxuICAgICAgICAgIC5uZXh0KFwiLmZsLXJvd1wiKVxuICAgICAgICAgIC5vZmZzZXQoKS50b3BcbiAgICAgIH0sXG4gICAgICAxMDAwXG4gICAgKTtcbiAgfTtcblxuICAvKiogVHJpZ2dlcnMgKi9cbiAgJCh3aW5kb3cpLm9uKFwibG9hZFwiLCBfaW5pdEhhc2hTY3JvbGwpO1xuICAkKFwiLnNsaWdodC1uZXh0XCIpLm9uKFwiY2xpY2tcIiwgX3Njcm9sbFRvSHJlZkRpdik7XG4gICQoJ2FbaHJlZj1cIiNuZXh0XCJdJykub24oXCJjbGlja1wiLCBfc2Nyb2xsVG9OZXh0Um93KTtcbiAgJCgnYVtocmVmPVwiI25leHQtbm8tYW5pbWF0aW9uXCJdJykub24oXCJjbGlja1wiLCBfc2Nyb2xsVG9OZXh0Um93KTtcbn0pKGpRdWVyeSk7XG4iLCIoZnVuY3Rpb24gKCQpIHtcbiAgLyoqXG4gICAqIENvZGUgdG8gSGlkZS9TaG93IEhlYWRlclxuICAgKi9cbiAgdmFyICRib2R5ID0gJChcImJvZHlcIik7XG4gIHZhciAkaGVhZGVyID0gJChcIiNoZWFkZXJcIik7XG4gIHZhciAkbWVudSA9ICQoXCIjbWVudVwiKTtcbiAgLy8gdmFyICRtZW51ID0gJGhlYWRlci5maW5kKCcjaGVhZGVyLW1lbnUnKTtcbiAgdmFyIGxhc3RTY3JvbGxUb3AgPSAwO1xuICB2YXIgbW9iaWxlU3RvcFNjcm9sbCA9IGZhbHNlO1xuXG4gIHZhciB0ZXN0U2Nyb2xsID0gZnVuY3Rpb24gKCkge1xuICAgIGlmIChtb2JpbGVTdG9wU2Nyb2xsKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIHZhciBzdCA9ICQod2luZG93KS5zY3JvbGxUb3AoKTtcbiAgICBpZiAoc3QgPCBsYXN0U2Nyb2xsVG9wKSB7XG4gICAgICBpZiAoJGhlYWRlci5oYXNDbGFzcyhcInNjcm9sbFwiKSkge1xuICAgICAgICByZW1vdmVTY3JvbGwoKTtcbiAgICAgIH1cbiAgICB9IGVsc2UgaWYgKHN0ID4gNzQpIHtcbiAgICAgIGlmICghJGhlYWRlci5oYXNDbGFzcyhcInNjcm9sbFwiKSkge1xuICAgICAgICBhZGRTY3JvbGwoKTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgcmVtb3ZlU2Nyb2xsKCk7XG4gICAgfVxuICAgIGxhc3RTY3JvbGxUb3AgPSBzdDtcbiAgfTtcblxuICB2YXIgYWRkU2Nyb2xsID0gZnVuY3Rpb24gKCkge1xuICAgICRoZWFkZXIuYWRkQ2xhc3MoXCJzY3JvbGxcIik7XG4gIH07XG5cbiAgdmFyIHJlbW92ZVNjcm9sbCA9IGZ1bmN0aW9uICgpIHtcbiAgICAkaGVhZGVyLnJlbW92ZUNsYXNzKFwic2Nyb2xsXCIpO1xuICB9O1xuXG4gIHZhciBvcGVuX21vYmlsZV9tZW51ID0gZnVuY3Rpb24gKCkge1xuICAgICQodGhpcylcbiAgICAgIC5jbG9zZXN0KFwiI21lbnVcIilcbiAgICAgIC50b2dnbGVDbGFzcyhcIm9wZW4tbWVudVwiKTtcbiAgICAkKHRoaXMpXG4gICAgICAuZmluZChcIi5iYXJzXCIpXG4gICAgICAudG9nZ2xlQ2xhc3MoXCJjaGFuZ2VcIik7XG4gICAgJChcImJvZHksIGh0bWxcIikudG9nZ2xlQ2xhc3MoXCJuby1zY3JvbGxcIik7XG4gICAgbW9iaWxlU3RvcFNjcm9sbCA9IG1vYmlsZVN0b3BTY3JvbGwgPyBmYWxzZSA6IHRydWU7XG4gIH07XG5cbiAgdmFyIGFjdGl2YXRlX3N1Yl9tZW51ID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciB0ZXN0ID0gJCh0aGlzKS5oYXNDbGFzcyhcImRyb3Bkb3duLW9wZW5cIik7XG4gICAgJCh0aGlzKVxuICAgICAgLmNsb3Nlc3QoXCIjbWVudS1tYWluLW1lbnVcIilcbiAgICAgIC5maW5kKFwiLmRyb3Bkb3duLW9wZW5cIilcbiAgICAgIC5yZW1vdmVDbGFzcyhcImRyb3Bkb3duLW9wZW5cIik7XG4gICAgaWYgKCF0ZXN0KSB7XG4gICAgICAkKHRoaXMpLmFkZENsYXNzKFwiZHJvcGRvd24tb3BlblwiKTtcbiAgICAgICQodGhpcylcbiAgICAgICAgLnNpYmxpbmdzKFwiLmRyb3Bkb3duXCIpXG4gICAgICAgIC50b2dnbGVDbGFzcyhcImRyb3Bkb3duLW9wZW5cIik7XG4gICAgfVxuICB9O1xuXG4gICQod2luZG93KS5vbihcInNjcm9sbFwiLCB0ZXN0U2Nyb2xsKTtcbiAgLy8gaWYgKCQod2luZG93KS53aWR0aCgpIDwgMTIwMCkge1xuICAvLyAgICRtZW51LmZpbmQoXCIjbW9iaWxlLW1lbnVcIikub24oXCJjbGlja1wiLCBvcGVuX21vYmlsZV9tZW51KTtcbiAgLy8gICAkbWVudS5maW5kKFwiLmRyb3Bkb3duLWJ1dHRvblwiKS5vbihcImNsaWNrXCIsIGFjdGl2YXRlX3N1Yl9tZW51KTtcbiAgLy8gfVxuXG5cblxuXG4gIHZhciBpbml0X21lbnUgPSBmdW5jdGlvbigpIHtcbiAgICBpZiAoJCh3aW5kb3cpLndpZHRoKCkgPCAxMjAwKSB7XG4gICAgICAkbWVudS5maW5kKFwiI21vYmlsZS1tZW51XCIpLm9uKFwiY2xpY2tcIiwgb3Blbl9tb2JpbGVfbWVudSk7XG4gICAgICAkbWVudS5maW5kKFwiLmRyb3Bkb3duLWJ1dHRvblwiKS5vbihcImNsaWNrXCIsIGFjdGl2YXRlX3N1Yl9tZW51KTtcbiAgICB9XG4gIH1cblxuICAkKGRvY3VtZW50KS5yZWFkeShpbml0X21lbnUpXG4gICQod2luZG93KS5vbigncmVzaXplJywgaW5pdF9tZW51KVxuICAvKipcbiAgICogY29kZSB0byBzY2hlZHVsZSBtZWV0aW5nIHBvcHVwXG4gICAqL1xuICAvLyAkbGlnaHRib3ggPSAkKFwiI3NtZy1sYlwiKTtcblxuICAvLyB2YXIgb3Blbl9saWdodGJveCA9IGZ1bmN0aW9uKGV2ZW50KSB7XG4gIC8vICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgLy8gICAkbGlnaHRib3guYWRkQ2xhc3MoXCJhY3RpdmVcIik7XG4gIC8vICAgJGxpZ2h0Ym94XG4gIC8vICAgICAuZmluZChcIiNzbWctbGItY29udFwiKVxuICAvLyAgICAgLmh0bWwoXG4gIC8vICAgICAgICc8c2NyaXB0IHNyYz1cImh0dHBzOi8vY2RuLnNjaGVkdWxlb25jZS5jb20vbWVyZ2VkanMvc28uanNcIj48L3NjcmlwdD48IS0tU2NoZWR1bGVPbmNlIGVtYmVkIFNUQVJULS0+PGRpdiBpZD1cIlNPSURJVl9Ub21NdXJwaHlcIiBkYXRhLXNvLXBhZ2U9XCJUb21NdXJwaHlcIiBkYXRhLWhlaWdodD1cIjU1MFwiIGRhdGEtc3R5bGU9XCJib3JkZXI6IDFweCBzb2xpZCAjZDhkOGQ4OyBtaW4td2lkdGg6IDI5MHB4OyBtYXgtd2lkdGg6IDkwMHB4O1wiIGRhdGEtcHN6PVwiMDBcIj48L2Rpdj48IS0tU2NoZWR1bGVPbmNlIGVtYmVkIEVORC0tPidcbiAgLy8gICAgICk7XG4gIC8vIH07XG4gIC8vIHZhciBjbG9zZV9saWdodGJveCA9IGZ1bmN0aW9uKGV2ZW50KSB7XG4gIC8vICAgJGxpZ2h0Ym94LnJlbW92ZUNsYXNzKFwiYWN0aXZlXCIpO1xuICAvLyAgICRsaWdodGJveC5maW5kKFwiI3NtZy1sYi1jb250XCIpLmh0bWwoXCJcIik7XG4gIC8vIH07XG5cbiAgLy8gJGxpZ2h0Ym94Lm9uKFwiY2xpY2tcIiwgY2xvc2VfbGlnaHRib3gpO1xuICAvLyAkbGlnaHRib3guZmluZChcIiNzbWctY2xvc2VcIikub24oXCJjbGlja1wiLCBjbG9zZV9saWdodGJveCk7XG5cbiAgLy8gJGJvZHkuZmluZChcIi5zY2gtbWVldFwiKS5vbihcImNsaWNrXCIsIG9wZW5fbGlnaHRib3gpO1xufSkoalF1ZXJ5KTtcbiIsInZhciBsYXp5TG9hZCA9IChmdW5jdGlvbigkKSB7XG4gIHZhciBfbGF6eUxvYWQgPSBmdW5jdGlvbihlbGVtLCBudW0pIHtcbiAgICB2YXIgJGVsZW0gPSAkKFwiLnNtZy1sYXp5LWxvYWRcIik7XG4gICAgdmFyIGRpdmlkZXIgPSAyOyBcblxuICAgIC8vIGNvbnNvbGUubG9nKGVsZW0pO1xuXG4gICAgaWYgKHR5cGVvZiBudW0gPT09IFwibnVtYmVyXCIpIHtcbiAgICAgIGRpdmlkZXIgPSBudW07XG4gICAgfVxuXG4gICAgaWYgKHR5cGVvZiBlbGVtID09PSBcInN0cmluZ1wiKSB7XG4gICAgICAkZWxlbSA9ICQoZWxlbSk7XG4gICAgfVxuXG4gICAgdmFyIGhlaWdodCA9IHdpbmRvdy5pbm5lckhlaWdodDtcbiAgICB2YXIgd2lkdGggPSB3aW5kb3cuaW5uZXJXaWR0aDtcblxuICAgICRlbGVtLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICBpZiAoJCh0aGlzKS5oYXNDbGFzcyhcImFjdGl2ZVwiKSkge1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIH1cblxuICAgICAgdmFyIHJlY3QgPSB0aGlzLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xuICAgICAgaWYgKHJlY3QudG9wIDwgaGVpZ2h0IC8gZGl2aWRlcikge1xuICAgICAgICAvLyBjb25zb2xlLmxvZyhyZWN0KTtcbiAgICAgICAgJCh0aGlzKS5hZGRDbGFzcyhcImFjdGl2ZVwiKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfTtcblxuICAkKHdpbmRvdykub24oXCJzY3JvbGxcIiwgX2xhenlMb2FkKTtcblxuICByZXR1cm4ge1xuICAgIGxhenk6IF9sYXp5TG9hZFxuICB9O1xufSkoalF1ZXJ5KTtcbiIsIihmdW5jdGlvbigkKSB7XG4gIGlmICgkKFwiLmxpZ2h0Ym94LXNtZ1wiKS5sZW5ndGggPCAxKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLyoqIERlZmluZSBWYXJpYWJsZXMgKi9cbiAgdmFyICRhcHAgPSAkKFwiLmxpZ2h0Ym94LXNtZ1wiKTtcblxuICAvKiogTG9hZCBWaWRlbyBGdW5jdGlvbiAqL1xuICB2YXIgX2xvYWRWaWRlbyA9IGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAkYXBwLnRvZ2dsZUNsYXNzKFwiYWN0aXZlXCIpO1xuICAgICRsaWdodFZpZCA9ICQodGhpcykuZGF0YShcInZpZFwiKTtcbiAgICAkbGlnaHRQaG90byA9ICQodGhpcykuZGF0YShcImxpZ2h0Ym94XCIpO1xuICAgIGlmICgkbGlnaHRWaWQpIHtcbiAgICAgICRhcHBcbiAgICAgICAgLmZpbmQoXCIubGlnaHRib3gtY29udGVudFwiKVxuICAgICAgICAuaHRtbChcbiAgICAgICAgICAnPGRpdiBjbGFzcz1cInJlc3BvbnNpdmUtaWZyYW1lXCI+PGlmcmFtZSB3aWR0aD1cIjU2MFwiIGhlaWdodD1cIjMxNVwiIGZyYW1lYm9yZGVyPVwiMFwiIGFsbG93ZnVsbHNjcmVlbiBzcmM9XCJodHRwczovL3d3dy55b3V0dWJlLmNvbS9lbWJlZC8nICtcbiAgICAgICAgICAgICRsaWdodFZpZCArXG4gICAgICAgICAgICAnP3JlbD0wJmF1dG9wbGF5PTFcIj48L2lmcmFtZT48L2Rpdj4nXG4gICAgICAgICk7XG4gICAgfSBlbHNlIGlmICgkbGlnaHRQaG90bykge1xuICAgICAgJGFwcC5maW5kKFwiLmxpZ2h0Ym94LWNvbnRlbnRcIikuaHRtbChcIjxpbWcgc3JjPSdcIiArICRsaWdodFBob3RvICsgXCInIC8+XCIpO1xuICAgIH1cbiAgfTtcblxuICAvKiogU3RvcCBWaWRlbyBGdW5jdGlvbiAqL1xuICB2YXIgX3N0b3BQbGF5ZXIgPSBmdW5jdGlvbigpIHtcbiAgICAkYXBwLnRvZ2dsZUNsYXNzKFwiYWN0aXZlXCIpO1xuICAgICRhcHAuZmluZChcImlmcmFtZVwiKS5hdHRyKFwic3JjXCIsIFwiXCIpO1xuICB9O1xuXG4gIC8qKiBUcmlnZ2VycyAqL1xuICAkKFwiLmxpZ2h0LXBvcHVwXCIpLm9uKFwiY2xpY2tcIiwgX2xvYWRWaWRlbyk7XG4gICRhcHAub24oXCJjbGlja1wiLCBcIi5vdmVybGF5LCAubGlnaHRib3gtY2xvc2VcIiwgX3N0b3BQbGF5ZXIpO1xufSkoalF1ZXJ5KTtcbiIsIi8vIChmdW5jdGlvbigkKSB7XG4vLyAgIHZhciAkc2NoTWVldCA9ICAkKCcuc2NoLW1lZXQnKTtcblxuLy8gICB2YXIgbG9hZF9vbl9jbGljayA9IGZ1bmN0aW9uKCl7XG5cbi8vICAgICAgPCAhLS1TY2hlZHVsZU9uY2UgZW1iZWQgU1RBUlQtLSA+XG4vLyAgICAgICA8ZGl2IGlkPVwiU09JRElWX1RvbU11cnBoeVwiIGRhdGEtc28tcGFnZT1cIlRvbU11cnBoeVwiIGRhdGEtaGVpZ2h0PVwiNTUwXCIgZGF0YS1zdHlsZT1cImJvcmRlcjogMXB4IHNvbGlkICNkOGQ4ZDg7IG1pbi13aWR0aDogMjkwcHg7IG1heC13aWR0aDogOTAwcHg7XCIgZGF0YS1wc3o9XCIwMFwiPjwvZGl2PlxuXG4vLyAgICAgICA8IS0tU2NoZWR1bGVPbmNlIGVtYmVkIEVORC0tID5cbi8vICAgfVxuLy8gICAkc2NoTWVldC5vbignY2xpY2snLCBsb2FkX29uX2NsaWNrKTtcbi8vICAgLy8gdmFyIF9pbml0ID0gZnVuY3Rpb24oKSB7XG4vLyAgIC8vICAgc2V0VGltZW91dChmdW5jdGlvbigpIHtcbi8vICAgLy8gICAgICQoXCJib2R5XCIpLmFwcGVuZChcbi8vICAgLy8gICAgICAgXCI8c2NyaXB0IHNyYz0naHR0cHM6Ly9jZG4uc2NoZWR1bGVvbmNlLmNvbS9tZXJnZWRqcy9zby5qcyc+PC9zY3JpcHQ+XCJcbi8vICAgLy8gICAgICk7XG5cbi8vICAgLy8gICAgIC8vIEVuYWJsZSB3aGVuIHRlc3RpbmcgYW5kIHB1c2hpbmcgdG8gcHJvZHVjdGlvblxuLy8gICAvLyAgICAgLy9cbi8vICAgLy8gICAgIC8vICQuZ2V0U2NyaXB0KHtcbi8vICAgLy8gICAgIC8vICAgdXJsOiBcImh0dHBzOi8vY2RuLnNjaGVkdWxlb25jZS5jb20vbWVyZ2VkanMvc28uanNcIixcbi8vICAgLy8gICAgIC8vICAgY2FjaGU6IHRydWVcbi8vICAgLy8gICAgIC8vIH0pO1xuLy8gICAvLyAgIH0sIDE1MDApO1xuLy8gICAvLyB9O1xuLy8gICAvLyBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKFwiRE9NQ29udGVudExvYWRlZFwiLCBfaW5pdCk7XG4vLyB9KShqUXVlcnkpO1xuIl19
