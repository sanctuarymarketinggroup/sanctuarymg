<?php
/**
  * The template for displaying the front page
  *
  * This is the template that displays all pages by default.
  * Please note that this is the WordPress construct of pages and that
  * other "pages" on your WordPress site will use a different template.
  *
*/

get_header(); ?>

<main id="front-page" class="container" role="main">
<?php while ( have_posts() ) : the_post(); ?>
	<article class="main-content" id="post-<?php the_ID(); ?>">
		<div class="entry-content">
			<?php the_content(); ?>
		</div>
	</article>
<?php endwhile;?>
</main>

<?php get_footer(); ?>
