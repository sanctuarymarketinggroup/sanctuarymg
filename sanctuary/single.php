<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

get_header(); 
//TO DO add in stuff from Theme options

$categories = get_the_category();
$separator = ', '; // define separator variable 
$count = count($categories);
$cat_list = '';
foreach ( $categories as $i=>$category ) {
	$cat_list .= esc_html( $category->name );
	if ( $i < $count - 1 )
	$cat_list .= $separator;
}
?>
<script type="application/ld+json">
{ 
	"@context": "http://schema.org", 
	"@type": "BlogPosting",
	"headline": "<?php echo get_the_title(); ?>",
	"name": "<?php echo get_the_title(); ?>",
	<?php if( has_post_thumbnail() ) { ?>
	"image": "<?php echo get_the_post_thumbnail_url(); ?>",
	<?php } ?> 
	"keywords": "<?php echo $cat_list; ?>", 
	"articleSection": "<?php echo $cat_list; ?>", 
	"wordcount": "<?php echo word_count();?>",
	"url": "<?php echo get_permalink(); ?>",
	"mainEntityOfPage": "<?php echo get_permalink(); ?>",
	"datePublished": "<?php echo get_the_date(); ?>",
	"dateCreated": "<?php echo get_the_date(); ?>",
	"isFamilyFriendly": "Yes",
	"dateModified": "<?php echo get_the_modified_date('F d, Y');?>",
	"description": "<?php echo esc_html(get_the_excerpt()); ?>",
	"articleBody": "<?php echo esc_html(get_the_content());  ?>",
  "author": {
    "@type": "Person",
    "name": "<?php get_the_author(); ?>"
  }
}
</script>

<main id="post" class="contianer subpage" role="main">
<?php while ( have_posts() ) { the_post(); ?>
	<article class="main-content" id="post-<?php the_ID(); ?>">
		<header>
			<h1 class="entry-title">
				<?php echo get_the_title(); ?>
			</h1>
		</header>
		<div class="entry-content row-radial-gradient-2">
			<div class="skinny fl-col">
				<div class="fl-row-content">
					<?php
					// if(has_post_thumbnail()) {
					// 	echo '<div class="featured-image">';
					// 		the_post_thumbnail();
					// 	echo '</div>';
					// }
					?>
					<div class="post-content">
						<?php if(is_single()){ ?>
							<div id="post-breadcrumbs">
								<?php yoast_breadcrumb(); ?>
							</div>
							<div class="post-title-entry">
								<?php smg_entry_meta(); ?>
							</div>
						<?php } ?>
						<?php the_content(); ?>
						<!-- Go to www.addthis.com/dashboard to customize your tools -->
						<div class="addthis_jumbo_share"></div>
						
					</div>
				</div>
			</div>
		</div>
		<footer>
			<meta itemprop="copyrightHolder" content="<?php echo get_setting('site-name');?>"/>
			<?php $header_logo = get_setting('header-logo');  ?> 
			<?php $url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID)); ?>
			<span itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
				<meta itemprop="url"  content="<?php echo $url['0'];?>"/>
				<meta itemprop="width"  content="<?php echo $url['1'];?>"/>
				<meta itemprop="height"  content="<?php echo $url['2'];?>"/>
			</span>
			<span itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
				<meta itemprop="url"  content="<?php echo HOME_URL;?>"/>
				<span itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
					<meta itemprop="url"  content="<?php echo $header_logo['url'];?>"/>
					<meta itemprop="width"  content="<?php echo $header_logo['width'];?>"/>
					<meta itemprop="height"  content="<?php echo $header_logo['height'];?>"/>
				</span>
				<meta itemprop="name" content="<?php echo get_setting('site-name');?>">
			</span>
		</footer>
	</article>
<?php } ?>
</main>
<?php get_footer(); ?>
