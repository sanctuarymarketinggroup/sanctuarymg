<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package WordPress
 * @subpackage SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

?>
		<footer id="footer">
			<?php if (!is_page(2403)) {?>
				<?php show_template('tp-footer-1');?>
				<?php show_template('st-footer-2');?>
			<?php }?>
			<?php show_template('st-footer-3');?>
		</footer>
		<?php show_template('st-schedule-meeting');?>
		<?php wp_footer();?>
	</body>
</html>
