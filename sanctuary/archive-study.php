<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

global $wp_query;

get_header();
?>

<div id="archive" class="container subpage" role="main">
	<div class="main-content">
		<header>
      <?php yoast_breadcrumb(); ?>
			<h1 class="entry-title scripty"> 
				<?php echo get_the_archive_title(); ?>
			</h1>
		</header>
		<div class="archive-content archive-preview-blocks">
			<div class="case-studies-row">
				<div class="case-studies-intro">
					<h1>Your wins are our wins.</h1>
					<p>When you partner with Sanctuary, our ultimate goal is to help you grow your business. Our end-to-end marketing system provides the framework for a profitable digital marketing model that’s scalable to your specific needs. The strategies we create for you are always accountable to results. Here’s proof.</p>
				</div>
			</div>
			<?php
				if( have_posts() ) {
					while ( have_posts() ) { the_post();
						$logo = get_field('archive_logo');
						$heading = get_field('title') ? get_field('title') : get_the_title();
						$content = get_the_excerpt();
						echo '<div class="case-studies-row">';
							echo '<div class="case-studies-inner">';
							if(get_field('archive_image')){ 
								$iurl = get_field('archive_image');
							}
								//style="background-image:url('.$iurl.');"
								echo '<div class="case-studies-image" >';
									echo acf_image($iurl);
									// if(get_the_post_thumbnail_url()){
									// 	$iurl = get_the_post_thumbnail_url();
									// } else {
									// 	$iurl = get_field('case_study_flip_image');
									// }
									// echo '<img src="'. $iurl .'" alt="'.get_the_title() .' Featured Image" />';
								echo '</div>';
								echo '<div class="case-studies-content">';
									// echo '<h2>' . get_the_title() . '</h2>';
									echo '<div class="logo">' . acf_image($logo) . '</div>';
									echo '<h2>' . $heading . '</h2>';
									echo '<p>' . $content . '</p>';
									echo '<a class="button outline" href="' . get_permalink() . '">View Case Study</a>';
								echo '</div>';
							echo '</div>';
						echo '</div>';
					}
				} else {
					get_template_part( 'content', 'none' );
				}
			?>
			<?php smg_pagination(); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
