<?php
/**
 * The template for displaying all single white papers
 *
 * @package WordPress
 * @subpackage SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

get_header();
$featured_image = get_the_post_thumbnail_url( get_the_id(), 'large' );

$categories = get_the_category();
$separator = ', '; // define separator variable 
$count = count($categories);
$cat_list = '';
foreach ( $categories as $i=>$category ) {
	$cat_list .= esc_html( $category->name );
	if ( $i < $count - 1 )
	$cat_list .= $separator;
}
?>
<script type="application/ld+json">
{ 
	"@context": "http://schema.org", 
	"@type": "BlogPosting",
	"headline": "<?php echo get_the_title(); ?>",
	"name": "<?php echo get_the_title(); ?>",
	<?php if( has_post_thumbnail() ) { ?>
	"image": "<?php echo get_the_post_thumbnail_url(); ?>",
	<?php } ?> 
	"keywords": "<?php echo $cat_list; ?>", 
	"articleSection": "<?php echo $cat_list; ?>", 
	"wordcount": "<?php echo word_count();?>",
	"url": "<?php echo get_permalink(); ?>",
	"mainEntityOfPage": "<?php echo get_permalink(); ?>",
	"datePublished": "<?php echo get_the_date(); ?>",
	"dateCreated": "<?php echo get_the_date(); ?>",
	"isFamilyFriendly": "Yes",
	"dateModified": "<?php echo get_the_modified_date('F d, Y');?>",
	"description": "<?php echo esc_html(get_the_excerpt()); ?>",
	"articleBody": "<?php echo esc_html(get_the_content());  ?>",
  "author": {
    "@type": "Person",
    "name": "<?php get_the_author(); ?>"
  }
}
</script>

<main id="post" class="contianer subpage white-paper" role="main">
<?php while ( have_posts() ) { the_post(); ?>
	<article class="main-content" id="post-<?php the_ID(); ?>">
		<!-- <header>
			<h1 class="entry-title">
				<?php // echo get_the_title(); ?>
			</h1>
		</header> -->

			<div id="white-paper-header" style="background-image:url(<?php echo $featured_image; ?>)">
				<h1 class="entry-title nonscripty">
					<?php the_title(); ?>
				</h1>
				<a class="slight-next" href="#white-paper-first-row">
					<?php echo get_svg('down-arrow-1'); ?> 
				</a>
			</div>

		<div class="entry-content row-radial-gradient-2">
      <?php the_content(); ?>
		</div>
		<footer>
			<meta itemprop="copyrightHolder" content="<?php echo get_setting('site-name');?>"/>
			<?php $header_logo = get_setting('header-logo');  ?> 
			<?php $url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID)); ?>
			<span itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
				<meta itemprop="url"  content="<?php echo $url['0'];?>"/>
				<meta itemprop="width"  content="<?php echo $url['1'];?>"/>
				<meta itemprop="height"  content="<?php echo $url['2'];?>"/>
			</span>
			<span itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
				<meta itemprop="url"  content="<?php echo HOME_URL;?>"/>
				<span itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
					<meta itemprop="url"  content="<?php echo $header_logo['url'];?>"/>
					<meta itemprop="width"  content="<?php echo $header_logo['width'];?>"/>
					<meta itemprop="height"  content="<?php echo $header_logo['height'];?>"/>
				</span>
				<meta itemprop="name" content="<?php echo get_setting('site-name');?>">
			</span>
		</footer>
	</article>
<?php } ?>
</main>
<?php get_footer(); ?>
