<?php
/**
 * Author: SMG Dev Team
 * URL: https://sanctuarymg.com/
 *
 * SanctuaryMG functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @package WordPress
 * @subpackage SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

/** Inital Functions */
require_once 'library/init.php';

/** Enqueue scripts */
require_once 'library/enqueue-scripts.php';

/** Return entry meta information for posts */
require_once 'library/entry-meta.php';

/** Add functions to be used throughout theme */
require_once 'library/helper.php';

/** Add menu walkers for top-bar and off-canvas */
require_once 'library/menu-walkers.php';

/** Register all navigation menus */
require_once 'library/navigation.php';

/** Register all navigation menus */
require_once 'library/post-types-taxonomies.php';

/** Theme Specific Functions */
require_once 'library/shortcodes.php';

/** Theme Options Functions */
require_once 'library/theme-options.php';

/** Theme Support Functions */
require_once 'library/theme-support.php';

/** Beaver Builder Modules Functions */
require_once 'library/beaver-builder/init.php';

/** Admin Functions */
require_once 'library/admin/init.php';

/** Archive Pages Content */
require_once 'library/archive-content/filters.php';

/** Lightbox */
require_once 'library/lightbox.php';

/** custom Funct */
require_once 'library/custom-funct.php';

/** Modules Content */
require_once 'modules/init.php';

/** Shortcodes Content */
require_once 'library/shortcodes/init.php';

// /** Shortcodes Content */
// require_once 'library/generateblocks/init.php';

/** Review Inifite */
require_once 'library/review/review-infinite.php';

define('SMG_THEME', ABSPATH . '/wp-content/themes/SanctuaryMG');
define('HOME_URL', home_url());
define('THEME_ROOT', get_stylesheet_directory_uri());
define('SEO_CHECKER', true);
define('LINK_CHECKER', true);

add_action('wp_ajax_nopriv_getProjects', 'get_more_projects');
add_action('wp_ajax_getProjects', 'get_more_projects');

function get_more_projects()
{
    $args = array(
        'post_type' => 'portfolio',
        'post_status' => 'publish',
        'offset' => $_POST['offset'],
        'posts_per_page' => '3',
        'order' => 'DESC',
        'orderby' => 'date',
    );

    $the_query = new WP_Query($args);
    if ($the_query->have_posts()) {
        while ($the_query->have_posts()) {$the_query->the_post();
            show_template(
                'content-block',
                array(
                    'terms' => array(
                        'portfolio_cat' => get_the_terms(get_the_ID(), 'portfolio_cat'),
                    ),
                    'image' => get_field('portfolio_archive_square_image')['url'], // get_the_post_thumbnail_url( get_the_id() ),
                    'link' => get_permalink(),
                    'before' => apply_filters('before_block_content', ''),
                    'after' => apply_filters('after_block_content', '<h2>' . get_the_title() . '</h2>'),
                ),
                './'
            );
        }
        wp_reset_postdata();
    }

    die();
}
