<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

add_filter( 'fl_builder_global_posts', function($ids){ $ids[] = 13581; return $ids;  } );

get_header();
?>

<div id="archive" class="container subpage row-radial-gradient-2 white-paper" role="main">
	<div class="main-content">
		<header>
      <?php yoast_breadcrumb(); ?>
			<h1 class="entry-title"> 
        Digital Marketing White Papers
			</h1>
    </header>
    <div class="entry-intro white-paper-intro">
      <div class="intro-text">
        <h2>Free Business and Digital Marketing Strategy Downloads</h2>
        <p>Get the latest expertise and strategic direction in digital marketing topics from our specialists in these free PDF downloads.</p>
      </div>
    </div>
		<div class="archive-content index-entries">
			<?php
				if( have_posts() ) {
					while ( have_posts() ) { the_post();
						get_template_part( 'content', 'white-paper' );
					}
				} else {
					get_template_part( 'content', 'none' );
				}
			?>
			<?php smg_pagination(); ?>
    </div>
		<div id="white-paper-archive-content">
    	<?php FLBuilder::render_query(array( 'post_type' => 'fl-builder-template', 'p' => 13581 )); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>