<?php
/**
 * The default template for displaying content
 *
 * Used for both projects and case studies.
 *
 * @package SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */
?>

<?php
$cats = '';
foreach($terms as $key => $value) {
  if (is_array($value)) {
    $val = '';
    foreach($value as $cat) {
      $val .= $cat->term_id.',';
    }
  }
}
$cats = trim($val, ',');
?>
<a data-cat="<?php echo $cats; ?>" href="<?php echo $link; ?>" class="preview-content-block" style="background-image:url(<?php echo $image; ?>)">
  <div class="before">
    <p><?php echo $before; ?></p>
  </div>
  <div class="after">
    <?php echo $after; ?>
  </div>
  <div class="portfolio-mag"><i class="fas fa-search"></i></div>
</a>
