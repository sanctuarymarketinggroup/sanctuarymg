<?php
$args = array(
    'header-logo',
    'phone',
);
$settings = get_setting($args);
?>
<header id="header">
  <div id="header-gradient"></div>
  <div id="header-inner">
    <div id="logo">
      <a href="<?php echo HOME_URL; ?>"><?php echo acf_image($settings['header-logo']); ?></a>
    </div>
    <div id="menu-right">
      <div id="upper-menu">
        <a href="https://meetings.hubspot.com/tom816" class="sch-meet">
          <!-- <i class="fas fa-angle-right"></i><i class="small-only fas fa-user-clock"></i> -->
          <span>Schedule a Meeting</span>
        </a> 
        <a href="<?php echo get_permalink(2403); ?>" class="contact-header">
          <!-- <i class="fas fa-envelope"></i> -->
          <span>Contact Us</span>
        </a> 
        <a href="tel:<?php echo filter_phone($settings['phone']); ?>" class="phone-header">
          <!-- <i class="fas fa-phone"></i> -->
          <span><?php echo $settings['phone']; ?></span>
        </a>
      </div>
      <div id="menu">
        <div id="mobile-menu">
          <span>MENU</span>
          <div class="bars">
            <div class="bar1"></div>
            <div class="bar2"></div>
            <div class="bar3"></div>
          </div>
        </div>
        <?php smg_menu();?>
      </div>
    </div>
  </div>
</header>