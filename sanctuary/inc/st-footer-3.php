<?php
$args = array(
  'footer-logo',
  'address',
  'site-name'
);
$settings = get_setting( $args );
?>
<div id="copyright">
  <div id="copy-1">
    <?php echo acf_image( $settings['footer-logo'], '', array() ); ?>
    <p>
      <?php echo $settings['site-name']; ?><br>
      <?php echo $settings['address']['full']; ?><br>
      Copyright &copy; 2006-<?php echo date('Y'); ?>- All Rights Reserved.
  </p>
  </div>
  <div id="copy-3">
    <div class="g-partnersbadge" data-agency-id="6833063686"></div>
    <a href="https://www.bbb.org/us/oh/north-canton/profile/internet-marketing-services/sanctuary-marketing-group-0282-92004568" target="_blank"><img src="<?php echo THEME_ROOT; ?>/assets/images/BBB.png" alt="BBB rating" width="201" height="73" /></a>
    <img src="<?php echo THEME_ROOT; ?>/assets/images/weatherhead-100.jpg" alt="Weatherhead 100" width="201" height="73" />
  </div>
</div>
<div id="footer-menu"> 
  <?php smg_footer_menu(); ?>
</div>
