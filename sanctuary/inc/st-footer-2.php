<?php
$args = array(
  'social'
);
$settings = get_setting( $args );
?>
<div id="smg-social-bar">
  <div id="social-inner">
    <!-- <div>
      <h3>SUBSCRIBE TO THE OHIO DIGITAL MARKETER NEWSLETTER</h3>
      <p>A quick, weekly summary of digital marketing news and commentary.</p>
    </div> -->
    <!-- <div>
      <a href="#" class="button outline slim">Sign-up Today</a>
    </div> -->
    <div class="social-connect">
      <span>Connect with us:</span>
      <a class="smg-facebook" href="<?php echo $settings['social']['facebook']; ?>" target="_Blank">
        <?php echo get_svg('social'); ?>
      </a>
      <a class="smg-instagram" href="<?php echo $settings['social']['instagram']; ?>" target="_Blank">
        <?php echo get_svg('social'); ?>
      </a>
      <a class="smg-linkedin" href="<?php echo $settings['social']['linkedin']; ?>" target="_Blank">
        <?php echo get_svg('social'); ?>
      </a>
      <a class="smg-twitter" href="<?php echo $settings['social']['twitter']; ?>" target="_Blank">
        <?php echo get_svg('social'); ?>
      </a>
    </div>
  </div>
</div>