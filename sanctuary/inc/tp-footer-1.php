<?php
$args = array(
    'phone',
);
$settings = get_setting($args);
?>
<div id="grow-with-us" style="background-image:url('<?php echo wp_get_attachment_image_url(13557, "large"); ?>);">
  <div id="grow-inner">
    <h3>grow with us!</h3>
    <p>Set up a time to tell us about your business goals and needs: <a href="tel:<?php echo filter_phone($settings['phone']); ?>">Call <?php echo $settings['phone']; ?></a></p>
    <div id="grow-button-bar">
      <a href="https://meetings.hubspot.com/tom816" class="button outline slim sch-meet">Schedule a Meeting</a>
      <a href="<?php echo HOME_URL; ?>/contact-us/" class="button outline slim">Contact Us</a>
      <a href="https://www.sanctuarymg.com/contact-us/" class="button outline slim smg-chat-now">Connect with us</a>
    </div>
  </div>
</div>