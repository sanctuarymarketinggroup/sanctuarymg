<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

get_header(); 
//TO DO add in stuff from Theme options
$portfolio = get_field('team_single_header_image');
$featured_image = $portfolio ? $portfolio['url'] : get_the_post_thumbnail_url( get_the_id(), 'large' );
?>

<main id="team-single" class="contianer subpage" role="main">
<?php while ( have_posts() ) { the_post(); ?>
	<article class="main-content" id="post-<?php the_ID(); ?>">
	<?php $mobile_x_positioning = get_field('mobile_x_positioning'); 
		if( $mobile_x_positioning ) {
			?>
			<style>
				@media only screen and (max-width: 767px) {
					#team-single #team-header {
						background-position: <?php echo $mobile_x_positioning; ?>% center !important;
					}
				}
			</style>
			<?php
		}
	?>
		<div id="team-header" style="background-image:url(<?php echo $featured_image; ?>)">
			<div id="team-name-title">
				<h1 class="entry-title nonscripty">
					<?php the_title(); ?>
				</h1>
				<p>
				<?php echo get_field('position'); ?>
				</p>
			</div>
			<a class="slight-next" href="#team-content">
				<?php echo get_svg('down-arrow-1'); ?> 
			</a>
		</div>
		<div id="team-content" class="entry-content row-radial-gradient-2">
			<?php smg_breadcrumbs(); ?>
			<?php the_content(); ?>
		</div>
	</article>
	<?php 
	$tid = get_field('team_member');
	if($tid) {
		$gallery= array();
		$team_gallery = get_field('team_gallery',$tid);
		$photos = $team_gallery['photos'];
		$videos = $team_gallery['videos'];
		foreach($photos as $photo){
			$images = array(
				'medium' => $photo['sizes']['large'],
				'full' => $photo['url']
 			);
			array_push($gallery, $images);
		}
		foreach($videos as $video){
			$vidarray = array(
				'url' => $video['youtube_url'],
				'image' => $video['image']
			);
			array_push($gallery, $vidarray);
		}
		shuffle($gallery);
		echo '<div id="team-gallery">';
		$i = 1;
		$v = 1;
		foreach($gallery as $id) {
			$class='';
			if($i == 1 ){
				$class=" larger";
			} else if($i % 7 == 0 ){
				$class=" larger";
				$i = 1;
			} 
			if( $v % 15 == 0){
				$i = 0;
			}
			if($id['full'] !== null){
				echo '<div data-lightbox="'.$id['full'].'" class="light-popup team-gallery-bg  '.$class.'" style="background-image:url('.$id['medium'].')"></div>';
			} else { 
				$parts = parse_url($id['url']);
				parse_str($parts['query'], $query);
				//echo '<a href="'. $id .'" class="video-popup" vid="'.$query['v'] .'">';
					echo '<div data-vid="'.$query['v'].'" class="light-popup team-gallery-bg  '.$class.'" style="background-image:url(' . $id['image']['sizes']['large'] . ')"></div>';
				//echo '</a>';
			}
			$i++;
			$v++;
		}
		echo '</div>';
	}
	?>
<?php } ?>
</main>
<?php get_footer(); ?>
