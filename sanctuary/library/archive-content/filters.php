<?php
/**
 * Apply Filters for each of the different post types 
 * using the archive file - Block Layout
 */


/**
 * Portfolio Before Archive Content
 */
function portfolio_archive_before( $content ) {
	if( ! is_post_type_archive( 'portfolio' ) ) return $content;
	return '';
}
add_filter('before_block_content', 'portfolio_archive_before');

/**
 * Portfolio After Archive Content
 */
function portfolio_archive_after( $content ) {
  if( ! is_post_type_archive( 'portfolio' ) ) return $content;
	return '<h2>' . get_the_title() . '</h2>';
}
add_filter('after_block_content', 'portfolio_archive_after');

/**
 * Study Archive No Content
 */
function portfolio_archive_no_content( $content ) {
  if( ! is_post_type_archive( 'study' ) ) return $content;
	return $content;
}
add_filter('no_content_block_content', 'portfolio_archive_no_content');


/**
 * Study Before Archive Content
 */
function study_archive_before( $content ) {
	if( ! is_post_type_archive( 'study' ) ) return $content;
	return get_the_post_logo( get_the_id(), 'full' );
}
add_filter('before_block_content', 'study_archive_before');

/**
 * Study After Archive Content
 */
function study_archive_after( $content ) {
  if( ! is_post_type_archive( 'study' ) ) return $content;
	return '<h2>' . get_the_title() . '</h2>';
}
add_filter('after_block_content', 'study_archive_after');

/**
 * Study Archive No Content
 */
function study_archive_no_content( $content ) {
  if( ! is_post_type_archive( 'study' ) ) return $content;
	return 'Want To Be A Partner?';
}
add_filter('no_content_block_content', 'study_archive_no_content');


add_filter( 'get_the_archive_title', function ($title) {
	if ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$title = single_tag_title( '', false );
	} elseif ( is_author() ) {
		$title = '<span class="vcard">' . get_the_author() . '</span>' ;
	}
	return $title;
});