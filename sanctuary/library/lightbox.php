<?php

function set_lightbox_player() {
  echo '<div class="lightbox-smg">';
    echo '<div class="lightbox-close"></div>';
    echo '<div class="overlay"></div>';
    echo '<div class="lightbox-content"></div>';
  echo '</div>';
}
add_action('wp_footer', 'set_lightbox_player');