<?php

//Infinite Scroll
function wp_infinitepaginate_review(){
     $paged = $_POST['page_no'];
     
     query_posts( $arg );
     $args = array(
        'post_status' => 'publish',
        'post_type' => 'review',
        'posts_per_page' => '12',
        'paged' => $paged,
        'order' => 'DESC',
        'orderby' => 'date',
    );

    $the_query = new WP_Query($args);
    if ($the_query->have_posts()) {
        while ($the_query->have_posts()) {$the_query->the_post();
            show_template('content-review', array(), './');
        }
        wp_reset_postdata();
    }

    die();

}
add_action('wp_ajax_review_infinite_scroll', 'wp_infinitepaginate_review'); // for logged in user
add_action('wp_ajax_nopriv_review_infinite_scroll', 'wp_infinitepaginate_review'); // if user not logged in