<?php
/**
 * Add all Custom Post Types or Taxonomies
 * 
 * Use {@link https://generatewp.com/}
 * 
 * @package WordPress
 * @subpackage SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */


// Register Case Studies Post Type
function case_studies_post_type() {

	$labels = array(
		'name'                  => _x( 'Case Studies', 'Post Type General Name', 'sanctuary' ),
		'singular_name'         => _x( 'Case Study', 'Post Type Singular Name', 'sanctuary' ),
		'menu_name'             => __( 'Case Studies', 'sanctuary' ),
		'name_admin_bar'        => __( 'Case Study', 'sanctuary' ),
		'archives'              => __( 'Study Archives', 'sanctuary' ),
		'attributes'            => __( 'Study Attributes', 'sanctuary' ),
		'parent_item_colon'     => __( 'Parent Study:', 'sanctuary' ),
		'all_items'             => __( 'All Studies', 'sanctuary' ),
		'add_new_item'          => __( 'Add New Study', 'sanctuary' ),
		'add_new'               => __( 'Add New', 'sanctuary' ),
		'new_item'              => __( 'New Study', 'sanctuary' ),
		'edit_item'             => __( 'Edit Study', 'sanctuary' ),
		'update_item'           => __( 'Update Study', 'sanctuary' ),
		'view_item'             => __( 'View Study', 'sanctuary' ),
		'view_items'            => __( 'View Studies', 'sanctuary' ),
		'search_items'          => __( 'Search Study', 'sanctuary' ),
		'not_found'             => __( 'Not found', 'sanctuary' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sanctuary' ),
		'featured_image'        => __( 'Featured Image', 'sanctuary' ),
		'set_featured_image'    => __( 'Set featured image', 'sanctuary' ),
		'remove_featured_image' => __( 'Remove featured image', 'sanctuary' ),
		'use_featured_image'    => __( 'Use as featured image', 'sanctuary' ),
		'insert_into_item'      => __( 'Insert into study', 'sanctuary' ),
		'uploaded_to_this_item' => __( 'Uploaded to this study', 'sanctuary' ),
		'items_list'            => __( 'Studies list', 'sanctuary' ),
		'items_list_navigation' => __( 'Studies list navigation', 'sanctuary' ),
		'filter_items_list'     => __( 'Filter studies list', 'sanctuary' ),
	);
	$rewrite = array(
		'slug'                  => '/business-growth/case-studies',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Case Study', 'sanctuary' ),
		'description'           => __( 'Post Type Description', 'sanctuary' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'business-growth/case-studies',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'study', $args );

}
add_action( 'init', 'case_studies_post_type', 0 );


// Register Client Reviews Post Type
function client_reviews_post_type() {

	$labels = array(
		'name'                  => _x( 'Client Reviews', 'Post Type General Name', 'sanctuary' ),
		'singular_name'         => _x( 'Client Review', 'Post Type Singular Name', 'sanctuary' ),
		'menu_name'             => __( 'Client Reviews', 'sanctuary' ),
		'name_admin_bar'        => __( 'Client Review', 'sanctuary' ),
		'archives'              => __( 'Review Archives', 'sanctuary' ),
		'attributes'            => __( 'Review Attributes', 'sanctuary' ),
		'parent_item_colon'     => __( 'Parent Review:', 'sanctuary' ),
		'all_items'             => __( 'All Reviews', 'sanctuary' ),
		'add_new_item'          => __( 'Add New Review', 'sanctuary' ),
		'add_new'               => __( 'Add New', 'sanctuary' ),
		'new_item'              => __( 'New Review', 'sanctuary' ),
		'edit_item'             => __( 'Edit Review', 'sanctuary' ),
		'update_item'           => __( 'Update Review', 'sanctuary' ),
		'view_item'             => __( 'View Review', 'sanctuary' ),
		'view_items'            => __( 'View Reviews', 'sanctuary' ),
		'search_items'          => __( 'Search Review', 'sanctuary' ),
		'not_found'             => __( 'Not found', 'sanctuary' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sanctuary' ),
		'featured_image'        => __( 'Featured Image', 'sanctuary' ),
		'set_featured_image'    => __( 'Set featured image', 'sanctuary' ),
		'remove_featured_image' => __( 'Remove featured image', 'sanctuary' ),
		'use_featured_image'    => __( 'Use as featured image', 'sanctuary' ),
		'insert_into_item'      => __( 'Insert into review', 'sanctuary' ),
		'uploaded_to_this_item' => __( 'Uploaded to this review', 'sanctuary' ),
		'items_list'            => __( 'Reviews list', 'sanctuary' ),
		'items_list_navigation' => __( 'Reviews list navigation', 'sanctuary' ),
		'filter_items_list'     => __( 'Filter reviews list', 'sanctuary' ),
	);
	$rewrite = array(
		'slug'                  => 'client-reviews',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Client Review', 'sanctuary' ),
		'description'           => __( 'Post Type Description', 'sanctuary' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'business-growth/client-reviews',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'review', $args );

}
add_action( 'init', 'client_reviews_post_type', 0 );

if ( ! function_exists('smg_projects_code') ) {

	// Register Custom Post Type
	function smg_projects_code() {
	
		$labels = array(
			'name'                  => _x( 'Recent Work', 'Post Type General Name', 'sanctuary' ),
			'singular_name'         => _x( 'Recent Work', 'Post Type Singular Name', 'sanctuary' ),
			'menu_name'             => __( 'Recent Works', 'sanctuary' ),
			'name_admin_bar'        => __( 'Recent Work', 'sanctuary' ),
			'archives'              => __( 'Recent Work Archives', 'sanctuary' ),
			'attributes'            => __( 'Recent Work Attributes', 'sanctuary' ),
			'parent_item_colon'     => __( 'Parent Recent Work:', 'sanctuary' ),
			'all_items'             => __( 'All Recent Work', 'sanctuary' ),
			'add_new_item'          => __( 'Add New Recent Work', 'sanctuary' ),
			'add_new'               => __( 'Add New', 'sanctuary' ),
			'new_item'              => __( 'New Recent Work', 'sanctuary' ),
			'edit_item'             => __( 'Edit Recent Work', 'sanctuary' ),
			'update_item'           => __( 'Update Recent Work', 'sanctuary' ),
			'view_item'             => __( 'View Recent Work', 'sanctuary' ),
			'view_items'            => __( 'View Recent Work', 'sanctuary' ),
			'search_items'          => __( 'Search Recent Work', 'sanctuary' ),
			'not_found'             => __( 'Not found', 'sanctuary' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'sanctuary' ),
			'featured_image'        => __( 'Featured Image', 'sanctuary' ),
			'set_featured_image'    => __( 'Set featured image', 'sanctuary' ),
			'remove_featured_image' => __( 'Remove featured image', 'sanctuary' ),
			'use_featured_image'    => __( 'Use as featured image', 'sanctuary' ),
			'insert_into_item'      => __( 'Insert into project', 'sanctuary' ),
			'uploaded_to_this_item' => __( 'Uploaded to this project', 'sanctuary' ),
			'items_list'            => __( 'Projects list', 'sanctuary' ),
			'items_list_navigation' => __( 'Projects list navigation', 'sanctuary' ),
			'filter_items_list'     => __( 'Filter projects list', 'sanctuary' ),
		);
		$rewrite = array(
			'slug'                  => '/business-growth/portfolio',
			'with_front'            => true,
			'pages'                 => true,
			'feeds'                 => true,
		);
		$args = array(
			'label'                 => __( 'Recent Work', 'sanctuary' ),
			'description'           => __( 'Recent Works Portfolio', 'sanctuary' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'revisions', 'page-attributes' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => 'business-growth/portfolio',
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'rewrite'               => $rewrite,
			'capability_type'       => 'page',
			'show_in_rest'          => true,
		);
		register_post_type( 'portfolio', $args );
	
	}
	add_action( 'init', 'smg_projects_code', 0 );
	
}


/**
 * white_pages_post_type
 * 
 * Register White Pages Post Type
 * 
 * @since 2.0.0
 */
if (! function_exists('white_pages_post_type')) {
	function white_pages_post_type() {

		$labels = array(
			'name'                  => _x( 'White Papers', 'Post Type General Name', 'sanctuary' ),
			'singular_name'         => _x( 'White Paper', 'Post Type Singular Name', 'sanctuary' ),
			'menu_name'             => __( 'White Papers', 'sanctuary' ),
			'name_admin_bar'        => __( 'White Paper', 'sanctuary' ),
			'archives'              => __( 'Paper Archives', 'sanctuary' ),
			'attributes'            => __( 'Paper Attributes', 'sanctuary' ),
			'parent_item_colon'     => __( 'Parent Paper:', 'sanctuary' ),
			'all_items'             => __( 'All Papers', 'sanctuary' ),
			'add_new_item'          => __( 'Add New Paper', 'sanctuary' ),
			'add_new'               => __( 'Add New', 'sanctuary' ),
			'new_item'              => __( 'New Paper', 'sanctuary' ),
			'edit_item'             => __( 'Edit Paper', 'sanctuary' ),
			'update_item'           => __( 'Update Paper', 'sanctuary' ),
			'view_item'             => __( 'View Paper', 'sanctuary' ),
			'view_items'            => __( 'View Papers', 'sanctuary' ),
			'search_items'          => __( 'Search Paper', 'sanctuary' ),
			'not_found'             => __( 'Not found', 'sanctuary' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'sanctuary' ),
			'featured_image'        => __( 'Featured Image', 'sanctuary' ),
			'set_featured_image'    => __( 'Set featured image', 'sanctuary' ),
			'remove_featured_image' => __( 'Remove featured image', 'sanctuary' ),
			'use_featured_image'    => __( 'Use as featured image', 'sanctuary' ),
			'insert_into_item'      => __( 'Insert into paper', 'sanctuary' ),
			'uploaded_to_this_item' => __( 'Uploaded to this paper', 'sanctuary' ),
			'items_list'            => __( 'Papers list', 'sanctuary' ),
			'items_list_navigation' => __( 'Papers list navigation', 'sanctuary' ),
			'filter_items_list'     => __( 'Filter papers list', 'sanctuary' ),
		);
		$rewrite = array(
			'slug'                  => '/white-papers',
			'with_front'            => true,
			'pages'                 => true,
			'feeds'                 => true,
		);
		$args = array(
			'label'                 => __( 'White Papers', 'sanctuary' ),
			'description'           => __( 'Post Type Description', 'sanctuary' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => 'white-papers',
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'rewrite'               => $rewrite,
			'capability_type'       => 'page',
			'show_in_rest'          => true,
		);
		register_post_type( 'white-paper', $args );

	}
	add_action( 'init', 'white_pages_post_type', 0 );
}


if ( ! function_exists('smg_projects_tax') ) {
	// Register Custom Taxonomy
	function smg_projects_tax() {

		$labels = array(
			'name'                       => _x( 'Recent Work Categories', 'Taxonomy General Name', 'text_domain' ),
			'singular_name'              => _x( 'Recent Work Category', 'Taxonomy Singular Name', 'text_domain' ),
			'menu_name'                  => __( 'Recent Work Category', 'text_domain' ),
			'all_items'                  => __( 'All Categories', 'text_domain' ),
			'parent_item'                => __( 'Parent Category', 'text_domain' ),
			'parent_item_colon'          => __( 'Parent Category:', 'text_domain' ),
			'new_item_name'              => __( 'New Category Name', 'text_domain' ),
			'add_new_item'               => __( 'Add New Category', 'text_domain' ),
			'edit_item'                  => __( 'Edit Category', 'text_domain' ),
			'update_item'                => __( 'Update Category', 'text_domain' ),
			'view_item'                  => __( 'View Category', 'text_domain' ),
			'separate_items_with_commas' => __( 'Separate categories with commas', 'text_domain' ),
			'add_or_remove_items'        => __( 'Add or remove categories', 'text_domain' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
			'popular_items'              => __( 'Popular Categories', 'text_domain' ),
			'search_items'               => __( 'Search Categories', 'text_domain' ),
			'not_found'                  => __( 'Not Found', 'text_domain' ),
			'no_terms'                   => __( 'No categories', 'text_domain' ),
			'items_list'                 => __( 'Categories list', 'text_domain' ),
			'items_list_navigation'      => __( 'Categories list navigation', 'text_domain' ),
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => false,
			'show_in_rest'          => true,
		);
		register_taxonomy( 'portfolio_cat', array( 'portfolio' ), $args );

	}
	add_action( 'init', 'smg_projects_tax', 0 );
}