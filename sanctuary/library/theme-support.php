<?php
/**
 * Register theme support for languages, menus, post-thumbnails, post-formats etc.
 *
 * @package WordPress
 * @subpackage SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

/**
 * Add Theme Support for Wordpress Settings
 */
if ( ! function_exists( 'smg_theme_support' ) ) {
  function smg_theme_support() {
    // Add language support
    load_theme_textdomain( 'smg', get_template_directory() . '/languages' );

    // Switch default core markup for search form, comment form, and comments to output valid HTML5
    add_theme_support( 'html5', array(
      'search-form',
      'comment-form',
      'comment-list',
      'gallery',
      'caption',
    ) );

    // Add menu support
    add_theme_support( 'menus' );

    // Let WordPress manage the document title
    add_theme_support( 'title-tag' );

    // Add post thumbnail support: http://codex.wordpress.org/Post_Thumbnails
    add_theme_support( 'post-thumbnails' );

    // RSS thingy
    add_theme_support( 'automatic-feed-links' );

    // Add post formats support: http://codex.wordpress.org/Post_Formats
    add_theme_support( 'post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat') );

    // Declare WooCommerce support per http://docs.woothemes.com/document/third-party-custom-theme-compatibility/
    add_theme_support( 'woocommerce' );

    // Add app.css as editor style https://codex.wordpress.org/Editor_Style
    add_editor_style( 'assets/stylesheets/app.css' );
  }
  add_action( 'after_setup_theme', 'smg_theme_support' );
}


/**
 * Add Theme Support for ACF Options Page
 * @title Theme Settings
 */
if ( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
	));
}