<?php

/**
 * This is an example module with only the basic
 * setup necessary to get it working.
 *
 * @class SMGPostsModule
 */
class SMGPostsModule extends FLBuilderModule {

  /** 
   * Constructor function for the module. You must pass the
   * name, description, dir and url in an array to the parent class.
   *
   * @method __construct
   */  
  public function __construct()
  {
    parent::__construct(array(
        'name'          => __('Posts Preview', 'fl-builder'),
        'description'   => __('Pull a handful of your most recent posts.', 'fl-builder'),
        'category'		=> __('Custom', 'fl-builder'),
        'dir'           => SMG_MODULE_DIR . 'modules/smg-posts/',
        'url'           => SMG_MODULE_URL . 'modules/smg-posts/',
        'editor_export' => true, // Defaults to true and can be omitted.
        'enabled'       => true, // Defaults to true and can be omitted.
    ));
  }

  public static function get_users() {
    $response = wp_remote_get( 'https://sanctuarymg.com/academy/wp-json/wp/v2/users/?per_page=100' );
    $body = wp_remote_retrieve_body( $response );
    $body = json_decode($body);
		 	
    $authors = array( '0' => 'None' );
    foreach($body as $user) {
      $authors[$user->id] = $user->name;
    }
    return $authors;
  }

  public static function get_categories() {
    $cats = array( '0' => 'None');
    // $categories = get_categories( array( 'orderby' => 'name' ) );
    $response = wp_remote_get( 'https://sanctuarymg.com/academy/wp-json/wp/v2/categories?per_page=100' );
    $body = wp_remote_retrieve_body( $response );
    $body = json_decode($body);
    foreach ( $body as $category ) {
      $cats[$category->id] = $category->name;
    }

    return $cats;
  }
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('SMGPostsModule', array(
  'general' => array( // Tab
    'title' => __('General', 'fl-builder'), // Tab title
    'sections' => array( // Tab Sections
      'general' => array( // Section
        'title' => __('Settings', 'fl-builder'), // Section Title
        'fields' => array( // Section Fields
          'title_select' => array(
            'type'          => 'select',
            'label'         => __('Title', 'fl-builder'),
            'options'       => array(
              'true'            => __('Yes', 'fl-builder'),
              'false'           => __('No', 'fl-builder')
            ),
            'toggle'        => array(
              'true'            => array(
                'fields'            => array('title_text')
              ),
              'false'           => array()
            )
          ),
          'title_text' => array(
            'type'          => 'text',
            'label'         => __('Title', 'fl-builder')
          ),
          'posts' => array(
            'type'          => 'unit',
            'label'         => __( '# of posts', 'fl-builder' ),
            'description'   => 'posts',
            'default'       => '3'
          ),
          'name' => array(
            'type'          => 'text',
            'label'         => __('Display Name', 'fl-builder')
          ),
          'author' => array(
            'type'          => 'select',
            'label'         => __('Author', 'fl-builder'),
            'options'       => SMGPostsModule::get_users()
          ),
          'category' => array(
            'type'          => 'select',
            'label'         => __('Category', 'fl-builder'),
            'options'       => SMGPostsModule::get_categories()
          ),
          'postids' => array(
            'type'          => 'text',
            'label'         => __('Post IDs', 'fl-builder')
          ),
          'readmore_select' => array(
            'type'          => 'select',
            'label'         => __('Read More', 'fl-builder'),
            'options'       => array(
              'true'            => __('Yes', 'fl-builder'),
              'false'           => __('No', 'fl-builder')
            )
          )
        )
      )
    )
  )
));