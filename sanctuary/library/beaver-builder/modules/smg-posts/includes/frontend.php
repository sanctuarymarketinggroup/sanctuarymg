<?php

/**
 * This file should be used to render each module instance.
 * You have access to two variables in this file: 
 * 
 * $module An instance of your module class.
 * $settings The module's settings.
 *
 * Example: 
 */
$urlArgs = '';

$args = array(
  'posts_per_page' => (int)$settings->posts ? (int)$settings->posts : 3
);

if ('0' != $settings->author) {
  $args['author'] = $settings->author;
  $urlArgs .= '&author='.$settings->author;
}

if ('0' != $settings->category) {
  $args['cat'] = $settings->category;
  $urlArgs .= '&categories='.$settings->category;
}

if('' != $settings->postids) {
  $args['post__in'] = explode(',', $settings->postids);
  foreach($args['post__in'] as $id){
    $urlArgs .= '&include[]='.$id;
  }
}
// $articles = new WP_Query( $args );

$url = 'https://sanctuarymg.com/academy/wp-json/wp/v2/posts/?per_page='.$settings->posts.$urlArgs;

$response = wp_remote_get( $url );
$body = wp_remote_retrieve_body( $response );
$body = json_decode($body);

?>

<div class="fl-smg-posts node-<?php echo $id; ?>">
  <?php
    // The Loop
    // if( $articles->have_posts() ) {
    if( $response['response']['code'] == 200 ) {
      if ($settings->title_select == 'true') {
        echo '<h2 class="fl-smg-posts-title">' . $settings->title_text . '</h2>';
      }

      echo '<div class="archive-content index-entries">';
      foreach($body as $acad_post){
        ?>
        <article class="main-content entry" id="post-<?php echo $acad_post->id; ?>">
          <div class="entry-image" onmouseenter="jQuery(this).parent().find('h2 a').addClass('hovered')" onmouseleave="jQuery(this).parent().find('h2 a').removeClass('hovered')">
            <a href="<?php echo $acad_post->link; ?>" target="_Blank">
              <?php
                if ($acad_post->fimg_url) {
                  echo '<img src="'.$acad_post->fimg_url.'" alt="'.$acad_post->title->rendered.'" />';
                } else {
                  echo wp_get_attachment_image('11624', 'medium');
                }
              ?>
            </a>
          </div>
          <h2 class="entry-title">
            <a href="<?php echo $acad_post->link; ?>" target="_Blank"><?php echo $acad_post->title->rendered; ?></a>
          </h2>
          <div class="entry-content">
            <?php
            echo $acad_post->excerpt->rendered; ?>
          </div>
        </article>
        <?php
      }
      // while ( $articles->have_posts() ) { $articles->the_post();
      //   get_template_part( 'content' );
      // }
      echo '</div>';
      
      if ($settings->readmore_select == 'true') {
        echo '<div>';
          echo '<a class="fl-smg-posts-author" href="' . $body[0]->a_url . '" target="_Blank">Click here to view more from ' . $settings->name . '</a>';
        echo '</div>';
      }
    } else {
      // get_template_part( 'content', 'none' );
    }
    /* Restore original Post Data */
    wp_reset_postdata();
  ?>
</div>