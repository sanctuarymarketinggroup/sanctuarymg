<?php

/**
 * This is an example module with only the basic
 * setup necessary to get it working.
 *
 * @class FLLocationModule
 */
class SmgPortfolio extends FLBuilderModule {

    /** 
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */  
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Portfolio', 'fl-builder'),
            'description'   => __('SMG Portfolio', 'fl-builder'),
            'category'		=> __('Custom', 'fl-builder'),
            'dir'           => SMG_MODULE_DIR . 'modules/smg-portfolio/',
            'url'           => SMG_MODULE_URL . 'modules/smg-portfolio/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));
    }
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('SmgPortfolio', array(
  'general'       => array( // Tab
    'title'         => __('General', 'fl-builder'), // Tab title
    'sections'      => array( // Tab Sections
      'general'       => array( // Section
      'label'         => __('Settings', 'fl-builder'), // Section Title
        'fields'        => array( // Section Fields
          'item_photo' => array(
            'type'          => 'photo',
            'label'         => __('Item Photo', 'fl-builder'),
            'show_remove'   => true,
          ),
          'item_text' => array(
            'type'          => 'editor',
            'media_buttons' => false,
            'wpautop'       => true
          ),
        )
      ),
    )
  )
));