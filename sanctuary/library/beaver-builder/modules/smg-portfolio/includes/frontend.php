<?php
$args = array(
  'post_type' => 'portfolio',
  'showposts' => 10,
  'meta_key' => '_featured_post',
  'meta_value' => 'yes'
);
// The Query
$the_query = new WP_Query( $args );
// The Loop
if ( $the_query->have_posts() ) { 
  ?>
  <div id="front-portfolio-slider">
  <?php
  while ( $the_query->have_posts() ) {
    $the_query->the_post();
    ?>
    <div class="portfolio-item">
      <div class="portfolio-inner">
      <?php echo '<a href="'. HOME_URL .'/business-growth/portfolio/">' . acf_image(get_field('portfolio_slider_desktop_image')) . '</a>';
      $mobile_image = get_field('portfolio_slider_mobile_image');
      if( isset ( $mobile_image ) ) {
        $url = $mobile_image['url'];
        $title = $mobile_image['title'];
        $alt = $mobile_image['alt'];
        $size = 'full';
        $thumb = $mobile_image['sizes'][ $size ];
        $width = $mobile_image['sizes'][ $size . '-width' ];
        $height = $mobile_image['sizes'][ $size . '-height' ];
        ?>
        <div class="mobile-phone">
          <div class="mobile-inner">
            <a href="<?php echo HOME_URL; ?>/business-growth/portfolio/"><img src="<?php echo $url; ?>" alt="Portfolio Image" width="<?php echo $width; ?>" height="<?php echo $height; ?>" /></a>
          </div> 
        </div>
        <?php
      }
      ?>
      </div>
    </div>
    <?php
  }
  ?>
  </div>
  <?php
  /* Restore original Post Data */
  wp_reset_postdata();
} else {
  // no posts found
}
