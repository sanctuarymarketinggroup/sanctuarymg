<?php

/**
 * This is an example module with only the basic
 * setup necessary to get it working.
 *
 * @class SMGCaseStudiesModule
 */
class SMGCaseStudiesModule extends FLBuilderModule {

    /** 
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */  
    public function __construct()
    {
      parent::__construct(array(
          'name'          => __('Case Studies', 'fl-builder'),
          'description'   => __('Add case studies to any page.', 'fl-builder'),
          'category'		=> __('Custom', 'fl-builder'),
          'dir'           => SMG_MODULE_DIR . 'modules/case-studies/',
          'url'           => SMG_MODULE_URL . 'modules/case-studies/',
          'editor_export' => true, // Defaults to true and can be omitted.
          'enabled'       => true, // Defaults to true and can be omitted.
      ));
    }
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('SMGCaseStudiesModule', array(
  'general' => array( // Tab
    'title' => __('General', 'fl-builder'), // Tab title
    'sections' => array( // Tab Sections
      'general' => array( // Section
        'title' => __('Settings', 'fl-builder'), // Section Title
        'fields' => array( // Section Fields
          'test' => array(
            'type'          => 'text',
            'label'         => __('Include Posts', 'fl-builder'),
          ),
        )
      )
    )
  )
));