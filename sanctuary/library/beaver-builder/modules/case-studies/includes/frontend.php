<?php

/**
 * This file should be used to render each module instance.
 * You have access to two variables in this file: 
 * 
 * $module An instance of your module class.
 * $settings The module's settings.
 *
 * Example: 
 */

?>

<div class="featured-studies node-<?php echo $id; ?>">
<?php
  if( $settings->test ){
    $includes = explode(',',$settings->test);

    $args = array(
      'post_type'  => 'study',
      'showposts'  => -1,
      'post__in'   => $includes
    );
  } else {
    $args = array(
      'post_type'  => 'study',
      'showposts'  => 4,
      'meta_key'   => '_featured_post',
      'meta_value' => 'yes'
    );
  }
  $studies = new WP_Query( $args );
  if ( $studies->have_posts() ) {
    while ( $studies->have_posts() ) { $studies->the_post();
      $what = get_field( 'what' );
      $who = get_field( 'who' );
      $statement = get_field( 'statement' );
      echo '<div class="study">';
        echo '<div class="flipper">';
          echo '<div class="inner">';
          echo get_field('case_study_flip_image') ? acf_image( get_field('case_study_flip_image'), '', array('post-thumbnail', 'case-bg') ) : '';
            echo '<div class="wrapper">';
              echo '<div>';
                echo '<div class="wrap">';
                  echo acf_image(get_field('case_study_flip_logo'), '', array());
                  //echo get_the_post_logo( get_the_id(), 'full' );
                echo '</div>';
                echo '<a href="' . get_permalink() . '" class="button outline">Case Study</a>';
              echo '</div>';
            echo '</div>';
          echo '</div>';
          echo '<div class="inner flip">';
            echo '<div class="wrap">';
              echo '<div>';
                 if( $what ) { 
                  echo '<div class="item"><img src="' . THEME_ROOT . '/assets/images/flipper-checkmark.png" width="35" height="41" alt="checkmark" /><span>' . $what . '</span></div>';
                }
                if( $who ) {
                  echo '<div class="item"><img src="' . THEME_ROOT . '/assets/images/flipper-location.png" width="41" height="45" alt="location" /><span>' . $who . '</span></div>';
                }
                if( $statement ) {
                  echo '<div class="item"><img src="' . THEME_ROOT . '/assets/images/flipper-quote.png" width="40" height="46" alt="quote" /><span>' . $statement . '</span></div>';
                }

              echo '</div>';
            echo '</div>';
            echo '<a href="' . get_permalink() . '" class="button outline">Click to Read Case Study</a>';
          echo '</div>';
        echo '</div>';
      echo '</div>';
    }
    wp_reset_postdata();
  } else {
    _e('<p>Sorry no case studies have been marked "featured".</p>', 'smg');
  }
?>
</div>