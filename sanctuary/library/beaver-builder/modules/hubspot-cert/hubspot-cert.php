<?php

/**
 * This is an example module with only the basic
 * setup necessary to get it working.
 *
 * @class SMGHubspotCertModule
 */
class SMGHubspotCertModule extends FLBuilderModule {

    /** 
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */  
    public function __construct()
    {
      parent::__construct(array(
          'name'          => __('HubSpot Certifications', 'fl-builder'),
          'description'   => __('Add up to 5 HubSpot Certifications', 'fl-builder'),
          'category'		=> __('Custom', 'fl-builder'),
          'dir'           => SMG_MODULE_DIR . 'modules/hubspot-cert/',
          'url'           => SMG_MODULE_URL . 'modules/hubspot-cert/',
          'editor_export' => true, // Defaults to true and can be omitted.
          'enabled'       => true, // Defaults to true and can be omitted.
      ));
    }
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('SMGHubspotCertModule', array(
  'general' => array( // Tab
    'title' => __('General', 'fl-builder'), // Tab title
    'sections' => array( // Tab Sections
      'general' => array( // Section
        'title' => __('Settings', 'fl-builder'), // Section Title
        'fields' => array( // Section Fields
          'hubspot' => array(
						'type'         => 'form',
						'label'        => __( 'Certifications', 'fl-builder' ),
						'form'         => 'HubspotForm', // ID from registered form below
            'multiple'     => true,
            'limit'        => 5
          ),
        )
      )
    )
  )
));


/**
 * Register a settings form to use in the "form" field type above.
 */
FLBuilder::register_settings_form('HubspotForm', array(
	'title' => __( 'Add Certification', 'fl-builder' ),
	'tabs'  => array(
		'general' => array(
			'title'    => __( 'General', 'fl-builder' ),
			'sections' => array(
				'general' => array(
					'title'  => '',
					'fields' => array(
            'name' => array(
              'type'          => 'text',
              'label'         => __( 'User Name', 'fl-builder' )
            ),
						'cert' => array(
              'type'          => 'text',
              'label'         => __( 'Certification Name', 'fl-builder' )
            ),
					)
				)
			)
		)
	)
));