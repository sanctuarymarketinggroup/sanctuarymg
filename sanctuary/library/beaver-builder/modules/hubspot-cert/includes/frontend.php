<?php

/**
 * This file should be used to render each module instance.
 * You have access to two variables in this file: 
 * 
 * $module An instance of your module class.
 * $settings The module's settings.
 *
 * Example: 
 */

?>

<div class="hubspot-certifications node-<?php echo $id; ?>">
<?php
  if ( 0 < count($settings->hubspot ) ) {
    for ( $i = 0; $i < count($settings->hubspot ); $i++ ) { 
      if ( empty($settings->hubspot[ $i ] ) ) continue; ?>
        <div class="hub-cert">
          <div class="hub-cert-image">
            <img src="<?php echo get_stylesheet_directory_uri(). '/assets/images/hubspot-partner.png'; ?>" alt="HubSpot Partner" />
          </div>
          <div class="hub-cert-info">
            <span class="hub-cert-user"><?php echo $settings->hubspot[ $i ]->name; ?></span>
            <span class="hub-cert-name"><?php echo $settings->hubspot[ $i ]->cert; ?></span>
          </div>
        </div>
        <?php
      }
    }
?>
</div>