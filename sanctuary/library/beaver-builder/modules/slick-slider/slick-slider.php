<?php

/**
 * This is an example module with only the basic
 * setup necessary to get it working.
 *
 * @class FLLocationModule
 */
class SmgSlickSlider extends FLBuilderModule {

  /** 
   * Constructor function for the module. You must pass the
   * name, description, dir and url in an array to the parent class.
   *
   * @method __construct
   */  
  public function __construct()
  {
      parent::__construct(array(
          'name'          => __('Slider', 'fl-builder'),
          'description'   => __('A basic slider module based on slick slider', 'fl-builder'),
          'category'		=> __('Custom', 'fl-builder'),
          'dir'           => SMG_MODULE_DIR . 'modules/slick-slider/',
          'url'           => SMG_MODULE_URL . 'modules/slick-slider/',
          'editor_export' => true, // Defaults to true and can be omitted.
          'enabled'       => true, // Defaults to true and can be omitted.
      ));
      
      /** 
       * Use these methods to enqueue css and js already
       * registered or to register and enqueue your own.
       */
      // Already registered

      // Register and enqueue your own

  }

}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('SmgSlickSlider', array(
  'content' => array( // Tab
    'title' => __('Content', 'fl-builder'), // Tab title
    'sections' => array( // Tab Sections
      'general' => array( // Section
        'label' => __('Content', 'fl-builder'), // Section Title
        'fields' => array( // Section Fields
          'source' => array( // Field
            'type'    => 'select',
            'label'   => __('Source', 'fl-builder'),
            'default' => 'text',
            'options' => array(
              'text'       => __( 'Text', 'fl-builder' ),
              'post_type' => __( 'Post Type', 'fl-builder' ),
              // Here for Future Integration
              // 'image'      => __( 'Images', 'fl-builder' ),
            ),
            'toggle'  => array(
              'text' => array (
                'sections' => array('text')
              ),
              'post_type' => array (
                'sections' => array('post_type')
              ),
              // Here for Future Integration
              // 'image' => array (
              //   'sections' => array('image')
              // )
            )
          ),
        )
      ),
      'text' => array( // Section
        'label' => __('Settings', 'fl-builder'), // Section Title
        'fields' => array( // Section Fields
          'text' => array( // Field
            'type'     => 'textarea',
            'label'    => __( 'My Text Field', 'fl-builder' ),
            'multiple' => true
          ),
        )
      ),
      'post_type' => array( // Section
        'label' => __('Settings', 'fl-builder'), // Section Title
        'fields' => array( // Section Fields
          'post_type' => array( // Field
            'type' => 'select',
            'label' => __('Content', 'fl-builder'),
            'options' => 'SMG_Custom_Modules_Loader::register_post_types_field',
          ),
          'count' => array(
            'type' => 'unit',
            'label' => __('Posts to Show', 'fl-builder'),
            'default' => 2,
            'description' => '# of posts'
          ),
          'now' => array(
            'type' => 'unit',
            'label' => __('Number of Words', 'fl-builder'),
            'default' => 100,
            'description' => 'Number of Words. -1 disables trimming of words.'
          )
        )
      ),
      // Here for Future Integration
      // 'image' => array( // Section
      //   'label' => __('Settings', 'fl-builder'), // Section Title
      //   'fields' => array( // Section Fields
      //     'test' => array( // Field
      //       'type' => 'photo',
      //       'label' => __('Content', 'fl-builder'),
      //     )
      //   )
      // )
    )
  ),
  'settings'       => array( // Tab
    'title'         => __('Settings', 'fl-builder'), // Tab title
    'sections'      => array( // Tab Sections
      'general'       => array( // Section
        // 'label'         => __('Settings', 'fl-builder'), // Section Title
        'fields'        => array( // Section Fields
          'width' => array(
            'type'          => 'unit',
            'label'         => __('Field Width', 'fl-builder'),
            'default' => '768',
            'description'     => 'px',
            'responsive' => true
          ),
          'autoplay' => array(
            'type'          => 'select',
            'label'         => __('Autoplay', 'fl-builder'),
            'default'       => 'false',
            'options'       => array(
              'false'      => __( 'No', 'fl-builder' ),
              'true'      => __( 'Yes', 'fl-builder' )
            ),
            'toggle'  => array(
              'true' => array (
                'fields' => array('speed')
              )
            )
          ),
          'speed' => array(
            'type'          => 'unit',
            'label'         => __('Speed', 'fl-builder'),
            'description'     => 'ms',
          ),
          'arrows' => array(
            'type'          => 'select',
            'label'         => __('Arrows', 'fl-builder'),
            'default'       => 'false',
            'options'       => array(
              'true'      => __( 'Yes', 'fl-builder' ),
              'false'      => __( 'No', 'fl-builder' )
            ),
            'toggle'  => array(
              'true' => array (
                'fields' => array('left_arrow', 'right_arrow')
              )
            )
          ),
          'left_arrow' => array(
            'type' => 'photo',
            'label' => __('Left Arrow', 'fl-builder'),
            'show_remove' => true,
          ),
          'right_arrow' => array(
            'type'          => 'photo',
            'label'         => __('Right Arrow', 'fl-builder'),
            'show_remove'   => true,
          ),
          'dots' => array(
            'type'          => 'select',
            'label'         => __('Dots', 'fl-builder'),
            'default'       => 'no',
            'options'       => array(
              'no'      => __( 'No', 'fl-builder' ),
              'yes'      => __( 'Yes', 'fl-builder' )
            ),
          ),
          'slidesToScroll' => array(
            'type'          => 'unit',
            'label'         => __('Slides to Scroll', 'fl-builder'),
            'description'   => 'slides'
          ),
        )
      )
    )
  )
));