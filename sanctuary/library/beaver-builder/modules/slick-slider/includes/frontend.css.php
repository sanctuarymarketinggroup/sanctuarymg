.node-<?php echo $id; ?> {
  
}
<?php if ( '' !== $settings->width_responsive ){ ?>
  .node-<?php echo $id; ?> {
    justify-content: center;
  }
<?php } ?>

.node-<?php echo $id; ?> .slide, .node-<?php echo $id; ?> .slick-list {
  width: <?php echo $settings->width; ?>px;
}

<?php if ( '' !== $settings->width_medium ){ ?>
  @media ( max-width: <?php echo $global_settings->medium_breakpoint ?>px ) {
    .node-<?php echo $id; ?> .slide, .node-<?php echo $id; ?> .slick-list {
      width: <?php echo $settings->width_medium; ?>px;
    }
  }
<?php } ?>
<?php if ( '' !== $settings->width_responsive ){ ?>
  @media ( max-width: <?php echo $global_settings->responsive_breakpoint ?>px ) {
    .node-<?php echo $id; ?> .slide, .node-<?php echo $id; ?> .slick-list {
      width: <?php echo $settings->width_responsive; ?>px;
    }
  }
<?php } ?>

@media ( max-width: <?php echo $global_settings->responsive_breakpoint ?>px ) {
  .smg-slider {
    flex-wrap: wrap;
  }
  .smg-slider .slick-arrow {
    display: flex !important;
    height: 75px;
    width: 50%;
    align-items: center;
    justify-content: center;
    order: 2;
  }
}