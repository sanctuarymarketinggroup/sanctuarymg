<?php
// print_r($settings);
?>

<div class="smg-slider node-<?php echo $id; ?>">
  <?php
    $now = $settings->now;
    $source = $settings->source;
    if( $source === 'text' ){
      foreach( $settings->$source as $slide ){
        echo '<div>' . print_r($slide, true) . '</div>';
      }
    }
    if( $source === 'post_type' ){
      $args = array(
        'post_type'  => $settings->post_type,
        'showposts'  => $settings->count,
        // Here for Future Integration
        // 'meta_key'   => '_featured_post',
        // 'meta_value' => 1
      );
      $items = new WP_Query( $args );
      if ( $items->have_posts() ) {
        while ( $items->have_posts() ) { $items->the_post();
          echo '<div class="slide">';
            if($now == -1){
              echo get_the_content();
            } else {
              $cpt = get_post_type();
              if( $cpt == 'review'){
                $link = get_post_type_archive_link( $cpt )."#post-".get_the_id();
              } else {
                $link = get_permalink();
              }
              $more = "... <a href='".$link."'>Read&nbsp;More</a>";
              echo wp_trim_words(get_the_content(), $now, $more);
            }
            $title = get_field('title');
            $company = get_field('company');
            if( $title || $company ) {
              echo '<div class="reviews author">';
                echo '<span><strong>'.get_the_title().'</strong> &mdash; '.$title.'</span>';
                echo '<span>'.$company.'</span>';
              echo '</div>';
            }
          echo '</div>';
        }
        wp_reset_postdata();
      } else {
        _e('<p>Sorry, there aren\'t any ' . $settings->post_type . ' quite yet.</p>', 'smg');
      }
    }
  ?>
</div>