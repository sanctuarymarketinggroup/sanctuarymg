(function($){

  <?php
    $slick = array();
    $slick['autoplay'] = ($settings->autoplay === 'true');
    $slick = fl_dependency_field( $slick['autoplay'], $slick, 'speed', $settings->speed );
    $slick['arrows'] = ($settings->arrows === 'true');
    $slick = fl_dependency_field( 
      $slick['arrows'], 
      $slick, 
      'prevArrow', 
      '<div class="slick-arrow slick-prev">' . smg_get_image($settings->left_arrow, 'full', false, '', 'src') . '</div>'
    );
    $slick = fl_dependency_field(
      $slick['arrows'],
      $slick,
      'nextArrow',
      '<div class="slick-arrow slick-next">' . smg_get_image($settings->right_arrow, 'full', false, '', 'src') . '</div>'
    );
    $slick = json_encode( $slick );
  ?>

  $('.node-<?php echo $id; ?>').slick(<?php echo $slick; ?>);

})(jQuery);