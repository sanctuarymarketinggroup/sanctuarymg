<?php

/**
 * This file should be used to render each module instance.
 * You have access to two variables in this file:
 *
 * $module An instance of your module class.
 * $settings The module's settings.
 *
 * Example:
 */

?>
<div class="full-funnel-module">
  <div class="funnel-title">
    <div class="funnel-photo">
      <?php
$image = wp_get_attachment_image_src($settings->photo, 'full');
if ($image && substr($image[0], -3) !== 'svg') {
    echo '<img src="' . $image[0] . '" alt="Interactive Full Funnel Image" width="' . $image[1] . '" height="' . $image[2] . '" />';
}
if ($image && substr($image[0], -3) === 'svg') {
    echo get_svg($image[0]);
}
?>
       <style>
        .full-funnel-module .funnel-photo svg #awareness:hover > polygon {
          fill: url(#top-hover);
        }

        .full-funnel-module .funnel-photo svg #advocacy:hover > polygon {
          fill: url(#bottom-hover);
        }

       </style>
    </div>
  </div>
  <?php
$steps = array(
    'awareness' => array(
        'title' => $settings->awareness_title,
        'content' => $settings->awareness_content,
    ),
    'consideration' => array(
        'title' => $settings->consideration_title,
        'content' => $settings->consideration_content,
    ),
    'intent' => array(
        'title' => $settings->intent_title,
        'content' => $settings->intent_content,
    ),
    'purchase' => array(
        'title' => $settings->purchase_title,
        'content' => $settings->purchase_content,
    ),
    'retention' => array(
        'title' => $settings->retention_title,
        'content' => $settings->retention_content,
    ),
    'loyalty' => array(
        'title' => $settings->loyalty_title,
        'content' => $settings->loyalty_content,
    ),
    'advocacy' => array(
        'title' => $settings->advocacy_title,
        'content' => $settings->advocacy_content,
    ),
);
foreach ($steps as $key => $value) {
    ?>
    <div class="<?php echo $key; ?>-popup interactive-popup">
      <div class="inter-pop-inner">
        <h3><?php echo $value['title']; ?></h3>
        <div class="inter-pop-content">
          <?php echo $value['content']; ?>
        </div>
      </div>
      <div class="pop-close">close</div>
    </div>
    <?php
}
?>
  <div id="full-funnel-learn-more" class="button outline">
    Learn More
  </div>
</div>