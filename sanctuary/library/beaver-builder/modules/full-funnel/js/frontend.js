(function($) {
  let $funnel = $(".full-funnel-module");
  let selection;

  function init_popup(event) {
    selection = $(this).attr("id");
    $funnel.find("." + selection + "-popup").addClass("open");
  }

  function close_popup(event) {
    $(this).removeClass("open");
  }

  function mobile_learn_more(event) {
    if ($funnel.find(".interactive-popup").hasClass("open-mobile")) {
      $funnel.find(".interactive-popup").removeClass("open-mobile");
      $funnel.find("#full-funnel-learn-more").text("Learn More");
    } else {
      $funnel.find(".interactive-popup").addClass("open-mobile");
      $funnel.find("#full-funnel-learn-more").text("Close");
    }
  }
  if ($(window).width() > 767) {
    $funnel.find(".funnel-photo svg g").on("click", init_popup);
  }
  $funnel.find(".interactive-popup").on("click", close_popup);
  $funnel.find(".pop-close", close_popup);
  $funnel.find("#full-funnel-learn-more").on("click", mobile_learn_more);
})(jQuery);
