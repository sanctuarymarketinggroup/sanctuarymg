<?php

/**
 * This is an example module with only the basic
 * setup necessary to get it working.
 *
 * @class FLLocationModule
 */
class FLFullFunnelModule extends FLBuilderModule {

    /** 
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */  
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Full Funnel', 'fl-builder'),
            'description'   => __('A basic module for adding Full Funnel Interactive Graphic.', 'fl-builder'),
            'category'		=> __('Custom', 'fl-builder'),
            'dir'           => SMG_MODULE_DIR . 'modules/full-funnel/',
            'url'           => SMG_MODULE_URL . 'modules/full-funnel/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));
    }
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('FLFullFunnelModule', array(
    'general'       => array( // Tab
        'title'         => __('General', 'fl-builder'), // Tab title
        'sections'      => array( // Tab Sections
            'general-photo'       => array( // Section
                'photo'         => __('Funnel Photo', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'photo' => array(
                        'type'          => 'photo',
                        'label'         => __('Funnel Photo', 'fl-builder'),
                        'show_remove'   => false,
                        'preview'         => array(
                            'type'             => 'text',
                            'selector'         => '.funnel-title .funnel-photo'  
                        )
                    ),
                )
            )
        )
    ),
    'awareness'       => array( // Tab
        'title'         => __('Awareness', 'fl-builder'), // Tab title
        'sections'      => array( // Tab Sections
            'awareness'       => array( // Section
                'title'         => __('Awareness Content', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'awareness_title' => array(
                        'type'          => 'text',
                        'label'         => __('Awareness Title', 'fl-builder'),
                        'preview'         => array(
                            'type'             => 'text',
                            'selector'         => '.awareness-popup .h3'  
                        )
                    ),
                    'awareness_content' => array(
                        'type'          => 'editor',
                        'label'         => __('Awareness Content', 'fl-builder'),
                        'media_buttons' => false,
                        'wpautop'       => true,
                        'preview'         => array(
                            'type'             => 'text',
                            'selector'         => '.awareness-popup .inter-pop-content'  
                        )
                    ),
                )
            )
        )
    ),
    'consideration'       => array( // Tab
        'title'         => __('Consideration', 'fl-builder'), // Tab title
        'sections'      => array( // Tab Sections
            'consideration'       => array( // Section
                'title'         => __('Consideration Content', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'consideration_title' => array(
                        'type'          => 'text',
                        'label'         => __('Consideration Title', 'fl-builder'),
                        'preview'         => array(
                            'type'             => 'text',
                            'selector'         => '.consideration-popup h3'  
                        )
                    ),
                    'consideration_content' => array(
                        'type'          => 'editor',
                        'label'         => __('Consideration Content', 'fl-builder'),
                        'media_buttons' => false,
                        'wpautop'       => true,
                        'preview'         => array(
                            'type'             => 'text',
                            'selector'         => '.consideration-popup .inter-pop-content'  
                        )
                    ),
                )
            )
        )
    ),
    'intent'       => array( // Tab
        'title'         => __('Intent', 'fl-builder'), // Tab title
        'sections'      => array( // Tab Sections
            'intent'       => array( // Section
                'title'         => __('Intent Content', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'intent_title' => array(
                        'type'          => 'text',
                        'label'         => __('Intent Title', 'fl-builder'),
                        'preview'         => array(
                            'type'             => 'text',
                            'selector'         => '.intent-popup h3'  
                        )
                    ),
                    'intent_content' => array(
                        'type'          => 'editor',
                        'label'         => __('Intent Content', 'fl-builder'),
                        'media_buttons' => false,
                        'wpautop'       => true,
                        'preview'         => array(
                            'type'             => 'text',
                            'selector'         => '.intent-popup .inter-pop-content'  
                        )
                    ),
                )
            )
        )
    ),
    'purchase'       => array( // Tab
        'title'         => __('Purchase', 'fl-builder'), // Tab title
        'sections'      => array( // Tab Sections
            'purchase'       => array( // Section
                'title'         => __('Purchase Content', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'purchase_title' => array(
                        'type'          => 'text',
                        'label'         => __('Purchase Title', 'fl-builder'),
                        'preview'         => array(
                            'type'             => 'text',
                            'selector'         => '.purchase-popup h3'  
                        )
                    ),
                    'purchase_content' => array(
                        'type'          => 'editor',
                        'label'         => __('Purchase Content', 'fl-builder'),
                        'media_buttons' => false,
                        'wpautop'       => true,
                        'preview'         => array(
                            'type'             => 'text',
                            'selector'         => '.purchase-popup .inter-pop-content'  
                        )
                    ),
                )
            )
        )
    ),
    'retention'       => array( // Tab
        'title'         => __('Retention', 'fl-builder'), // Tab title
        'sections'      => array( // Tab Sections
            'retention'       => array( // Section
                'title'         => __('Retention Content', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'retention_title' => array(
                        'type'          => 'text',
                        'label'         => __('Retention Title', 'fl-builder'),
                        'preview'         => array(
                            'type'             => 'text',
                            'selector'         => '.retention-popup h3'  
                        )
                    ),
                    'retention_content' => array(
                        'type'          => 'editor',
                        'label'         => __('Retention Content', 'fl-builder'),
                        'media_buttons' => false,
                        'wpautop'       => true,
                        'preview'         => array(
                            'type'             => 'text',
                            'selector'         => '.retention-popup .inter-pop-content'  
                        )
                    ),
                )
            )
        )
    ),
    'loyalty'       => array( // Tab
        'title'         => __('Loyalty', 'fl-builder'), // Tab title
        'sections'      => array( // Tab Sections
            'loyalty'       => array( // Section
                'title'         => __('Loyalty Content', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'loyalty_title' => array(
                        'type'          => 'text',
                        'label'         => __('Loyalty Title', 'fl-builder'),
                        'preview'         => array(
                            'type'             => 'text',
                            'selector'         => '.loyalty-popup .h3'  
                        )
                    ),
                    'loyalty_content' => array(
                        'type'          => 'editor',
                        'label'         => __('Loyalty Content', 'fl-builder'),
                        'media_buttons' => false,
                        'wpautop'       => true,
                        'preview'         => array(
                            'type'             => 'text',
                            'selector'         => '.loyalty-popup .inter-pop-content'  
                        )
                    ),
                )
            )
        )
    ),
    'advocacy'       => array( // Tab
        'title'         => __('Advocacy', 'fl-builder'), // Tab title
        'sections'      => array( // Tab Sections
            'advocacy'       => array( // Section
                'title'         => __('Advocacy Content', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'advocacy_title' => array(
                        'type'          => 'text',
                        'label'         => __('Advocacy Title', 'fl-builder'),
                        'preview'         => array(
                            'type'             => 'text',
                            'selector'         => '.advocacy-popup h3'  
                        )
                    ),
                    'advocacy_content' => array(
                        'type'          => 'editor',
                        'label'         => __('Advocacy Content', 'fl-builder'),
                        'media_buttons' => false,
                        'wpautop'       => true,
                        'preview'         => array(
                            'type'             => 'text',
                            'selector'         => '.advocacy-popup .inter-pop-content'  
                        )
                    ),
                )
            )
        )
    ),
));