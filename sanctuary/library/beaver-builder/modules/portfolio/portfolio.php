<?php

/**
 * This is an example module with only the basic
 * setup necessary to get it working.
 *
 * @class SmgPortfolio
 */
class SmgPortfolio extends FLBuilderModule {

    /** 
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */  
    public function __construct()
    {
      parent::__construct(array(
          'name'          => __('Portfolio', 'fl-builder'),
          'description'   => __('A Portfolio Widget', 'fl-builder'),
          'category'		=> __('Custom', 'fl-builder'),
          'dir'           => SMG_MODULE_DIR . 'modules/portfolio/',
          'url'           => SMG_MODULE_URL . 'modules/portfolio/',
          'editor_export' => true, // Defaults to true and can be omitted.
          'enabled'       => true, // Defaults to true and can be omitted.
      ));
    }
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('SmgPortfolio', array(
  'general' => array( // Tab
    'title' => __('General', 'fl-builder'), // Tab title
    'sections' => array( // Tab Sections
      'general' => array( // Section
        'title' => __('Settings', 'fl-builder'), // Section Title
        'fields' => array( // Section Fields
          'svg' => array(
            'type'          => 'photo',
            'label'         => __('SVG', 'fl-builder'),
            'show_remove'   => true,
          ),
        )
      )
    )
  )
));