<?php

/**
 * This is an example module with only the basic
 * setup necessary to get it working.
 *
 * @class SMGSvgModule
 */
class SMGSvgModule extends FLBuilderModule {

    /** 
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */  
    public function __construct()
    {
      parent::__construct(array(
          'name'          => __('SVG', 'fl-builder'),
          'description'   => __('A basic module for adding svg\'s to the page the right way.', 'fl-builder'),
          'category'		=> __('Custom', 'fl-builder'),
          'dir'           => SMG_MODULE_DIR . 'modules/svg/',
          'url'           => SMG_MODULE_URL . 'modules/svg/',
          'editor_export' => true, // Defaults to true and can be omitted.
          'enabled'       => true, // Defaults to true and can be omitted.
      ));
    }
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('SMGSvgModule', array(
  'general' => array( // Tab
    'title' => __('General', 'fl-builder'), // Tab title
    'sections' => array( // Tab Sections
      'general' => array( // Section
        'title' => __('Settings', 'fl-builder'), // Section Title
        'fields' => array( // Section Fields
          'svg' => array(
            'type'          => 'photo',
            'label'         => __('SVG', 'fl-builder'),
            'show_remove'   => true,
          ),
          'link' => array(
            'type'          => 'link',
            'label'         => __('Link', 'fl-builder')
          ),
          'height' => array(
            'type'        => 'unit',
            'label'       => 'Height',
            'units'	      => array( 'px', 'vw', '%' ),
            'default_unit' => 'px', // Optional
            'responsive'  => true
          ),
          'width' => array(
            'type'        => 'unit',
            'label'       => 'Width',
            'units'	      => array( 'px', 'vw', '%' ),
            'default_unit' => 'px', // Optional
            'responsive'  => true
          ),
          'text_align' => array(
            'type'          => 'select',
            'label'         => __( 'Select Field', 'fl-builder' ),
            'default'       => 'start',
            'options'       => array(
                'flex-start'      => __( 'Left', 'fl-builder' ),
                'center'      => __( 'Center', 'fl-builder' ),
                'flex-end'      => __( 'Right', 'fl-builder' )
            ),
          ),
        )
      )
    )
  )
));