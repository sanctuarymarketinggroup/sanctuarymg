<?php

?>

.node-<?php echo $id; ?> {
  justify-content: <?php echo $settings->text_align; ?>;
}

<?php

FLBuilderCSS::responsive_rule( array(
	'settings'	=> $settings,
	'setting_name'	=> 'height',
	'selector'	=> ".node-$id svg",
	'prop'		=> 'height',
) );

FLBuilderCSS::responsive_rule( array(
	'settings'	=> $settings,
	'setting_name'	=> 'width',
	'selector'	=> ".node-$id svg",
	'prop'		=> 'width',
) );

?>