<?php

/**
 * This file should be used to render each module instance.
 * You have access to two variables in this file: 
 * 
 * $module An instance of your module class.
 * $settings The module's settings.
 *
 * Example: 
 */

?>

<div class="fl-svg node-<?php echo $id; ?>">
  <?php if( ! empty( $settings->link ) ){ echo '<a href="' . $settings->link . '">'; } ?>
    <?php $image = wp_get_attachment_image_src( $settings->svg, 'full', false ); ?>
    <img src="<?php echo $image[0]; ?>"/>
  <?php if( ! empty( $settings->link ) ){ echo '</a>'; } ?>
</div>