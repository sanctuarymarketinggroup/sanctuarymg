<div class="smg-portfolio-item" >
<?php 
$image = wp_get_attachment_image_src($settings->portfolioPhoto, 'large');
echo show_template(
  'content-block',
  array(
    'image' => $image[0],
    'link' => $settings->portfolioLink,
    'before' => $settings->portfolioBefore,
    'after' => $settings->portfolioAfter
  ),
  './'
);
?>
</div>