<?php

/**
 * This is an example module with only the basic
 * setup necessary to get it working.
 *
 * @class FLLocationModule
 */
class SmgPortfolioItem extends FLBuilderModule {

    /** 
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */  
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Portfolio Item', 'fl-builder'),
            'description'   => __('Portfolio Item', 'fl-builder'),
            'category'		=> __('Custom', 'fl-builder'),
            'dir'           => SMG_MODULE_DIR . 'modules/portfolio-item/',
            'url'           => SMG_MODULE_URL . 'modules/portfolio-item/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));
    }
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('SmgPortfolioItem', array(
  'general'       => array( // Tab
    'title'         => __('General', 'fl-builder'), // Tab title
    'sections'      => array( // Tab Sections
      'general'       => array( // Section
      'label'         => __('Settings', 'fl-builder'), // Section Title
        'fields'        => array( // Section Fields
          'portfolioPhoto' => array(
            'type'          => 'photo',
            'label'         => __('Background Photo', 'fl-builder'), 
            'show_remove'   => true,
          ),
          'portfolioLink' => array(
            'type'          => 'link',
            'label'         => __('Portfolio Item Link', 'fl-builder')
          ),
          'portfolioBefore' => array(
            'label'         => __('Before Hover Text', 'fl-builder'),
            'type'          => 'text',
          ),
          'portfolioAfter' => array(
            'label'         => __('After Hover Text', 'fl-builder'),
            'type'          => 'editor',
            'media_buttons' => false,
            'wpautop'       => true
          ),
          
        )
      ),
    )
  )
));