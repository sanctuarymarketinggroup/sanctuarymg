<?php

/**
 * This file should be used to render each module instance.
 * You have access to two variables in this file: 
 * 
 * $module An instance of your module class.
 * $settings The module's settings.
 *
 * Example: 
 */

?>
<div class="chli node-<?php echo $id; ?>">
  <?php
  $label_location = $settings->label_location;
  $nofollow = '';
  if($settings->link_nofollow == 'yes') {
    $nofollow = 'rel="nofollow"';
  }
  ?>
  <a class="chli-link <?php echo $label_location; ?>" href="<?php echo $settings->link; ?>" <?php echo $nofollow; ?> target="<?php echo $settings->link_target; ?>">
    <div class="img">
      <?php echo wp_get_attachment_image($settings->icon, 'full'); ?>
    </div>
    <?php if($label_location == 'label-block') { ?>
      <div class="label">
        <?php echo $settings->label; ?>
      </div>
    <?php } ?>
  </a>
  <?php if($label_location == 'label-outside') { ?>
    <div class="label label-external">
      <a href="<?php echo $settings->link; ?>" <?php echo $nofollow; ?> target="<?php echo $settings->link_target; ?>">
        <?php echo $settings->label; ?> 
      </a>
    </div>
  <?php } ?>
</div>