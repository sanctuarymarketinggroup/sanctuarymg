<?php

/**
 * This is an example module with only the basic
 * setup necessary to get it working.
 *
 * @class SMGColumnHoverLinkImg
 */
class SMGColumnHoverLinkImg extends FLBuilderModule {

    /** 
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */  
    public function __construct()
    {
      parent::__construct(array(
          'name'          => __('Column Link with Image', 'fl-builder'),
          'description'   => __('Add up to 5 HubSpot Certifications', 'fl-builder'),
          'category'		=> __('Custom', 'fl-builder'),
          'dir'           => SMG_MODULE_DIR . 'modules/column-hover-link-img/',
          'url'           => SMG_MODULE_URL . 'modules/column-hover-link-img/',
          'editor_export' => true, // Defaults to true and can be omitted.
          'enabled'       => true, // Defaults to true and can be omitted.
      ));
    }
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('SMGColumnHoverLinkImg', array(
  'general' => array( // Tab
    'title' => __('General', 'fl-builder'), // Tab title
    'sections' => array( // Tab Sections
      'general' => array( // Section
        'title' => __('Settings', 'fl-builder'), // Section Title
        'fields' => array( // Section Fields
          'label_location' => array(
            'type'          => 'select',
            'label'         => __( 'Label within block?', 'fl-builder' ),
            'default'       => 'label-block',
            'options'       => array(
              'label-block'      => __( 'Yes, Label with Block', 'fl-builder' ),
              'label-outside'      => __( 'No, Label outside Block', 'fl-builder' )
            ),
          ),
          'bg_color' => array(
						'type'         => 'color',
						'label'        => __( 'BG Color', 'fl-builder' ),
            'default'       => '524D43',
            'show_reset'    => true,
            'show_alpha'    => true
          ),
          'bg_color_hover' => array(
						'type'         => 'color',
						'label'        => __( 'BG Hover Color', 'fl-builder' ),
            'default'       => '787160',
            'show_reset'    => true,
            'show_alpha'    => true
          ),
          'icon' => array(
            'type'          => 'photo',
            'label'         => __('Icon', 'fl-builder'),
            'show_remove'   => false,
          ),
          'label' => array(
            'type'          => 'text',
            'label'         => __('Label', 'fl-builder'),
          ),
          'link' => array(
            'type'          => 'link',
            'label'         => __('Link', 'fl-builder'),
            'show_target'   => true,
            'show_nofollow' => true,
          ),
        )
      )
    )
  )
));

