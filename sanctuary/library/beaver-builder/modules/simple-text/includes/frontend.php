<?php

/**
 * This file should be used to render each module instance.
 * You have access to two variables in this file: 
 * 
 * $module An instance of your module class.
 * $settings The module's settings.
 *
 * Example: 
 */

?>
<?php // print_r( $module ); ?>
<?php // print_r( $settings ); ?>

<div class="fl-text preset-<?php echo $settings->preset; ?> node-<?php echo $id; ?>">
  <<?php echo $settings->html; ?>><?php echo $settings->text; ?></<?php echo $settings->html; ?>>
</div>