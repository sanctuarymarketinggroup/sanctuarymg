
<?php // Align ?>
.fl-text.node-<?php echo $id; ?> {
  <?php 
  switch ( $settings->text_align ) {
    case 'flex-start':
      $align = 'left';
      break;
    case 'center':
      $align = 'center';
      break;
    case 'flex-end':
      $align = 'right';
      break;
  }
  ?>
  text-align: <?php echo $align; ?>;
  justify-content: <?php echo $settings->text_align; ?>;
}

<?php // Color ?>
.fl-text.node-<?php echo $id; ?> <?php echo $settings->html; ?> {
  color: #<?php echo $settings->text_color; ?>;
}

<?php // Style ?>
<?php if ( 'true' === $settings->custom ) { ?>
  .fl-text.node-<?php echo $id; ?> <?php echo $settings->html; ?> {
    <?php echo smg_get_style_field( $settings->text_size, '', 'font-size', 'px' ); ?>
    <?php echo smg_get_style_field( $settings->text_weight, '', 'font-weight', '' ); ?>
    <?php echo smg_get_style_field( $settings->text_spacing, '', 'letter-spacing', 'px' ); ?>
    <?php echo smg_get_style_field( $settings->line_height, '', 'line-height', 'px' ); ?>
    <?php echo smg_get_style_field( $settings->text_transform, '', 'text-transform', '' ); ?>
  }
  <?php if ( '' !== $settings->text_size_medium || '' !== $settings->line_height_medium ){ ?>
    @media ( max-width: <?php echo $global_settings->medium_breakpoint ?>px ) {
      .fl-text.node-<?php echo $id; ?> <?php echo $settings->html; ?> {
        <?php echo smg_get_style_field( $settings->text_size_medium, '', 'font-size', 'px' ); ?>
        <?php echo smg_get_style_field( $settings->line_height_medium, '', 'line-height', 'px' ); ?>
      }
    }
  <?php } ?>
  <?php if ( '' !== $settings->text_size_responsive || '' !== $settings->line_height_responsive ){ ?>
    @media ( max-width: <?php echo $global_settings->responsive_breakpoint ?>px ) {
      .fl-text.node-<?php echo $id; ?> <?php echo $settings->html; ?> {
        <?php echo smg_get_style_field( $settings->text_size_responsive, '', 'font-size', 'px' ); ?>
        <?php echo smg_get_style_field( $settings->line_height_responsive, '', 'line-height', 'px' ); ?>
      }
    }
  <?php } ?>
<?php } ?>

