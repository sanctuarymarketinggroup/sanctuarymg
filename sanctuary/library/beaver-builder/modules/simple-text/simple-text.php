<?php

/**
 * This is an example module with only the basic
 * setup necessary to get it working.
 *
 * @class SMGSvgModule
 */
class SMGSimpleTextModule extends FLBuilderModule {

    /** 
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */  
    public function __construct()
    {
      parent::__construct(array(
          'name'          => __('Simple Text', 'fl-builder'),
          'description'   => __('A basic module for adding simple text blocks to a page.', 'fl-builder'),
          'category'		=> __('Custom', 'fl-builder'),
          'dir'           => SMG_MODULE_DIR . 'modules/simple-text/',
          'url'           => SMG_MODULE_URL . 'modules/simple-text/',
          'editor_export' => true, // Defaults to true and can be omitted.
          'enabled'       => true, // Defaults to true and can be omitted.
      ));
    }
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('SMGSimpleTextModule', array(
  'general' => array( // Tab
    'title' => __('General', 'fl-builder'), // Tab title
    'sections' => array( // Tab Sections
      'general' => array( // Section
        'title' => __('General', 'fl-builder'), // Section Title
        'fields' => array( // Section Fields
          'preset' => array(
            'type' => 'select',
            'label' => __('Preset', 'fl-builder'),
            'default' => 'default',
            'options' => array(
              'default' => __('Default', 'fl-builder'),
              '1' => __('Preset 1', 'fl-builder'),
              '2' => __('Preset 2', 'fl-builder'),
              '3' => __('Preset 3', 'fl-builder'),
              '4' => __('Preset 4', 'fl-builder'),
              '5' => __('Preset 5', 'fl-builder'),
            )
          ),
          'text' => array(
            'type'          => 'text',
            'label'         => __('Text', 'fl-builder')
          ),
        ),
      ),
      'style' => array( // Section
        'title' => __('Style', 'fl-builder'), // Section Title
        'fields' => array( // Section Fields
          'text_align' => array(
            'type'          => 'select',
            'label'         => __( 'Align', 'fl-builder' ),
            'default'       => 'start',
            'options'       => array(
              'flex-start'      => __( 'Left', 'fl-builder' ),
              'center'      => __( 'Center', 'fl-builder' ),
              'flex-end'      => __( 'Right', 'fl-builder' )
            ),
          ),
          'text_color' => array(
            'type'          => 'color',
            'label'         => __( 'Color', 'fl-builder' ),
            'default'       => '#fff',
            'show_reset'    => true,
            'show_alpha'    => true,
          ),
          'custom' => array(
            'type' => 'select',
            'label' => __('Customize', 'fl-builder'),
            'options' => array(
              'false' => __('Default', 'fl-builder'),
              'true' => __('Custom', 'fl-builder')
            ),
            'toggle' => array(
              'true' => array(
                'fields' => array(
                  'text_size',
                  'line_height',
                  'text_spacing',
                  'text_weight',
                  'text_transform'
                )
              )
            )
          ),
          'text_size' => array(
            'type'          => 'unit',
            'label'         => __( 'Font Size', 'fl-builder' ),
            'description' => 'px',
            'placeholder' => '18',
            'responsive'  => true,
          ),
          'line_height' => array(
            'type'          => 'unit',
            'label'         => __( 'Line Height', 'fl-builder' ),
            'description' => 'px',
            'placeholder' => '18',
            'responsive'  => true,
          ),
          'text_spacing' => array(
            'type'          => 'unit',
            'label'         => __( 'Letter Spacing', 'fl-builder' ),
            'description' => 'px',
            'placeholder' => '0'
          ),
          'text_weight' => array(
            'type'          => 'select',
            'label'         => __( 'Font Weight', 'fl-builder' ),
            'default'       => '400',
            'options'       => array(
              '100' => __('Thin', 'fl-builder'),
              '200' => __('Extra Light', 'fl-builder'),
              '300' => __('Light', 'fl-builder'),
              '400' => __('Normal', 'fl-builder'),
              '500' => __('Medium', 'fl-builder'),
              '600' => __('Semi Bold', 'fl-builder'),
              '700' => __('Bold', 'fl-builder'),
              '800' => __('Extra Bold', 'fl-builder'),
              '900' => __('Black', 'fl-builder')
            )
          ),
          'text_transform' => array(
            'type'          => 'select',
            'label'         => __( 'Text Transform', 'fl-builder' ),
            'default'       => 'inherit',
            'options'       => array(
              'inherit'      => __( 'Default', 'fl-builder' ),
              'capitalize'      => __( 'Capitalize', 'fl-builder' ),
              'uppercase'      => __( 'Uppercase', 'fl-builder' ),
              'lowercase'      => __( 'Lowercase', 'fl-builder' )
            ),
          )
        )
      ),
      'advanced' => array( // Section
        'title' => __('Advanced', 'fl-builder'), // Section Title
        'fields' => array( // Section Fields
          'html' => array(
            'type' => 'text',
            'label' => __('HTML', 'fl-builder'),
            'default' => 'p'
          )
        ),
      ),
    )
  )
));