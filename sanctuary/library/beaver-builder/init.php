<?php

define( 'SMG_MODULE_DIR', get_template_directory() . '/library/beaver-builder' );
define( 'SMG_MODULE_URL', get_stylesheet_directory_uri() . 'library/beaver-builder' );

/**
 * A class that handles loading custom modules and custom
 * fields if the builder is installed and activated.
 */
class SMG_Custom_Modules_Loader {
	
	/**
	 * Initializes the class once all plugins have loaded.
	 */
	static public function init() {
    self::setup_hooks();
	}
	
	/**
	 * Setup hooks if the builder is installed and activated.
	 */
	static public function setup_hooks() {
		if ( ! class_exists( 'FLBuilder' ) ) {
			return;
		}
		
		// Load custom modules.
		add_action( 'init', __CLASS__ . '::load_modules' );
		
		// Register custom fields.
		// add_filter( 'fl_builder_custom_fields', __CLASS__ . '::register_fields' );
		
		// Enqueue custom field assets.
		add_action( 'init', __CLASS__ . '::enqueue_field_assets' );
	}
	
	/**
	 * Loads our custom modules.
	 */
	static public function load_modules() {
		require_once 'modules/case-studies/case-studies.php';
		require_once 'modules/svg/svg.php';
		require_once 'modules/full-funnel/full-funnel.php';
		require_once 'modules/slick-slider/slick-slider.php';
		require_once 'modules/smg-portfolio/smg-portfolio.php';
		require_once 'modules/portfolio-item/portfolio-item.php';
		require_once 'modules/simple-text/simple-text.php';
		require_once 'modules/smg-posts/smg-posts.php';
		require_once 'modules/hubspot-cert/hubspot-cert.php';
		require_once 'modules/column-hover-link-img/column-hover-link-img.php';
	}
	
	/**
	 * Registers our custom fields.
	 */
	// static public function register_fields( $fields ) {
	// 	$fields['smg-authors'] = SMG_MODULE_DIR . '/fields/authors.php';
	// 	return $fields;
	// }

	static public function register_post_types_field() {
		$args = array(
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true
		);
		$posts = get_post_types( $args, 'names', 'and' );
		unset($posts['attachment']);
		return $posts;
	}
	
	/**
	 * Enqueues our custom field assets only if the builder UI is active.
	 */
	static public function enqueue_field_assets() {
		if ( ! FLBuilderModel::is_builder_active() ) {
			return;
		}
		wp_enqueue_style( 'my-custom-fields', SMG_MODULE_URL . 'assets/css/fields.css', array(), '' );
		wp_enqueue_script( 'my-custom-fields', SMG_MODULE_URL . 'assets/js/fields.js', array(), '', true );
	}
}

SMG_Custom_Modules_Loader::init();