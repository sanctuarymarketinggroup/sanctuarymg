<?php
/**
 * Register Menus
 *
 * Learn more {@link http://codex.wordpress.org/Function_Reference/register_nav_menus#Examples}
 * 
 * @package WordPress
 * @subpackage SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

register_nav_menus(array(
	'topbar-nav' => 'Topbar Navigation',
	'main-nav' => 'Main Navigation',
	'footer-nav' => 'Footer Navigation',
));

/**
 * Main Navigation
 * http://codex.wordpress.org/Function_Reference/wp_nav_menu
 */
if ( ! function_exists( 'smg_menu' ) ) {
	function smg_menu() {
		wp_nav_menu(array(
			'container'      => false,                     // Remove nav container
			'menu_class'     => 'dropdown menu',           // Adding custom nav class
			'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			'theme_location' => 'main-nav',                // Where it's located in the theme
			'depth'          => 3,                         // Limit the depth of the nav
			'fallback_cb'    => false,                     // Fallback function (see below)
			'walker'         => new SMG_Top_Bar_Walker(),
		));
	}
}

if ( ! function_exists( 'smg_footer_menu' ) ) {
	function smg_footer_menu() {
		wp_nav_menu(array(
			'container'      => false,                     // Remove nav container
			'menu_class'     => 'dropdown menu',           // Adding custom nav class
			'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			'theme_location' => 'footer-nav',                // Where it's located in the theme
			'depth'          => 3,                         // Limit the depth of the nav
			'fallback_cb'    => false,                     // Fallback function (see below)
			'walker'         => new SMG_Top_Bar_Walker(),
		));
	}
}