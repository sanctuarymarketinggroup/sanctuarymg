<?php
/**
 * Website Specific Functions
 * 
 * @package WordPress
 * @subpackage SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

function smg_get_image( $id, $size = 'full', $icon = false, $args = '', $return = 'full' ) {
  $image = wp_get_attachment_image_src( $id, $size, $icon );
  if( strpos( $image[0], '.svg' ) ) {
    return get_svg( $image[0] );
  }
  if( 'src' === $return ) {
    return $image[0];
  }
  return wp_get_attachment_image( $id, $size, $icon, $args );
}

function get_the_post_logo( $post_id, $size, $icon = false, $args = '' ) {
  $id = get_post_meta( $post_id, '_featured_logo', true );
  return smg_get_image( $id, $size, $icon, $args );
}

function fl_dependency_field( $dependency, $array, $field, $value ) {
  if( 'true' == $dependency ) {
    $array[$field] = $value;
  }
  return $array;
}

function smg_get_style_field( $value, $null, $field, $extra = '' ){
  return ($null !== $value) ? $field . ':' . $value . $extra . ';' : '';
}

add_filter( 'get_the_archive_title', 'remove_pretext_from_archives');
function remove_pretext_from_archives($title) {
  if ( is_category() ) {
    $title = single_cat_title( '', false );
  } elseif ( is_tag() ) {
    $title = single_tag_title( '', false );
  } elseif ( is_author() ) {
    $title = '<span class="vcard">' . get_the_author() . '</span>' ;
  } elseif ( is_post_type_archive() ) {
    $title = post_type_archive_title( '', false );
  }
  return $title;
}
