<?php
/**
 * Shortcode Available
 * 
 * @package WordPress
 * @subpackage SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */
function view_portfolio_homepage(){
  $args = array(
    'post_type' => 'portfolio',
    'showposts' => 10
  );
  // The Query
  $the_query = new WP_Query( $args );

  $output='';
  // The Loop
  if ( $the_query->have_posts() ) { 
    $output .= '<div id="front-portfolio-slider" data-slick=\'{"slidesToShow": 1, "slidesToScroll": 1, "infinite": true, "cssEase": linear }]\'>';
    while ( $the_query->have_posts() ) {
      $the_query->the_post();
      $output .= '<div class="portfolio-item">';
        $output .= '<div class="portfolio-inner">';
        $output .= get_the_post_thumbnail( get_the_id(), 'full' );
        $mobile_id = get_post_meta( get_the_id(), '_mobile_image', true );
        $mobile_image = wp_get_attachment_image_src( $mobile_id, 'full' );
        if( isset ( $mobile_image ) ) {
          $output .= '<div class="mobile-phone">';
            $output .= '<div class="mobile-inner">';
              $output .= '<img src="' . $mobile_image[0] . '" alt="Portfolio Image" width="' . $mobile_image[1] . '" height="' . $mobile_image[2] . '" />';
            $output .= '</div>'; 
          $output .= '</div>';
        }
        $output .= '</div>';
      $output .= '</div>';
    }
    $output .= '</ul>';
    /* Restore original Post Data */
    wp_reset_postdata();
  } else {
    // no posts found
  }
  return $output;
}

add_shortcode( 'smg_portfolio', 'view_portfolio_homepage' );


function smg_recent_works_code($atts){
  	// Attributes
	$atts = shortcode_atts(
		array(
      'limit' => '3',
      'cat' => ''
		),
		$atts,
		'recent_work'
	);
  // Query
  $args = array ( 
    'post_type' => 'portfolio',
    'showposts' => $atts['limit'] 
  ); 
  if($atts['cat'] != '') {
    $cats = explode(',',$atts['cat']);
    $tax_query = array(
      array(
        'taxonomy' => 'portfolio_cat',
        'field'    => 'term_id',
        'terms'    => $cats,
      ),
    );
    $args['tax_query'] = $tax_query;
  }
	$the_query = new WP_Query( $args );
	
  // Posts
  ob_start();
  echo '<div id="port-short" class="archive-preview-blocks">';
	while ( $the_query->have_posts() ) {
    $the_query->the_post();
	  show_template(
      'content-block',
      array(
        'terms' => array(
          'portfolio_cat' => get_the_terms(get_the_ID(), 'portfolio_cat')
        ),
        'image' => get_field('portfolio_archive_square_image', get_the_ID())['url'], // get_the_post_thumbnail_url( get_the_id() ),
        'link' => get_permalink(get_the_ID()),
        'before' => '',
        'after' => '<h2>' . get_the_title() . '</h2>'
      ),
      './'
    );
  }
	echo '</div>';
	// Reset post data
	wp_reset_postdata();
	
	// Return code
	return ob_get_clean();
}
add_shortcode('recent_work', 'smg_recent_works_code');



function smg_client_reviews_code($atts){
  // Attributes
$atts = shortcode_atts(
  array(
    'limit' => '3',
    'ids' => ''
  ),
  $atts,
  'client_reviews'
);
// Query
$args = array ( 
  'post_type' => 'review',
  'showposts' => $atts['limit'] 
); 
if($atts['ids'] != '') {
  $ids = explode(',',$atts['ids']);
  $args['post__in'] = $ids;
  $args['orderby'] = 'post__in'; 
}
$the_query = new WP_Query( $args );

// Posts
ob_start();
echo '<div id="client-reviews-short" class="">';
while ( $the_query->have_posts() ) {
  $the_query->the_post();
  show_template(
    'content-review',
    array(),
    './'
  );
}
echo '</div>';
// Reset post data
wp_reset_postdata();

// Return code
return ob_get_clean();
}
add_shortcode('client_reviews', 'smg_client_reviews_code');



function roi_shortcode_code(){
	$output ='';
	$clv = $_GET['clv'];
	$clv = str_replace('$', '', $clv);
	
	$values = "<div class='roi-values-cont'>";
	$values .= "<h4>Customer Acquisition Cost:</h4> ".$_GET['cac']." </br>";
	$values .= "<h4>Customer Lifetime Revenue:</h4> ".$_GET['clr']." </br>";
	$values .= "<h4>Customer Lifetime Value:</h4> ".$_GET['clv'];
	$values .= "</div>";

	if($clv < 0){
		$output .="<h1>Yikes.</h1>";
		$output .="<p>It looks like you're losing money on your marketing efforts.</p>";
		$output .='<img src="'.get_stylesheet_directory_uri().'/assets/images/chart-needs-help.jpg" alt="ROI Needs Help" width="620" height="163" />';
	} else{
		if($_GET['ROI1']){
			$roi1 = $_GET['ROI1'];
			$perc =  round($roi1,2).'%'; 
			$output .='<h1>Your ROI is: '. $perc .'</h1>';
			if($roi1 < 76){
				$output .='<img src="'.get_stylesheet_directory_uri().'/assets/images/chart-needs-help.jpg" alt="ROI Needs Help" width="620" height="163" />';
			} else if($roi1 < 126){
				$output .='<img src="'.get_stylesheet_directory_uri().'/assets/images/chart-not-good.jpg" alt="ROI Not Good" width="620" height="163" />';
			} else if($roi1 < 176){
				$output .= '<img src="'.get_stylesheet_directory_uri().'/assets/images/chart-ok.jpg" alt="ROI OK" width="620" height="163" />';
			} else if($roi1 >= 176){
				$output .= '<img src="'.get_stylesheet_directory_uri().'/assets/images/chart-great.jpg" alt="ROI Great!" width="620" height="163" />';
			}
		} else if($_GET['ROI2']){
			$roi2 = $_GET['ROI2'];
			$perc =  round($roi2, 2).'%'; 
			$output .='<h1>Your ROI is: '. $perc .'</h1>';
			if($roi2 < 76){
				$output .= '<img src="'.get_stylesheet_directory_uri().'/assets/images/chart-needs-help.jpg" alt="ROI Needs Help" width="620" height="163" />';
	
			} else if($roi2 < 126){
				$output .= '<img src="'.get_stylesheet_directory_uri().'/assets/images/chart-not-good.jpg" alt="ROI Not Good" width="620" height="163" />';
	
			} else if($roi2 < 176){
				$output .= '<img src="'.get_stylesheet_directory_uri().'/assets/images/chart-ok.jpg" alt="ROI OK" width="620" height="163" />';
			} else if($roi2 >= 176){
				$output .= '<img src="'.get_stylesheet_directory_uri().'/assets/images/chart-great.jpg" alt="ROI Great!" width="620" height="163" />';
	
			}
		}
	}
	$output .= $values;
	return $output;
}
add_shortcode('roi','roi_shortcode_code');