<?php


/**
 * smg_tawk_to_code
 */
if (! function_exists('smg_tawk_to_code')) {
  function smg_tawk_to_code() {
    ?>
    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src="https://embed.tawk.to/5e6a769feec7650c331fb29b/default";
s1.charset="UTF-8";
s1.setAttribute("crossorigin","*");
s0.parentNode.insertBefore(s1,s0);

    window.dataLayer = window.dataLayer || [];
Tawk_API.onChatStarted = function(){
    //place your code here
    window.dataLayer.push({'event': 'chat-started'});
};
Tawk_API.onChatEnded = function(){
    //place your code here
    window.dataLayer.push({'event': 'chat-ended'});
};
Tawk_API.onOfflineSubmit = function(data){
  //place your code here
    window.dataLayer.push({'event': 'offline-chat'});
};


})();
</script>
<!--End of Tawk.to Script-->

    <?php
  }
  // add_action('wp_footer', 'smg_tawk_to_code', 100); 
}

// Tawk_API.onChatStarted = function(){
//     //place your code here
//      ga('send', 'event', 'chat', 'started');
// };
// Tawk_API.onChatEnded = function(){
//     //place your code here
//      ga('send', 'event', 'chat', 'ended');
// };


add_filter('wpseo_breadcrumb_single_link', 'smg_remove_breadcrumb_title' );
function smg_remove_breadcrumb_title( $link_output) {
  if(is_single()){
    if(strpos( $link_output, 'breadcrumb_last' ) !== false ) {
      $link_output = '';
    }
    return $link_output;
  }
  return $link_output;
}