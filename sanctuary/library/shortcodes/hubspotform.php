<?php
// Add Shortcode
function smg_hub_form_shortcode($atts)
{

    // Attributes
    $atts = shortcode_atts(
        array(
            'labels' => false,
            'form' => '',
            'classes' => 'custom-hub-smg',
        ),
        $atts,
        'smg_hub_form'
    );

    ob_start();

    if ($atts['labels'] == false || $atts['labels'] == 'false') {

        echo '<div class="hs-no-label">';
    }
    echo '<!--[if lte IE 8]>
    <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
    <![endif]-->
    <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
    <script>
      hbspt.forms.create({
      region: "na1",
      portalId: "8263658",
      formId: "' . $atts['form'] . '",
      cssClass: "' . $atts['classes'] . '"
    });
    </script>';

    if ($atts['labels'] == false || $atts['labels'] == 'false') {

        echo '</div>';
    }
    if (isset($_GET['fl_builder'])) {
        $output = ob_get_clean();
        $output = 'Form will be rendered in preview/on the front end.';
    } else {
        $output = ob_get_clean();
    }
    // Return custom embed code
    return $output;

}
add_shortcode('smg_hub_form', 'smg_hub_form_shortcode');
