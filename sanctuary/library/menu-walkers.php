<?php
/**
 * Customize the output of menus
 *
 * @package WordPress
 * @subpackage SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

if ( ! class_exists( 'SMG_Top_Bar_Walker' ) ) {
  class SMG_Top_Bar_Walker extends Walker_Nav_Menu {
	  function start_lvl( &$output, $depth = 0, $args = array() ) {
 			$indent = str_repeat("\t", $depth); 
			 $output .= "<span class='dropdown-button'><img src='". THEME_ROOT ."/assets/images/arrow.png'/></span>\n$indent<ul class=\"dropdown menu vertical\" data-toggle>\n";
		}
	}
}
  
if ( ! class_exists( 'SMG_Mobile_Walker' ) ) {
	class SMG_Mobile_Walker extends Walker_Nav_Menu {
		function start_lvl( &$output, $depth = 0, $args = array() ) {
			$indent = str_repeat("\t", $depth);
			$output .= "\n$indent<ul class=\"vertical nested menu\">\n";
		}
	}
}

// Add Foundation 'active' class for the current menu item.
if ( ! function_exists( 'smg_active_nav_class' ) ) {
	function smg_active_nav_class( $classes, $item ) {
		if ( 1 === $item->current || true === $item->current_item_ancestor ) {
			$classes[] = 'active';
		}
		return $classes;
	}
	add_filter( 'nav_menu_css_class', 'smg_active_nav_class', 10, 2 );
}
