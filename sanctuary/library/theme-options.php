<?php

function get_setting($value){
  // old # 440-946-1718
  $theme_options = array(
    'site-name' => get_bloginfo('name'),
    'header-logo' => array(
      'url' => THEME_ROOT . '/assets/images/header-logo.png',
      'width' => '394',
      'height' => '69',
      'alt' => get_bloginfo('name'),
      'title' => get_bloginfo('name') . ' Logo',
    ),
    'footer-logo' => array(
      'url' => THEME_ROOT . '/assets/images/footer-logo.png',
      'width' => '91',
      'height' => '50',
      'alt' => 'Established 2006',
      'title' => 'Established 2006',
    ), 
    'social' => array(
      'facebook' => 'http://www.facebook.com/sanctuarymg',
      'twitter' => 'https://twitter.com/sanctuarymg/',
      'instagram' => 'https://www.instagram.com/sanctuary.mg/',
      'linkedin' => 'http://www.linkedin.com/company/sanctuary-marketing-group',
    ),
    'address' => array(
      'street' => '219 E. Maple St., Ste 125',
      'city' => 'North Canton',
      'state' => 'OH',
      'zipcode' => '44720',
      'full' => '219 E. Maple St., Ste 125, North Canton OH 44720'
    ),
    'phone' => '330-266-1188',
  );
  if( is_array($value)) {
    foreach($value as $key) {
      $output[$key] = $theme_options[$key];
    }
    return $output;
  }
  return $theme_options[$value];
}

function smg_get_excerpt($limit, $source = null){
  $excerpt = $source == "content" ? get_the_content() : get_the_excerpt();
  $excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
  $excerpt = strip_shortcodes($excerpt);
  $excerpt = strip_tags($excerpt);
  $excerpt = substr($excerpt, 0, $limit);
  $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
  $excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
  $excerpt = $excerpt.'...';
  return $excerpt;
}

add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

function smg_entry_excerpt_length( $length ) {
	return 147;
}
// add_filter( 'excerpt_length', 'smg_entry_excerpt_length', 999 );

function smg_entry_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'smg_entry_excerpt_more');

function smg_reorder_projects_posts($query) {
  if ( $query->is_post_type_archive('portfolio') && $query->is_main_query() ) {
    $query->set( 'order', 'ASC' );
    $query->set( 'orderby', 'menu_order' );
    $query->set( 'posts_per_page', -1 );
  }
}