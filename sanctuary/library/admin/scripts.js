(function($){

  $(function(){

  // Set Variables
  var frame;
  var $this;
  var $app = $('.smg-image');
  var $add = $app.find('.upload-smg-img');
  var $remove = $app.find('.delete-smg-img');
  // var $preview = $app.find('.image-preview');
  // var $input = $app.find('.smg-img-id');

  // Set Actions
  $(function(){
    _init();
    frame.on('select', _getValues);
    $add.on('click', _addImage);
    $remove.on('click', _removeImage);
  });

  var _init = function(){
    frame = wp.media({
      title: 'Select or Upload Media:',
      button: {
        text: 'Select'
      },
      multiple: false  // Set to true to allow multiple files to be selected
    });
  };

  // Set Functions
  var _addImage = function(e) {
    e.preventDefault();
    if( frame ) {
      $this = $(this);
      frame.open();
      return;
    }
  };

  var _removeImage = function(e) {
    e.preventDefault();
    $(this).closest('.smg-image').find('.image-preview').html( '' );
    $(this).closest('.smg-image').find('.upload-smg-img').removeClass( 'hidden' );
    $(this).closest('.smg-image').find('.delete-smg-img').addClass( 'hidden' );
    $(this).closest('.smg-image').find('.smg-img-id').val( '' );
  };

  var _getValues = function() {
    var attachment = frame.state().get('selection').first().toJSON();
    $this.closest('.smg-image').find('.image-preview').append( '<img src="'+attachment.url+'" alt="" style="max-width:100%;"/>' );
    $this.closest('.smg-image').find('.smg-img-id').val( attachment.id );
    $this.closest('.smg-image').find('.upload-smg-img').addClass( 'hidden' );
    $this.closest('.smg-image').find('.delete-smg-img').removeClass( 'hidden' );
  };

  });

})(jQuery);
