<?php

function smg_addtional_featued_image_field( $id, $post_id, $post_type, $args = array() ) {
  global $current_screen;
  if(
    ! ( 'post.php' === $current_screen->parent_file || 'post-new.php' === $current_screen->parent_file ) &&
    ! ( $post_type === $current_screen->post_type || ( is_array( $post_type ) && in_array( $current_screen->post_type, $post_type ) ) ) 
    ) {
    return;
  }
  $field_value = get_post_meta( $post_id, $id, true );
  $field_preview = wp_get_attachment_image_src( $field_value, 'full' );
  $field_have_img = is_array( $field_preview );
  $field_show_upload = $field_have_img ? 'hidden' : '';
  $field_show_delete = $field_have_img ? '' : 'hidden';
  $set_text = isset( $args['set_text'] ) ? __( $args['set_text'] ) : __('Set custom image');
  $remove_text = isset( $args['remove_text'] ) ? __( $args['remove_text'] ) : __('Remove this image');
  $field = '';
  $field .= '<div class="smg-image">';

    // Your image container, which can be manipulated with js
    $field .= '<p class="image-preview hide-if-no-js">';
      if( $field_have_img ){
        $field .= sprintf(
          '<img src="%1$s" style="max-width: 250px;">',
          $field_preview[0]
        );
      }
    $field .= '</p>';

    // Your add & remove image links
    $field .= '<p class="hide-if-no-js">';
      $field .= sprintf('
        <a class="upload-smg-img %1$s" href="#">%2$s</a>
        <a class="delete-smg-img %3$s" href="#">%4$s</a>',
        $field_show_upload, $set_text, $field_show_delete, $remove_text
      );
    $field .= '</p>';

    // A hidden input to set and post the chosen image id
    $field .= sprintf(
      '<input class="smg-img-id" type="hidden" name="%1$s" id="%1$s" value="%2$s">',
      $id, $field_value
    );

  $field .= '</div>';
  return $field;
}

function add_featured_image_fields( $content, $post_id ) {

  $field = '';
  $field .= smg_addtional_featued_image_field( '_featured_logo', $post_id, 'study', array('set_text' => 'Set Client Logo', 'remove_text' => 'Remove Client Logo') );
  $field .= smg_addtional_featued_image_field( '_mobile_image', $post_id, 'portfolio', array('set_text' => 'Set Mobile Image', 'remove_text' => 'Remove Mobile Image' ) );

	return $content .= $field;
}
add_filter( 'admin_post_thumbnail_html', 'add_featured_image_fields', 10, 2 );

function save_study_client_logo( $post_id, $post, $update ) {
  if( isset( $_REQUEST['_featured_logo'] ) ) {
    update_post_meta( $post_id, '_featured_logo', $_REQUEST['_featured_logo'] );
  }
  if( isset( $_REQUEST['_mobile_image'] ) ) {
    update_post_meta( $post_id, '_mobile_image', $_REQUEST['_mobile_image'] );
  }
  
}
add_action( 'save_post', 'save_study_client_logo', 10, 3 );
