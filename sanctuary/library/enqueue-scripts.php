<?php
/**
 * Enqueue all styles and scripts
 *
 * Learn more about enqueue_style: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_style }
 * Learn more about enqueue_script: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_script}
 *
 * Styles Format
 * wp_enqueue_style( string $handle, string $src = '', array $deps = array(), string|bool|null $ver = false, string $media = 'all' );
 * $handle = required, $src = optional, $deps = optional, $ver = optional, $media = optional
 *
 * Scripts Format
 * wp_enqueue_script( string $handle, string $src = '', array $deps = array(), string|bool|null $ver = false, bool $in_footer = false );
 * $handle = required, $src = optional, $deps = optional, $ver = optional, $in_footer = optional
 *
 * @package WordPress
 * @subpackage SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

if (!function_exists('smg_scripts')) {
    function smg_scripts()
    {

        $css_timestamp = filemtime(get_stylesheet_directory() . '/assets/stylesheets/app.css');
        $js_timestamp = filemtime(get_stylesheet_directory() . '/assets/javascript/app.js');
        // Enqueue the main Stylesheet.
        wp_enqueue_style('main-stylesheet', get_stylesheet_directory_uri() . '/assets/stylesheets/app.css#asyncload', array(), (String) $css_timestamp, 'all');

        // Google Fonts
        // $fonts = 'Open+Sans:400,600,700,800';
        $fonts = 'Open+Sans:wght@400;600;700;800';
        wp_enqueue_style('google_fonts', '//fonts.googleapis.com/css2?family=' . $fonts .'&display=swap', array(), null);

        // Stylesheet

        if (current_user_can('administrator') && is_user_logged_in()) {
            /** Uncomment if you want the Wordpress bar to be in the footer */
            // wp_enqueue_style('loggedin-css', get_stylesheet_directory_uri() . '/library/admin/css/logged-in.css', array(), '1.0', 'all');
        }

        // Enqueue the main Scripts
        wp_enqueue_script('main-script', get_template_directory_uri() . '/assets/javascript/app.js#asyncload', array(), (String) $js_timestamp, true);
        wp_enqueue_script('google-partner', get_stylesheet_directory_uri() . '/assets/javascript/external/google-partner.js#asyncload', array(), '1.0.0', true);
        // wp_enqueue_script( 'intercom-smg', get_stylesheet_directory_uri().'/assets/javascript/external/intercom.js#asyncload', array(), '1.0.0', true );
        wp_enqueue_script('slick-smg', get_stylesheet_directory_uri() . '/assets/javascript/external/slick.js#asyncload', array(), '1.0.0', true);

        // wp_enqueue_script('lead-feeder', get_stylesheet_directory_uri() . '/js/leadfeeder.js', array(), '1.0.0', true); 

        if (is_singular('post')) {
            wp_enqueue_script('add-this', '//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-559d2a823c07b28d', array(), '1.0.0', true);
        }
        if (is_singular('team')) {
            wp_enqueue_script('isotope', get_stylesheet_directory_uri() . '/assets/javascript/specific/isotope.pkgd.min.js#asyncload', array(), '1.0.0', true);
            wp_enqueue_script('packery', get_stylesheet_directory_uri() . '/assets/javascript/specific/packery-mode.pkgd.min.js#asyncload', array('isotope'), '1.0.0', true);
            wp_enqueue_script('team-gallery', get_stylesheet_directory_uri() . '/assets/javascript/specific/team-gallery.js#asyncload', array('isotope'), '1.0.0', true);
        }
        if (is_post_type_archive('portfolio')) {

            wp_enqueue_script('portfolio-filter', get_stylesheet_directory_uri() . '/assets/javascript/specific/portfolio-filtering.js#asyncload', array(), '1.0.0', true);
            wp_localize_script('portfolio-filter', 'portAjax', array(
                'ajax_url' => admin_url('admin-ajax.php'),
            ));
        }
        if (is_page(247)) {
            wp_enqueue_script('why-sanctuary', get_stylesheet_directory_uri() . '/assets/javascript/specific/why-sanctuary.js#asyncload', array(), '1.0.0', true);
        }

        if(is_post_type_archive('review')){
            wp_enqueue_script('clutch-js', 'https://widget.clutch.co/static/js/widget.js', array(), '1.0.0', true);
            
        }

        wp_enqueue_style('smg-font-awesome-5', get_stylesheet_directory_uri() . '/assets/fontawesome/all.min.css', array(), '1.1.1', 'all');

        wp_enqueue_script('smg-facebook', get_stylesheet_directory_uri() . '/assets/javascript/external/facebook.js', array(), '1.0.0', true);
    }
    add_action('wp_enqueue_scripts', 'smg_scripts', 9);
}

// if ( ! function_exists( 'smg_scripts_fa' ) ) {
//     function smg_scripts_fa() {
//             wp_enqueue_style('font-awesome-5');
//     }
//     add_action( 'wp_enqueue_scripts', 'smg_scripts_fa' );
// }

if (!function_exists('smg_footer_scripts')) {
    function smg_footer_scripts()
    {
        if (current_user_can('administrator') && is_user_logged_in()) {
            /** Uncomment if you want the Wordpress bar to be in the footer */
            wp_enqueue_style('loggedin-css', get_stylesheet_directory_uri() . '/library/admin/css/logged-in.css#asyncload', array(), '1.0', 'all');
        }
    }
    add_action('wp_footer', 'smg_footer_scripts', 9);
}

/**
 * Enqueue admin styles and scripts
 */
if (!function_exists('smg_admin_scripts')) {
    function smg_admin_scripts($hook)
    {
        global $current_screen;

        if (('post.php' == $hook || 'post-new.php' == $hook) && in_array($current_screen->post_type, array('study', 'portfolio'))) {
            wp_enqueue_script('post-meta-images', THEME_ROOT . '/library/admin/scripts.js', array('jquery', 'media-editor'), '', true);
        }

    }
    add_action('admin_enqueue_scripts', 'smg_admin_scripts');
}

if (!function_exists('defer_fb_pixel')) {
    function defer_fb_pixel()
    {
        ?>
        <!-- Facebook Pixel Code -->
        
        <noscript>
          <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1544594085810074&ev=PageView&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->
        <?php
    }
    add_action('wp_footer', 'defer_fb_pixel');
}
