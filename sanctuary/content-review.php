<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

?>

<section class="main-content review-content" id="post-<?php the_ID();?>">
	<div class="image">
		<img src="<?php echo get_the_post_thumbnail_url(get_the_id(), 'full'); ?>" />
	</div>
	<div class="content">
		<div class="entry-content">
			<?php the_content();?>
		</div>
		<div class="author reviews">
			<span><strong><?php the_title();?></strong> &mdash; <?php echo get_field('title'); ?></span>
			<span><?php echo get_field('company'); ?></span>
		</div>
	</div>
</section>
