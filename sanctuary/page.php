<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

// Testing #4

get_header(); ?>

<main id="page" class="container subpage " role="main">
<?php while ( have_posts() ) : the_post(); ?>
    <article class="main-content" id="post-<?php the_ID(); ?>">
        <header>
            <?php $font_style = get_field('remove_scripty_font') ? 'nonscripty' : 'scripty'; ?>
            <h1 class="entry-title <?php echo $font_style; ?>"><?php the_title(); ?></h1>
        </header>
        <div class="entry-content row-radial-gradient-2 ">
            <div class="not-beaver-builder">
                <?php the_content(); ?>
                <?php // edit_post_link( __( 'Edit', 'foundationpress' ), '<span class="edit-link">', '</span>' ); ?>
            </div>
        </div>
        <footer>
            <?php // wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
        </footer>
    </article>
<?php endwhile;?>
</main>

<?php get_footer();
