<?php
get_header();
?>

<div id="archive" class="container subpage row-radial-gradient-2" role="main">
	<div class="main-content">
		<header>
      <?php yoast_breadcrumb(); ?>
			<h1 class="entry-title">
				<?php echo get_the_title(get_option( 'page_for_posts' )); ?>
			</h1>
		</header>
		<div class="archive-content index-entries">
			<?php
				if( have_posts() ) {
					while ( have_posts() ) { the_post();
						get_template_part( 'content' );
					}
				} else {
					get_template_part( 'content', 'none' );
				}
			?>
			<?php smg_pagination(); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>