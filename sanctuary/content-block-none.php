<?php
/**
 * The default template for displaying no content blocks
 *
 * Used for both projects and case studies.
 *
 * @package SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */
?>
<div class="preview-content-block preview-nocontent-block">
  <div class="before">
    <h2><?php echo apply_filters('no_content_block_content', 'New Coming Soon'); ?></h2>
  </div>
</div>