<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

?>

<article class="main-content entry" id="post-<?php the_ID(); ?>">
	<div class="entry-image" onmouseenter="jQuery(this).parent().find('h2 a').addClass('hovered')" onmouseleave="jQuery(this).parent().find('h2 a').removeClass('hovered')">
		<a href="<?php the_permalink(); ?>">
			<?php
				if (has_post_thumbnail()) {
					the_post_thumbnail('medium');
				} else {
					echo wp_get_attachment_image('11624', 'medium');
				}
			?>
		</a>
	</div>
	<h2 class="entry-title">
		<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
	</h2>
	<div class="entry-content">
		<?php echo smg_get_excerpt(170); ?>
	</div>
</article>
