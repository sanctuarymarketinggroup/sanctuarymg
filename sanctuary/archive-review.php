<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package SanctuaryMG
 * @since SanctuaryMG 1.0.0
 */

get_header();
?>

<div id="archive" class="container subpage row-radial-gradient-2" role="main">
	<div class="main-content">
		<header>
      <?php yoast_breadcrumb(); ?>
			<h1 class="entry-title">
				<?php echo get_the_archive_title(); ?>
			</h1>
		</header>
		<div class="archive-content">
			<div id="client-reviews-intro">
				<p>We've been partnering with our clients since 2006, so these sample reviews and kind words are just scratching the surface of the hundreds of clients that we've successfully partnered with over the years. If you'd like to hear more about our clients and our current successes please contact us and we'll be glad to discuss the specifics further.</p>
					<iframe width="1280" height="720" src="https://www.youtube.com/embed/hnIs3Ojg1AQ?rel=0&amp;showinfo=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
			<?php
				if( have_posts() ) {
					echo '<div id="smg-rev-cont">';
					while ( have_posts() ) { the_post();
						show_template('content-review', array(), './');
					}
					echo '</div>';
				} else {
					get_template_part( 'content', 'none' );
				}
			?>
			<div id="infiniteScrollCont"><div id="smginfinitescroller"></div></div>
			<?php //smg_pagination(); ?>
			<div class="review-external">
				<div>
					<a href="https://agencyvista.com/agency/sanctuary/marketing-agency-north-canton-ohio-us" id="agencyvista-badge" target="_Blank">
						<img src='https://agencyvista.com/verified-agency-vista-badge.png' alt='Agency Vista Verified Badge' width="100" />
					</a>
				</div>
				<div>
					<div class="clutch-widget" data-url="https://widget.clutch.co" data-widget-type="2" data-height="45" data-clutchcompany-id="1426008"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
(function($) {
   var page = 2;
   var total = <?php echo $wp_query->max_num_pages; ?>;
	 var elementTop = $('#infiniteScrollCont').offset().top - 150;
   $(window).scroll(function () {
     if ($(window).scrollTop() + $(window).height() > elementTop) {
       if (page > total) {
         return false;
       } else {
         loadArticle(page);
       }
       page++;
     }
   });

   function loadArticle(pageNumber) {
     $('#smginfinitescroller').show('fast');
     $.ajax({
       url: "<?php echo admin_url(); ?>admin-ajax.php",
       type: 'POST',
       data: "action=review_infinite_scroll&page_no=" + pageNumber,
       success: function (html) {
         $('#smginfinitescroller').hide('1000');
         $("#smg-rev-cont").append(html);
       }
     });
     return false;
   }

})(jQuery);
</script>
<?php get_footer(); ?>
